module org.refcodes.runtime {
	requires java.management;
	requires org.fusesource.jansi;
	requires org.refcodes.generator;
	requires org.refcodes.numerical;
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.data;
	requires transitive org.refcodes.mixin;
	requires transitive org.refcodes.struct;
	requires jdk.unsupported;
	requires transitive java.logging;

	exports org.refcodes.runtime;
}
