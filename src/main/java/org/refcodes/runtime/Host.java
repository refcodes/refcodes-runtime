// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.runtime;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.refcodes.data.DaemonLoopSleepTime;
import org.refcodes.data.Literal;
import org.refcodes.mixin.ResultAccessor;
import org.refcodes.numerical.NumericalUtility;

/**
 * The {@link Host} type abstracts the computer system on which this application
 * runs on.
 */
public final class Host {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private static File _currentDir = null;
	private static String _uname = null;
	private static boolean _hasUname = false;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private Host() {}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Determines the computer's name. First it tries to get it from the
	 * {@link InetAddress}, if it fails it tries to get it from the system's
	 * environment using the {@link EnvironmentVariable#COMPUTERNAME} (on
	 * Windows machines only) and if both fails, it returns the default
	 * {@link Literal#LOCALHOST} identifier.
	 * 
	 * @return The computer's name, as fallback, {@link Literal#LOCALHOST}
	 *         ("localhost") is returned.
	 */
	public static String getComputerName() {
		try {
			return InetAddress.getLocalHost().getHostName();
		}
		catch ( UnknownHostException e ) {
			String theName = EnvironmentVariable.COMPUTERNAME.getValue();
			if ( theName == null || theName.isEmpty() ) {
				theName = EnvironmentVariable.HOSTNAME.getValue();
			}
			if ( theName != null && theName.length() > 0 ) {
				return theName;
			}
		}
		return Literal.LOCALHOST.getValue();
	}

	/**
	 * If on a *nix alike system, this method returns the output of the "uname
	 * -a" command: "uname" prints system information, "-a" instructs it to
	 * print all information.
	 * 
	 * @return The "uname -a" output or null if "uname" is not known.
	 */
	public static String getUname() {
		if ( _hasUname ) {
			return _uname;
		}
		try {
			_uname = exec( DaemonLoopSleepTime.NORM.getTimeMillis(), "uname -a" );
		}
		catch ( IOException | InterruptedException ignore ) {
			try {
				_uname = exec( DaemonLoopSleepTime.NORM.getTimeMillis(), "bash", "-c", "uname -a" );
			}
			catch ( IOException | InterruptedException ignore2 ) {}
		}
		_hasUname = true;
		return _uname;
	}

	/**
	 * Executes a command and returns the output.
	 *
	 * @param aTimeoutMillis The time in milliseconds to wait till the process
	 *        is killed when not terminated yet.
	 * @param aCommandLine the command
	 * 
	 * @return Null if execution failed, else the according output. An empty
	 *         {@link String} stands fur successful execution.
	 * 
	 * @throws IOException in case there were problems executing the command.
	 * @throws InterruptedException thrown in case execution as been
	 *         interrupted.
	 */
	public static String exec( int aTimeoutMillis, String aCommandLine ) throws IOException, InterruptedException {
		// System.out.println( "Executing " + aCommandLine );
		final ProcessBuilder theProcessBuilder = new ProcessBuilder( aCommandLine );
		/*
		 * theProcessBuilder.redirectError( ProcessBuilder.Redirect.INHERIT );
		 * theProcessBuilder.redirectOutput( ProcessBuilder.Redirect.INHERIT );
		 * theProcessBuilder.redirectInput( ProcessBuilder.Redirect.INHERIT );
		 * theProcessBuilder.inheritIO();
		 */
		final Process theProcess = theProcessBuilder.start();
		// Process theProcess = Runtime.getRuntime().exec( aCommandLine );
		final String theOut = new ProcessResult( theProcess, aTimeoutMillis ).toString();
		return theOut;
	}

	/**
	 * Executes a command and returns the output.
	 *
	 * @param aTimeoutMillis The time in milliseconds to wait till the process
	 *        is killed when not terminated yet.
	 * @param aCommandLine the command with the arguments to be passed to the
	 *        command.
	 * 
	 * @return Null if execution failed, else the according output. An empty
	 *         {@link String} stands fur successful execution.
	 * 
	 * @throws IOException in case there were problems executing the command.
	 * @throws InterruptedException thrown in case execution as been
	 *         interrupted.
	 */
	public static String exec( int aTimeoutMillis, String... aCommandLine ) throws IOException, InterruptedException {
		// System.out.println( "Executing " + Arrays.toString( aCommandLine ) );
		final ProcessBuilder theProcessBuilder = new ProcessBuilder( aCommandLine );
		/*
		 * theProcessBuilder.redirectError( ProcessBuilder.Redirect.INHERIT );
		 * theProcessBuilder.redirectOutput( ProcessBuilder.Redirect.INHERIT );
		 * theProcessBuilder.redirectInput( ProcessBuilder.Redirect.INHERIT );
		 * theProcessBuilder.inheritIO();
		 */
		final Process theProcess = theProcessBuilder.start();
		// Process theProcess = Runtime.getRuntime().exec( aCommandLine );
		final String theOut = new ProcessResult( theProcess, aTimeoutMillis ).toString();
		return theOut;
	}

	/**
	 * Executes a command and returns the output.
	 *
	 * @param aCommandLine the command
	 * 
	 * @return Null if execution failed, else the according output. An empty
	 *         {@link String} stands fur successful execution.
	 * 
	 * @throws IOException in case there were problems executing the command.
	 */
	public static String exec( String aCommandLine ) throws IOException {
		// System.out.println( "Executing " + aCommandLine );
		// Process theProcess = Runtime.getRuntime().exec( aCommandLine );
		final ProcessBuilder theProcessBuilder = new ProcessBuilder( aCommandLine );
		// theProcessBuilder.redirectError( ProcessBuilder.Redirect.INHERIT );
		// theProcessBuilder.redirectOutput(ProcessBuilder.Redirect.INHERIT);
		// theProcessBuilder.inheritIO();
		final Process theProcess = theProcessBuilder.start();
		final String theOut = new ProcessResult( theProcess ).toString();
		return theOut;
	}

	/**
	 * Executes a command and returns the output.
	 *
	 * @param aCommandLine the command with the arguments to be passed to the
	 *        command.
	 * 
	 * @return Null if execution failed, else the according output. An empty
	 *         {@link String} stands fur successful execution.
	 * 
	 * @throws IOException in case there were problems executing the command.
	 */
	public static String exec( String... aCommandLine ) throws IOException {
		// System.out.println( "Executing " + Arrays.toString( aCommandLine ) );
		// Process theProcess = Runtime.getRuntime().exec( aCommandLine );
		final ProcessBuilder theProcessBuilder = new ProcessBuilder( aCommandLine );
		// theProcessBuilder.redirectError( ProcessBuilder.Redirect.INHERIT );
		// theProcessBuilder.redirectOutput(ProcessBuilder.Redirect.INHERIT);
		// theProcessBuilder.inheritIO();
		final Process theProcess = theProcessBuilder.start();
		final String theOut = new ProcessResult( theProcess ).toString();
		return theOut;
	}

	/**
	 * Gathers all available system information from this Artifac's point of
	 * view.
	 * 
	 * @return A {@link Map} containing the available information being
	 *         gathered.
	 */
	public static Map<String, String> toSystemInfo() {
		final Map<String, String> theInfo = new HashMap<>();
		theInfo.put( "applicationId", NumericalUtility.toHexString( ":", NumericalUtility.toBytes( SystemContext.APPLICATION.toContextId() ) ) + "" );
		int[] theIds = SystemContext.APPLICATION.toContextIds( 3 );
		final String[] theIdTexts = new String[3];
		for ( int i = 0; i < 3; i++ ) {
			theIdTexts[i] = NumericalUtility.toHexString( ":", NumericalUtility.toBytes( theIds[i] ) );
		}
		theInfo.put( "applicationIds", Arrays.toString( theIdTexts ) );
		theInfo.put( "applicationString", NumericalUtility.toHexString( ":", SystemContext.APPLICATION.toContextString().getBytes() ) );
		theInfo.put( "fileEncoding", SystemProperty.FILE_ENCODING.getValue() );
		theInfo.put( "hostId", NumericalUtility.toHexString( ":", NumericalUtility.toBytes( SystemContext.HOST.toContextId() ) ) + "" );
		theIds = SystemContext.HOST.toContextIds( 3 );
		for ( int i = 0; i < 3; i++ ) {
			theIdTexts[i] = NumericalUtility.toHexString( ":", NumericalUtility.toBytes( theIds[i] ) );
		}
		theInfo.put( "hostIds", Arrays.toString( theIdTexts ) );
		theInfo.put( "hostString", NumericalUtility.toHexString( ":", SystemContext.HOST.toContextString().getBytes() ) );
		theInfo.put( "userId", NumericalUtility.toHexString( ":", NumericalUtility.toBytes( SystemContext.USER.toContextId() ) ) + "" );
		theIds = SystemContext.USER.toContextIds( 3 );
		for ( int i = 0; i < 3; i++ ) {
			theIdTexts[i] = NumericalUtility.toHexString( ":", NumericalUtility.toBytes( theIds[i] ) );
		}
		theInfo.put( "userIds", Arrays.toString( theIdTexts ) );
		theInfo.put( "userString", NumericalUtility.toHexString( ":", SystemContext.USER.toContextString().getBytes() ) );
		theInfo.put( "computerName", getComputerName() );
		try {
			theInfo.put( "ipAddress", NumericalUtility.toHexString( ":", toHostIpAddress() ) );
		}
		catch ( SocketException | UnknownHostException e ) {
			theInfo.put( "ipAddress", e.getMessage() );
		}
		theInfo.put( "isAnsiTerminalPreferred", Terminal.isAnsiTerminalEnabled() + "" );
		theInfo.put( "isAnsiTerminal", Terminal.isAnsiTerminal() + "" );
		theInfo.put( "isCygwinTerminal", Terminal.isCygwinTerminal() + "" );
		theInfo.put( "isLineBreakRequired", Terminal.isLineBreakRequired( Terminal.getWidth() ) + "" );
		theInfo.put( "launcherDir", Execution.toLauncherDir().getAbsolutePath() );
		theInfo.put( "lineBreak", Terminal.getLineBreak().replaceAll( "\\n", "\\\\n" ).replaceAll( "\\r", "\\\\r" ) );
		try {
			theInfo.put( "macAddress", NumericalUtility.toHexString( ":", toHostMacAddress() ) );
		}
		catch ( SocketException | UnknownHostException e ) {
			theInfo.put( "macAddress", e.getMessage() );
		}
		theInfo.put( "mainClass", Execution.getMainClass().getName() );
		theInfo.put( "operatingSystem", OperatingSystem.toOperatingSystem() + "" );
		theInfo.put( "operatingSystemVersion", OperatingSystem.toOperatingSystem().getVersion() );
		theInfo.put( "preferredTerminalHeight", Integer.toString( Terminal.toHeuristicHeight() ) );
		theInfo.put( "preferredTerminalWidth", Integer.toString( Terminal.toHeuristicWidth() ) );
		theInfo.put( "shell", Shell.toShell() + "" );
		theInfo.put( "systemConsole", ( System.console() != null ? System.console().toString() : null ) );
		theInfo.put( "terminalCharSetCapabilities", Terminal.toCharSetCapability().name() );
		theInfo.put( "terminalEncoding", Terminal.getEncoding() );
		theInfo.put( "tempDir", getTempDir() );
		theInfo.put( "terminal", Terminal.toTerminal() + "" );
		theInfo.put( "terminalEncoding", Terminal.getEncoding() + "" );
		theInfo.put( "terminalHeight", Integer.toString( Terminal.getHeight() ) );
		theInfo.put( "terminalWidth", Integer.toString( Terminal.getWidth() ) );
		theInfo.put( "uname", getUname() );
		theInfo.put( "documentArrayIndex", Boolean.toString( SystemProperty.DOCUMENT_ARRAY_INDEX.isEnabled() ) );
		theInfo.put( "documentEnvelope", Boolean.toString( SystemProperty.DOCUMENT_ENVELOPE.isEnabled() ) );
		return theInfo;
	}

	/**
	 * Determines the temporary directory for scratch space.
	 * 
	 * @return The according temporary directory.
	 */
	public static String getTempDir() {
		String theTempDir = SystemProperty.TEMP_DIR.getValue();
		if ( theTempDir == null || theTempDir.isEmpty() ) {
			theTempDir = EnvironmentVariable.TMPDIR.getValue();
		}
		return theTempDir;
	}

	/**
	 * Gathers all available system information from this Artifac's point of
	 * view. This method may rely on the output of {@link #toSystemInfo()}.
	 * 
	 * @return A {@link String} containing the available information being
	 *         gathered.
	 */
	public static String toPrettySystemInfo() {
		final StringBuilder theBuffer = new StringBuilder();
		final Map<String, String> theInfo = toSystemInfo();
		int maxLength = -1;
		for ( String eKey : theInfo.keySet() ) {
			if ( eKey.length() > maxLength ) {
				maxLength = eKey.length();
			}
		}
		final List<String> theKeys = new ArrayList<>( theInfo.keySet() );
		Collections.sort( theKeys );
		String tmpKey;
		for ( String eKey : theKeys ) {
			tmpKey = eKey;
			while ( tmpKey.length() < maxLength ) {
				tmpKey = tmpKey + " ";
			}
			theBuffer.append( tmpKey + " = " + theInfo.get( eKey ) + Terminal.getLineBreak() );
		}
		return theBuffer.toString(); //.replaceAll( "0x", "" );
	}

	/**
	 * Tries to determine a no-localhost IP-Address for this machine. The best
	 * guess is returned. If none no-localhost address is found, then the
	 * localhost's IP-Address may be returned (as of
	 * {@link InetAddress#getLocalHost()}).
	 * 
	 * @return The best guest for a no-localhost IP-Address or as a fall back
	 *         the localhost's IP-Address (as of
	 *         {@link InetAddress#getLocalHost()} may be returned.
	 * 
	 * @throws SocketException Thrown to indicate that accessing the network
	 *         interfaces caused a problem.
	 * @throws UnknownHostException Thrown to indicate that the IP address of
	 *         the local host could not be determined.
	 */
	public static byte[] toHostIpAddress() throws SocketException, UnknownHostException {
		final Enumeration<NetworkInterface> eNetworks;
		eNetworks = NetworkInterface.getNetworkInterfaces();
		NetworkInterface eNetwork;
		Enumeration<InetAddress> eAddresses;
		InetAddress eAddress;
		while ( eNetworks.hasMoreElements() ) {
			eNetwork = eNetworks.nextElement();
			eAddresses = eNetwork.getInetAddresses();
			while ( eAddresses.hasMoreElements() ) {
				try {
					eAddress = eAddresses.nextElement();
					if ( !eAddress.isAnyLocalAddress() && !eAddress.isLoopbackAddress() ) {
						return eAddress.getAddress();
					}
				}
				catch ( IllegalArgumentException ignore ) {
					/* ignore */
				}
			}
		}
		return InetAddress.getLocalHost().getAddress();
	}

	/**
	 * Tries to determine the host Mac-Address for this machine. The best guess
	 * is returned.
	 * 
	 * @return The best guest for a Mac-Address is returned.
	 * 
	 * @throws SocketException Thrown to indicate that accessing the network
	 *         interfaces caused a problem.
	 * @throws UnknownHostException Thrown to indicate that the IP address of
	 *         the local host could not be determined.
	 */
	public static byte[] toHostMacAddress() throws SocketException, UnknownHostException {
		final Enumeration<NetworkInterface> eNetworks;
		eNetworks = NetworkInterface.getNetworkInterfaces();
		NetworkInterface eNetwork;
		byte[] eAddress;
		while ( eNetworks.hasMoreElements() ) {
			eNetwork = eNetworks.nextElement();
			eAddress = eNetwork.getHardwareAddress();
			if ( eAddress != null && eAddress.length != 0 ) {
				return eAddress;
			}
		}
		try {
			return NetworkInterface.getByInetAddress( InetAddress.getLocalHost() ).getHardwareAddress();
		}
		catch ( SocketException | UnknownHostException e ) {
			throw e;
		}
		catch ( Exception e ) {
			throw new UnknownHostException( "Unable to determine host address: " + e.getMessage() );
		}
	}

	/**
	 * Bad hack to get the JVM's (process TID) PID of the process running your
	 * JVM instance.
	 * 
	 * @return The PID (process TID) of the JVM running your thread.
	 * 
	 * @see "http://stackoverflow.com/questions/35842/how-can-a-java-program-get-its-own-process-id"
	 */
	public static Long getPid() {
		Long thePid = null;
		try {
			thePid = Long.parseLong( SystemProperty.PROCESS_ID.getValue() );
		}
		catch ( NumberFormatException e1 ) {
			final String theJvmName = ManagementFactory.getRuntimeMXBean().getName();
			if ( theJvmName.indexOf( '@' ) != -1 ) {
				try {
					thePid = Long.parseLong( theJvmName.substring( 0, theJvmName.indexOf( '@' ) ) );
				}
				catch ( NumberFormatException e2 ) {
					// Unable to determine PID
				}
			}
		}
		return thePid;
	}

	/**
	 * Bad hack to kill an OS thread by PID. The current threads does not wait
	 * till the operation finished.
	 * 
	 * @param aPid The process TID (PID) of the process to kill.
	 * 
	 * @return The {@link Process} object representing the kill operation. This
	 *         instance will let you wait till the operation finished
	 *         {@link Process#waitFor()} and provides access to the
	 *         {@link Process#exitValue()}
	 * 
	 * @throws IOException Thrown in case of failing to successfully execute the
	 *         kill operation.
	 * 
	 * @see "http://stackoverflow.com/questions/9573696/kill-a-process-based-on-pid-in-java"
	 * @see "http://stackoverflow.com/questions/2950338/how-can-i-kill-a-linux-process-in-java-with-sigkill-process-destroy-does-sigte"
	 */
	public static Process killProcess( long aPid ) throws IOException {
		Process theProcess = null;
		final String theCmd[];
		switch ( OperatingSystem.toOperatingSystem() ) {
		case WINDOWS -> {
			theCmd = new String[] { "taskkill", "/F", "/PID", Long.toString( aPid ) };
			theProcess = java.lang.Runtime.getRuntime().exec( theCmd );
		}
		case UNIX -> {
			theCmd = new String[] { "kill", "-9", Long.toString( aPid ) };
			theProcess = java.lang.Runtime.getRuntime().exec( theCmd );
		}
		default -> {
		}
		}
		return theProcess;
	}

	/**
	 * Bad hack to kill an OS thread by PID. The current threads does wait till
	 * the operation finished.
	 *
	 * @param aPid The process TID (PID) of the process to kill.
	 * 
	 * @return True in case killing the process was successful (e.g. the kill
	 *         operation returned an exit code 0), else false.
	 * 
	 * @throws IOException Thrown in case of failing to successfully execute the
	 *         kill operation.
	 * @throws InterruptedException the interrupted exception
	 * 
	 * @see "http://stackoverflow.com/questions/9573696/kill-a-process-based-on-pid-in-java"
	 * @see "http://stackoverflow.com/questions/2950338/how-can-i-kill-a-linux-process-in-java-with-sigkill-process-destroy-does-sigte"
	 */
	public static boolean kill( Long aPid ) throws IOException, InterruptedException {
		final Process theProcess = killProcess( aPid );
		if ( theProcess != null ) {
			theProcess.waitFor();
			return ( theProcess.exitValue() == 0 );
		}
		return false;
	}

	/**
	 * Tries to determine the current directory from which the Java programm was
	 * started for example as of bash's <code>pwd</code> command. Pass as JVM
	 * argument via "-Dcurrent.dir=/path/to/current/dir".
	 *
	 * @return The current path of the user invoking the Java programm or null
	 *         if the current path cannot be determined.
	 */
	public static File toUserWorkingDir() {
		if ( _currentDir == null ) {
			String theCurrentDir = SystemProperty.USER_DIR.getValue();
			if ( theCurrentDir != null && theCurrentDir.length() != 0 ) {
				File theCurrentFile = new File( theCurrentDir );
				if ( theCurrentFile.exists() && theCurrentFile.isDirectory() ) {
					_currentDir = theCurrentFile;
				}
				else {
					theCurrentDir = EnvironmentVariable.USER_DIR.getValue();
					if ( theCurrentDir != null && theCurrentDir.length() != 0 ) {
						theCurrentFile = new File( theCurrentDir );
						if ( theCurrentFile.exists() && theCurrentFile.isDirectory() ) {
							_currentDir = theCurrentFile;
						}
					}
				}
			}
		}
		return _currentDir;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Retrieves the Result of the given process as {@link String}.
	 */
	private static class ProcessResult implements ResultAccessor<String, RuntimeException> {

		// /////////////////////////////////////////////////////////////////////
		// STATICS:
		// /////////////////////////////////////////////////////////////////////

		// /////////////////////////////////////////////////////////////////////
		// CONSTANTS:
		// /////////////////////////////////////////////////////////////////////

		private final String _result;

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Constructs a {@link ProcessResult} instance which will synchronously
		 * return the process' result via {@link #getResult()} (and
		 * {@link #toString()} accordingly).
		 * 
		 * @param aProcess The {@link Process} from which to retrieve the
		 *        result.
		 * 
		 * @throws IOException Thrown in case there were problems retrieving the
		 *         result. If possible, the error stream of the process is
		 *         captured in the exception's message.
		 */
		public ProcessResult( Process aProcess ) throws IOException {
			try {
				aProcess.waitFor();
			}
			catch ( InterruptedException ignoree ) {}
			// while ( aProcess.isAlive() ) {
			// 	try {
			// 		Thread.sleep( SleepLoopTime.MIN.getMilliseconds() );
			// 	}
			// 	catch ( InterruptedException ignore ) {}
			// }
			_result = toResult( aProcess );
		}

		/**
		 * Constructs a {@link ProcessResult} instance which will synchronously
		 * return the process' result via {@link #getResult()} (and
		 * {@link #toString()} accordingly).
		 * 
		 * @param aProcess The {@link Process} from which to retrieve the
		 *        result.
		 * @param aTimeoutMillis The time to wait till to terminate with an
		 *        {@link InterruptedException}.
		 * 
		 * @throws IOException Thrown in case there were problems retrieving the
		 *         result. If possible, the error stream of the process is
		 *         captured
		 * @throws InterruptedException Thrown in case the timeout has exceeded
		 *         before process' termination.
		 */
		public ProcessResult( Process aProcess, int aTimeoutMillis ) throws InterruptedException, IOException {
			if ( aTimeoutMillis != -1 ) {
				aProcess.waitFor( aTimeoutMillis, TimeUnit.MILLISECONDS );
				// double now = System.currentTimeMillis();
				// while ( aProcess.isAlive() && System.currentTimeMillis() < now + aTimeoutMillis ) {
				// 	Thread.sleep( SleepLoopTime.MIN.getMilliseconds() );
				// }
				if ( aProcess.isAlive() ) {
					// long thePid = aProcess.pid();
					try {
						aProcess.destroy();
					}
					catch ( Exception ignore ) {}
					throw new InterruptedException( "Killed process <" + aProcess.toString() + ">!" );
				}
			}
			else {
				aProcess.waitFor();
				// while ( aProcess.isAlive() ) {
				// 	Thread.sleep( SleepLoopTime.MIN.getMilliseconds() );
				// }
			}
			_result = toResult( aProcess );
		}

		// /////////////////////////////////////////////////////////////////////
		// INJECTION:
		// /////////////////////////////////////////////////////////////////////

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String getResult() {
			return _result;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean hasResult() {
			return _result != null;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String toString() {
			return _result;
		}

		// /////////////////////////////////////////////////////////////////////
		// HOOKS:
		// /////////////////////////////////////////////////////////////////////

		// /////////////////////////////////////////////////////////////////////
		// HELPER:
		// /////////////////////////////////////////////////////////////////////

		private String toResult( Process aProcess ) throws IOException {
			try {
				final StringBuilder theOutBuilder = new StringBuilder();
				try ( BufferedReader theOutReader = new BufferedReader( new InputStreamReader( aProcess.getInputStream() ) ) ) {
					String eLine;
					while ( ( eLine = theOutReader.readLine() ) != null ) {
						theOutBuilder.append( eLine );
					}
				}
				return theOutBuilder.toString();
			}
			catch ( IOException e ) {
				final StringBuilder theErrBuilder = new StringBuilder();
				try ( BufferedReader theErrReader = new BufferedReader( new InputStreamReader( aProcess.getInputStream() ) ) ) {
					if ( theErrReader != null ) {
						String eErr;
						while ( ( eErr = theErrReader.readLine() ) != null ) {
							theErrBuilder.append( eErr );
						}
					}
				}
				catch ( Exception ignore ) {}
				if ( theErrBuilder.length() != 0 ) {
					throw new IOException( theErrBuilder.toString(), e );
				}
				throw e;
			}
		}
	}
}
