// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.runtime;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.regex.Matcher;

import org.refcodes.data.Delimiter;
import org.refcodes.data.ExitCode;
import org.refcodes.data.Folder;
import org.refcodes.data.Scheme;
import org.refcodes.data.SleepLoopTime;
import org.refcodes.exception.Trap;
import org.refcodes.struct.Attribute;
import org.refcodes.struct.AttributeImpl;

import sun.misc.Signal;

/**
 * Utility for acquiring runtime information on software systems, classes or
 * objects.
 */
public final class Execution {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Logger LOGGER = Logger.getLogger( Execution.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String[] LOGGER_PACKAGE_PARTS = new String[] { ".log.", ".logger.", ".logging.", "org.refcodes.struct.", "org.refcodes.properties." };

	/**
	 * This class indicates a non existing value in the name-to-value mapping.
	 */
	public static class NonExistingValueClass {}

	public static final NonExistingValueClass NON_EXISTING_VALUE = new NonExistingValueClass();
	public static final String ALIAS_SET = "set";
	public static final String ALIAS_GET = "get";
	public static final String ALIAS_HAS = "has";
	public static final String ALIAS_IS = "is";
	public static final String ALIAS_TOSTRING = "toString";

	private static final String[] RESERVED_PACKAGE_PREFIXES = new String[] { "sun.", "com.sun.", "java.", "javax." };
	private static String[] MAIN_METHODS = { "main", "start", "<clinit>", "<init>" };
	private static String[] MAIN_METHOD_PREFIXES = { "test" };
	private static String[] FRAMEWORK_LAUNCHERS = { "JarLauncher", "TestRunner", "ForkedBooter" };
	private static Scheme[] JAR_PROTOCOLS = new Scheme[] { Scheme.JAR, Scheme.ZIP, Scheme.SH };
	private static final String SPACE_PLACEHOLDER = "%20";
	public static final String CTRL_C_SIGNAL = "INT";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private static Boolean _isUnderTest = null;
	private static boolean _isAnsiInstalled = false;
	private static boolean _hasShutdownHandle = false;
	private static List<Thread> _shutdownHooks = new LinkedList<>();
	private static Thread _shutdownHook;
	private static Class<?> _mainClass = null;
	private static File _launcherDir = null;

	private static final boolean _isAnsi = System.console() != null && Terminal.isAnsiTerminalEnabled(); // Jansi crashes when run without console (javaw), see https://github.com/fusesource/jansi/issues/216

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private Execution() {}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Generates the base path relative to the given class location. Depending
	 * on the runtime, the path is truncated till the required path is
	 * determined. In case the {@link SystemProperty#LAUNCHER_DIR} (e.g. when
	 * providing the following argument
	 * <code>-Dlauncher.dir=/path/to/launcher/dir </code> when invoking your
	 * java's executable) is set and in case its value points to a directory,
	 * then this directory is returned.
	 *
	 * @return The base path of this application.
	 */
	public static File toLauncherDir() {
		if ( _launcherDir == null ) {
			final String theLauncherDir = SystemProperty.LAUNCHER_DIR.getValue();
			if ( theLauncherDir != null && theLauncherDir.length() != 0 ) {
				final File theLauncherFile = new File( theLauncherDir );
				if ( theLauncherFile.exists() && theLauncherFile.isDirectory() ) {
					return theLauncherFile;
				}
			}

			Class<?> baseClass = getMainClass();
			if ( baseClass == null ) {
				baseClass = Execution.class;
			}
			String basePath = baseClass.getProtectionDomain().getCodeSource().getLocation().getPath();
			if ( basePath.startsWith( Scheme.FILE.toProtocol() ) ) {
				basePath = basePath.substring( Scheme.FILE.toProtocol().length() );
			}
			String truncate;
			try {
				final String theCanonicalName = baseClass.getCanonicalName();
				truncate = theCanonicalName.replace( '.', '/' );
			}
			catch ( Exception e ) {
				return null;
			}
			truncate = truncate.substring( 0, truncate.indexOf( baseClass.getSimpleName() ) );
			final int endIndex = basePath.indexOf( truncate );
			if ( endIndex > 0 ) {
				basePath = basePath.substring( 0, endIndex );
			}
			if ( basePath.endsWith( "" + Delimiter.PATH.getChar() ) ) {
				basePath = basePath.substring( 0, basePath.length() - 1 );
			}
			if ( basePath.endsWith( Folder.CLASSES.getName() ) ) {
				basePath = basePath.substring( 0, basePath.indexOf( Folder.CLASSES.getName() ) );
			}
			int index = -1;

			for ( Scheme eProtocol : JAR_PROTOCOLS ) {
				index = eProtocol.firstMarkerIndex( basePath.toLowerCase() );
				if ( index != -1 ) {
					break;
				}
			}
			if ( index != -1 ) {
				basePath = basePath.substring( 0, index );
				index = basePath.lastIndexOf( Delimiter.PATH.getChar() );
				if ( index != -1 ) {
					basePath = basePath.substring( 0, index );
				}
			}
			if ( !basePath.endsWith( "" + Delimiter.PATH.getChar() ) ) {
				basePath = basePath + Delimiter.PATH.getChar();
			}
			_launcherDir = new File( basePath );
			if ( !basePath.endsWith( Folder.TARGET.getName() + Delimiter.PATH.getChar() ) ) {
				_launcherDir = _launcherDir.getParentFile();
			}
			if ( !_launcherDir.exists() && basePath.contains( SPACE_PLACEHOLDER ) ) {
				basePath = basePath.replaceAll( Matcher.quoteReplacement( SPACE_PLACEHOLDER ), " " );
				_launcherDir = new File( basePath );
				if ( !basePath.endsWith( Folder.TARGET.getName() + Delimiter.PATH.getChar() ) ) {
					_launcherDir = _launcherDir.getParentFile();
				}
			}
		}
		return _launcherDir;
	}

	/**
	 * Returns the main class launching the application.
	 *
	 * @return The main class.
	 */
	@SuppressWarnings("rawtypes")
	public static Class getMainClass() {
		out: {
			if ( _mainClass == null ) {
				synchronized ( Host.class ) {
					final StackTraceElement[] stack = Thread.currentThread().getStackTrace();
					StackTraceElement eElement;
					Class<?> mainClass;
					final List<StackTraceElement> theCandidates = new ArrayList<>();
					for ( StackTraceElement aStack : stack ) {
						eElement = aStack;
						if ( hasTestAnnotation( eElement ) ) {
							theCandidates.add( eElement );
							continue;
						}
					}
					for ( int i = stack.length - 1; i > 0; i-- ) {
						eElement = stack[i];
						for ( String eMain : MAIN_METHODS ) {
							if ( eElement.getMethodName().equals( eMain ) ) {
								theCandidates.add( eElement );
								continue;
							}
						}
						for ( String ePrefix : MAIN_METHOD_PREFIXES ) {
							if ( eElement.getMethodName().startsWith( ePrefix ) ) {
								theCandidates.add( eElement );
								continue;
							}
						}
					}

					// First try to skip framework launchers |-->
					NEXT: for ( StackTraceElement eCandidate : theCandidates ) {
						for ( String eLauncer : FRAMEWORK_LAUNCHERS ) {
							if ( eCandidate.getClassName().toLowerCase().contains( eLauncer.toLowerCase() ) ) {
								continue NEXT;
							}
						}
						try {
							mainClass = Class.forName( eCandidate.getClassName() );
							if ( mainClass.getProtectionDomain().getCodeSource() != null ) {
								_mainClass = mainClass;
								break out;
							}
						}
						catch ( ClassNotFoundException e ) {}
					}
					// First try to skip framework launchers <--|

					for ( StackTraceElement eCandidate : theCandidates ) {
						try {
							mainClass = Class.forName( eCandidate.getClassName() );
							if ( mainClass.getProtectionDomain().getCodeSource() != null ) {
								_mainClass = mainClass;
								break out;
							}
						}
						catch ( ClassNotFoundException e ) {}
					}
				}
			}
		}
		return _mainClass;
	}

	/**
	 * Registers a new virtual-machine shutdown hook.
	 * 
	 * In case of running as a native image (GraalVM AOT), then a {@link Signal}
	 * handler for capturing CTRL+C is registered exiting the application
	 * regularirly so that shutdown hooks registered with
	 * {@link Execution#addShutdownHook(Thread)} are also called when inside a
	 * native image. Exiting vie CTRL+C then will exit with status code 130 as
	 * of {@link ExitCode#CONTROL_C}.
	 * 
	 * @param hook An initialized but not started {@link Thread} object
	 */
	public static void addShutdownHook( Thread hook ) {
		if ( !_shutdownHooks.contains( hook ) ) {
			// The later, the more application logic! Application logic should be shut down beofre system services |-->
			_shutdownHooks.add( 0, hook );
			// The later, the more application logic! Application logic should be shut down beofre system services <--|
		}
		if ( _shutdownHook == null ) {
			synchronized ( _shutdownHooks ) {
				if ( _shutdownHook == null ) {
					_shutdownHook = new Thread( () -> {
						for ( Thread t : _shutdownHooks ) {
							try {
								t.run();
							}
							catch ( Exception e ) {
								LOGGER.log( Level.WARNING, "Encountered erroneous shutdown hook <" + t + "> as of: " + Trap.asMessage( e ), e );
							}
						}
					} );
					_shutdownHook.setDaemon( true );
					java.lang.Runtime.getRuntime().addShutdownHook( _shutdownHook );
				}
			}
		}
		if ( !_hasShutdownHandle && isNativeImage() ) {
			synchronized ( Host.class ) {
				if ( !_hasShutdownHandle ) {
					_hasShutdownHandle = true;
					try {
						Signal.handle( new Signal( CTRL_C_SIGNAL ), sig -> System.exit( ExitCode.CONTROL_C.getStatusCode() ) );
					}
					catch ( IllegalArgumentException ignore ) {
						// !!! DO NOT FAIL IN CASE OF BEING UNSUPPORTED !!!
					}
				}
			}
		}
	}

	/**
	 * Registers a new virtual-machine shutdown hook.
	 * 
	 * In case of running as a native image (GraalVM AOT), then a {@link Signal}
	 * handler for capturing CTRL+C is registered exiting the application
	 * properly so that shutdown hooks registered with
	 * {@link Execution#addShutdownHook(Thread)} are also called when inside a
	 * native image. Exiting vie CTRL+C then will exit with status code 130 as
	 * of {@link ExitCode#CONTROL_C}.
	 * 
	 * @param hook A {@link Runnable} object (convenience method for
	 *        {@link #addShutdownHook(Thread)}).
	 */
	public static void addShutdownHook( Runnable hook ) {
		java.lang.Runtime.getRuntime().addShutdownHook( new Thread( hook ) );
		if ( !_hasShutdownHandle && isNativeImage() ) {
			synchronized ( Host.class ) {
				if ( !_hasShutdownHandle ) {
					_hasShutdownHandle = true;
					Signal.handle( new Signal( CTRL_C_SIGNAL ), sig -> System.exit( ExitCode.CONTROL_C.getStatusCode() ) );
				}
			}
		}
	}

	/**
	 * Redirects the {@link Logger} configuration and sets the provided handler.
	 * 
	 * @param aHandler The {@link Handler} to be set as the one and only
	 *        handler.
	 */
	public static void setJulLoggingHandler( Handler aHandler ) {
		LogManager.getLogManager().reset();
		final Logger theRootLogger = LogManager.getLogManager().getLogger( "" );
		theRootLogger.addHandler( aHandler );
	}

	/**
	 ** Redirects the {@link Logger} configuration to the provided streams.
	 * 
	 * @param aStdStream The {@link OutputStream} to write all unproblematic
	 *        logs with levels such as {@link Level#INFO} or
	 *        {@link Level#CONFIG} to.
	 * @param aErrStream The {@link OutputStream} to write all problematic logs
	 *        with levels such as {@link Level#WARNING} or {@link Level#SEVERE}
	 *        to.
	 * 
	 * @param aLevel The log {@link Level} as of which to log to the provided
	 *        streams.
	 */
	public static void setJulLoggingStreams( OutputStream aStdStream, OutputStream aErrStream, Level aLevel ) {
		setJulLoggingHandler( new Handler() {

			@SuppressWarnings("resource")
			PrintWriter _stdWriter = new PrintWriter( aStdStream );
			PrintWriter _errWriter = new PrintWriter( aErrStream );

			@Override
			public void publish( LogRecord aRecord ) {
				if ( aRecord.getLevel().intValue() >= aLevel.intValue() ) {
					final Throwable theThrown = aRecord.getThrown();
					String theLine = aRecord.getLevel() + ": " + Trap.asMessage( aRecord.getMessage(), aRecord.getParameters() );
					if ( theThrown != null && theThrown.getMessage() != null ) {
						theLine += " (" + theThrown.getMessage() + ")";
					}
					if ( aRecord.getLevel().intValue() > Level.INFO.intValue() ) {
						_errWriter.println( theLine );
						if ( theThrown != null ) {
							theThrown.printStackTrace( _errWriter );
						}
					}
					else {
						if ( theThrown == null ) {
							_stdWriter.println( theLine );
						}
						else {
							_errWriter.println( theLine );
						}
						if ( theThrown != null ) {
							theThrown.printStackTrace( _errWriter );
						}
					}
				}
			}

			@Override
			public void flush() {
				_stdWriter.flush();
				_errWriter.flush();
			}

			@Override
			public void close() {
				_stdWriter.close();
				_errWriter.close();

			}
		} );

	}

	/**
	 * Sets the current JUL logging {@link Level} to the given {@link Level}.
	 * 
	 * @param aLevel The {@link Level} to which to set the current JUL logging
	 *        {@link Level}.
	 */
	public static void setJulLoggingLevel( Level aLevel ) {
		final java.util.logging.Logger theRootJulLogger = LogManager.getLogManager().getLogger( "" );
		theRootJulLogger.setLevel( aLevel );
		for ( Handler e : theRootJulLogger.getHandlers() ) {
			e.setLevel( aLevel );
		}
	}

	/**
	 * Returns the original {@link PrintStream} for STDOUT as initialized by the
	 * JVM at bootstrapping time (the {@link System#out} {@link PrintStream} may
	 * have got modified by libraries or frameworks).
	 * 
	 * The returned {@link PrintStream}'s {@link PrintStream#close()} method is
	 * disabled for preventing the STD OUT stream to be closed upon an auto
	 * close <code>try( ... ) { ... }</code> statement!
	 * 
	 * @return The real {@link PrintStream} as initially set.
	 */
	public static PrintStream toBootstrapStandardOut() {
		return new PrintStream( new FileOutputStream( FileDescriptor.out ) {
			@Override
			public void close() throws IOException {
				// Do not close me in auto close try() !
			}
		} );
	}

	/**
	 * Returns the original {@link PrintStream} for STDERR as initialized by the
	 * JVM at bootstrapping time (the {@link System#err} {@link PrintStream} may
	 * have got modified by libraries or frameworks in the meanwhile).
	 * 
	 * The returned {@link PrintStream}'s {@link PrintStream#close()} method is
	 * disabled for preventing the STD OUT stream to be closed upon an auto
	 * close <code>try( ... ) { ... }</code> statement!
	 * 
	 * @return The real {@link PrintStream} as initially set.
	 */
	public static PrintStream toBootstrapStandardError() {
		return new PrintStream( new FileOutputStream( FileDescriptor.err ) {
			@Override
			public void close() throws IOException {
				// Do not close me in auto close try() !
			}
		} );
	}

	/**
	 * Returns the original {@link InputStream} for STDIN as initialized by the
	 * JVM at bootstrapping time (the {@link System#in} {@link InputStream} may
	 * have got modified by libraries or frameworks in the meanwhile).
	 * 
	 * The returned {@link InputStream}'s {@link InputStream#close()} method is
	 * disabled for preventing the STD OUT stream to be closed upon an auto
	 * close <code>try( ... ) { ... }</code> statement!
	 * 
	 * @return The real {@link InputStream} as initially set.
	 */
	public static InputStream toBootstrapStandardIn() {
		return new FileInputStream( FileDescriptor.in ) {
			@Override
			public void close() throws IOException {
				// Do not close me in auto close try() !
			}
		};
	}

	/**
	 * Create a {@link PrintStream} from {@link System#out} being UTF-8 encoded
	 * and filtering out ANSI escape codes in case the underlying terminal does
	 * not support the such.
	 * 
	 * The returned {@link PrintStream}'s {@link PrintStream#close()} method is
	 * disabled for preventing the STD OUT stream to be closed upon an auto
	 * close <code>try( ... ) { ... }</code> statement!
	 * 
	 * @return The {@link System#out} {@link PrintStream} accordingly wrapped.
	 */
	public static PrintStream toSystemOut() {
		return OperatingSystem.toOperatingSystem() == OperatingSystem.WINDOWS && !isUnderTest() ? toAnsiOut() : new PrintStream( System.out ) {
			@Override
			public void close() {
				// Do not close me in auto close try() !
			}
		};
	}

	/**
	 * Create a {@link PrintStream} from {@link System#err} being UTF-8 encoded
	 * and filtering out ANSI escape codes in case the underlying terminal does
	 * not support the such.
	 * 
	 * The returned {@link PrintStream}'s {@link PrintStream#close()} method is
	 * disabled for preventing the STD OUT stream to be closed upon an auto
	 * close <code>try( ... ) { ... }</code> statement!
	 * 
	 * @return The {@link System#err} {@link PrintStream} accordingly wrapped.
	 */
	public static PrintStream toSystemErr() {
		return OperatingSystem.toOperatingSystem() == OperatingSystem.WINDOWS && !isUnderTest() ? toAnsiErr() : new PrintStream( System.err ) {
			@Override
			public void close() {
				// Do not close me in auto close try() !
			}
		};
	}

	/**
	 * Determines whether we are inside an executing native image, e.g. the
	 * {@link SystemProperty#NATIVE_IMAGE_CODE}'s value evaluates to
	 * {@link NativeImageCode#RUNTIME}'s value.
	 * 
	 * @return True in case we are inside an executing native image.
	 */
	public static boolean isNativeImage() {
		return NativeImageCode.isRuntime();
	}

	/**
	 * Determines whether this code is executed from inside a unit test or not.
	 * 
	 * @return True in case if executed from within a unit test.
	 */
	public static boolean isUnderTest() {
		if ( _isUnderTest != null ) {
			return _isUnderTest;
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			return true;
		}
		if ( SystemProperty.LOG_DEBUG.isEnabled() ) {
			return true;
		}
		final StackTraceElement[] theStackTrace = Thread.currentThread().getStackTrace();
		final List<StackTraceElement> theList = Arrays.asList( theStackTrace );
		out: {
			for ( StackTraceElement eElement : theList ) {
				if ( eElement.getClassName().startsWith( "org.junit." ) || eElement.getClassName().startsWith( "org.testng." ) ) {
					_isUnderTest = true;
					break out;
				}
			}
			_isUnderTest = false;
		}
		return _isUnderTest;
	}

	/**
	 * Gets the stack trace for the current thread.
	 * 
	 * @return The stack trace from the current thread.
	 */
	public static String toStackTrace() {
		final StringWriter theStringWriter = new StringWriter();
		new Throwable( "" ).printStackTrace( new PrintWriter( theStringWriter ) );
		return theStringWriter.toString();
	}

	/**
	 * Returns the {@link StackTraceElement} belonging to the direct caller of
	 * this method. When you use this method in your code, you get the stack
	 * trace element of your method (invoking this method).
	 * 
	 * @return The stack element of the direct caller of this method.
	 */
	public static StackTraceElement getCallerStackTraceElement() {
		final StackTraceElement[] theStackTraceElements = Thread.currentThread().getStackTrace();
		for ( StackTraceElement eStackTraceElement : theStackTraceElements ) {
			if ( !isSkipStackTraceElement( eStackTraceElement ) ) {
				return eStackTraceElement;
			}
		}
		return null;
	}

	/**
	 * Returns the {@link StackTraceElement} belonging to the caller of the
	 * callee. Best you do not use the {@link Object#getClass()} method, instead
	 * use YourClass.class as as of inheritance, {@link Object#getClass()}
	 * returns the wrong type not being the actual callee!
	 * 
	 * @param aCallee The callee class which wants to find out who called it.
	 * 
	 * @return The stack element of the caller of the callee or null if the
	 *         callee is not present or if there is no caller of the given
	 *         callee in the current stack trace.
	 */
	public static StackTraceElement getCallerStackTraceElement( Class<?> aCallee ) {
		return getCallerStackTraceElement( aCallee.getName() );
	}

	/**
	 * Returns the {@link StackTraceElement} belonging to the caller of the
	 * callee. Best you do not use the {@link Object#getClass()} method, instead
	 * use YourClass.class as as of inheritance, {@link Object#getClass()}
	 * returns the wrong type not being the actual callee!
	 * 
	 * @param aCallee The callee {@link StackTraceElement} which wants to find
	 *        out who called it.
	 * 
	 * @return The stack element of the caller of the callee or null if the
	 *         callee is not present or if there is no caller of the given
	 *         callee in the current stack trace.
	 */
	public static StackTraceElement getCallerStackTraceElement( StackTraceElement aCallee ) {
		return getCallerStackTraceElement( aCallee.getClassName() );
	}

	/**
	 * Same as {@link #getCallerStackTraceElement(Class)} with the difference
	 * that the passed callees are tried out one after the other until the first
	 * caller determined for a callee is returned.
	 *
	 * @param aCallees the callees
	 * 
	 * @return the caller {@link StackTraceElement}
	 */
	public static StackTraceElement getCallerStackTraceElement( Class<?>... aCallees ) {
		StackTraceElement eCaller = null;
		for ( Class<?> eClallee : aCallees ) {
			eCaller = getCallerStackTraceElement( eClallee );
			if ( eCaller != null ) {
				return eCaller;
			}
		}
		return null;
	}

	/**
	 * Same as {@link #getCallerStackTraceElement(StackTraceElement)} with the
	 * difference that the passed callees are tried out one after the other
	 * until the first caller determined for a callee is returned.
	 *
	 * @param aCallees the callees
	 * 
	 * @return the caller {@link StackTraceElement}
	 */
	public static StackTraceElement getCallerStackTraceElement( StackTraceElement... aCallees ) {
		StackTraceElement eCaller = null;
		for ( StackTraceElement eClallee : aCallees ) {
			eCaller = getCallerStackTraceElement( eClallee );
			if ( eCaller != null ) {
				return eCaller;
			}
		}
		return null;
	}

	/**
	 * Same as {@link #getCallerStackTraceElement(String)} with the difference
	 * that the passed callees are tried out one after the other until the first
	 * caller determined for a callee is returned.
	 *
	 * @param aCalleeClassNames the callee class names
	 * 
	 * @return the caller {@link StackTraceElement}
	 */
	public static StackTraceElement getCallerStackTraceElement( String... aCalleeClassNames ) {
		StackTraceElement eCaller = null;
		for ( String eClallee : aCalleeClassNames ) {
			eCaller = getCallerStackTraceElement( eClallee );
			if ( eCaller != null ) {
				return eCaller;
			}
		}
		return null;
	}

	/**
	 * Returns the {@link StackTraceElement} belonging to the caller of the
	 * callee. The callee can also be a package namespace where the matchee's
	 * must begin with the given package namespace.
	 * 
	 * @param aCalleeClassName The callee class name or package namespace which
	 *        wants to find out who called it.
	 * 
	 * @return The stack element of the caller of the callee or null if the
	 *         callee is not present or if there is no caller of the given
	 *         callee in the current stack trace.
	 */
	public static StackTraceElement getCallerStackTraceElement( String aCalleeClassName ) {
		final StackTraceElement[] theStackTraceElements = Thread.currentThread().getStackTrace();
		boolean hasCalleeElement = false;
		for ( StackTraceElement eStackTraceElement : theStackTraceElements ) {
			if ( hasCalleeElement && !eStackTraceElement.getClassName().startsWith( aCalleeClassName ) ) {
				return eStackTraceElement;
			}
			if ( eStackTraceElement.getClassName().startsWith( aCalleeClassName ) ) {
				hasCalleeElement = true;
			}
		}
		return null;
	}

	/**
	 * Returns true if the caller's class name starts with any reserved package
	 * path.
	 * 
	 * @param aCaller The caller for which to test.
	 * 
	 * @return True in case the caller's package declaration starts with a
	 *         reserved package prefix.
	 */
	private static boolean hasReservedPackagePrefix( StackTraceElement aCaller ) {
		final String theClassName = aCaller.getClassName();
		for ( String ePrefix : RESERVED_PACKAGE_PREFIXES ) {
			if ( theClassName.startsWith( ePrefix ) ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns the best matching {@link StackTraceElement} belonging to the
	 * caller of the callee. The "best matching" {@link StackTraceElement} is
	 * considered to be the one not belonging to any "internal" API in a package
	 * namespace such as "com.sun.*", "java.*" or "javax.*". The callee can also
	 * be a package namespace where the matchee's must begin with the given
	 * package namespace.
	 * 
	 * @param aCalleeClassName The callee class name or package namespace which
	 *        wants to find out who called it.
	 * 
	 * @return The stack element of the caller of the callee or null if the
	 *         callee is not present or if there is no caller of the given
	 *         callee in the current stack trace.
	 */
	public static StackTraceElement toHeurisitcCallerStackTraceElement( String aCalleeClassName ) {
		final StackTraceElement theStackTraceElement = getCallerStackTraceElement( aCalleeClassName );
		StackTraceElement eStackTraceElement = theStackTraceElement;
		while ( eStackTraceElement != null && hasReservedPackagePrefix( eStackTraceElement ) ) {
			eStackTraceElement = getCallerStackTraceElement( eStackTraceElement );
		}
		return eStackTraceElement != null ? eStackTraceElement : theStackTraceElement;
	}

	/**
	 * Returns the best matching {@link StackTraceElement} belonging to the
	 * caller of the callee.The "best matching" {@link StackTraceElement} is
	 * considered to be the one not belonging to any "internal" API in a package
	 * namespace such as "com.sun.*", "java.*" or "javax.*".
	 * 
	 * @param aCallee The callee {@link StackTraceElement} which wants to find
	 *        out who called it.
	 * 
	 * @return The stack element of the caller of the callee or null if the
	 *         callee is not present or if there is no caller of the given
	 *         callee in the current stack trace.
	 */
	public static StackTraceElement toHeurisitcCallerStackTraceElement( StackTraceElement aCallee ) {
		return toHeurisitcCallerStackTraceElement( aCallee.getClassName() );
	}

	/**
	 * Returns the best matching {@link StackTraceElement} belonging to the
	 * caller of the callee.The "best matching" {@link StackTraceElement} is
	 * considered to be the one not belonging to any "internal" API in a package
	 * namespace such as "com.sun.*", "java.*" or "javax.*".
	 * 
	 * @param aCallee The callee {@link StackTraceElement} which wants to find
	 *        out who called it.
	 * 
	 * @return The stack element of the caller of the callee or null if the
	 *         callee is not present or if there is no caller of the given
	 *         callee in the current stack trace.
	 */
	public static StackTraceElement toHeurisitcCallerStackTraceElement( Class<?> aCallee ) {
		return toHeurisitcCallerStackTraceElement( aCallee.getName() );
	}

	/**
	 * Returns the best matching {@link StackTraceElement} belonging to the
	 * caller of one of the callees (in the order passed to the method).The
	 * "best matching" {@link StackTraceElement} is considered to be the one not
	 * belonging to any "internal" API in a package namespace such as
	 * "com.sun.*", "java.*" or "javax.*". The callees can also be a package
	 * namespace where the matchee's must begin with the given package
	 * namespace.
	 * 
	 * @param aCallees The callees names for which to find out who called it.
	 * 
	 * @return The stack element of the caller of the callee or null if the
	 *         callee is not present or if there is no caller of the given
	 *         callee in the current stack trace.
	 */
	public static StackTraceElement toHeurisitcCallerStackTraceElement( String[] aCallees ) {
		StackTraceElement eStackTraceElement;
		for ( String e : aCallees ) {
			eStackTraceElement = toHeurisitcCallerStackTraceElement( e );
			if ( eStackTraceElement != null ) {
				return eStackTraceElement;
			}
		}
		return null;
	}

	/**
	 * Returns the best matching {@link StackTraceElement} belonging to the
	 * caller of one of the callees (in the order passed to the method).The
	 * "best matching" {@link StackTraceElement} is considered to be the one not
	 * belonging to any "internal" API in a package namespace such as
	 * "com.sun.*", "java.*" or "javax.*".
	 * 
	 * @param aCallees The callees {@link StackTraceElement} instances for which
	 *        to find out who called it.
	 * 
	 * @return The stack element of the caller of the callee or null if the
	 *         callee is not present or if there is no caller of the given
	 *         callee in the current stack trace.
	 */
	public static StackTraceElement toHeurisitcCallerStackTraceElement( StackTraceElement[] aCallees ) {
		StackTraceElement eStackTraceElement;
		for ( StackTraceElement e : aCallees ) {
			eStackTraceElement = toHeurisitcCallerStackTraceElement( e );
			if ( eStackTraceElement != null ) {
				return eStackTraceElement;
			}
		}
		return null;
	}

	/**
	 * Returns the best matching {@link StackTraceElement} belonging to the
	 * caller of one of the callees (in the order passed to the method).The
	 * "best matching" {@link StackTraceElement} is considered to be the one not
	 * belonging to any "internal" API in a package namespace such as
	 * "com.sun.*", "java.*" or "javax.*".
	 * 
	 * @param aCallees The callees {@link Class} instances for which to find out
	 *        who called it.
	 * 
	 * @return The stack element of the caller of the callee or null if the
	 *         callee is not present or if there is no caller of the given
	 *         callee in the current stack trace.
	 */
	public static StackTraceElement toHeurisitcCallerStackTraceElement( Class<?>[] aCallees ) {
		StackTraceElement eStackTraceElement;
		for ( Class<?> e : aCallees ) {
			eStackTraceElement = toHeurisitcCallerStackTraceElement( e );
			if ( eStackTraceElement != null ) {
				return eStackTraceElement;
			}
		}
		return null;
	}

	/**
	 * Returns the type of the ({@link Class}) belonging to the direct caller of
	 * this method. When you use this method in your code, you get the
	 * {@link Class} of your method (invoking this method).
	 * 
	 * @return The type ({@link Class}) of the direct caller of this method.
	 */
	public static Class<?> getCallerType() {
		final StackTraceElement theStackTraceElement = getCallerStackTraceElement();
		return toClass( theStackTraceElement );
	}

	/**
	 * Returns the type of the ({@link Class}) belonging to the caller of the
	 * callee.
	 * 
	 * @param aCallee The callee class which wants to find out who called it.
	 * 
	 * @return The type ({@link Class}) of the caller of the caller of this
	 *         method.
	 */
	public static Class<?> getCallerType( Class<?> aCallee ) {
		final StackTraceElement theStackTraceElement = getCallerStackTraceElement( aCallee );
		return toClass( theStackTraceElement );
	}

	/**
	 * Same as {@link #getCallerType(Class)} with the difference that the passed
	 * callees are tried out one after the other until the first caller
	 * determined for a callee is returned.
	 *
	 * @param aCallees the callees
	 * 
	 * @return the caller type
	 */
	public static Class<?> getCallerType( Class<?>... aCallees ) {
		final StackTraceElement theStackTraceElement = getCallerStackTraceElement( aCallees );
		return toClass( theStackTraceElement );
	}

	/**
	 * Returns the type of the ({@link Class}) belonging to the caller of the
	 * callee.
	 * 
	 * @param aCallee The callee class which wants to find out who called it.
	 * 
	 * @return The type ({@link Class}) of the caller of the caller of this
	 *         method.
	 */
	public static Class<?> getCallerType( StackTraceElement aCallee ) {
		final StackTraceElement theStackTraceElement = getCallerStackTraceElement( aCallee );
		return toClass( theStackTraceElement );
	}

	/**
	 * Same as {@link #getCallerType(String)} with the difference that the
	 * passed callees are tried out one after the other until the first caller
	 * determined for a callee is returned.
	 *
	 * @param aCallees the callees
	 * 
	 * @return the caller type
	 */
	public static Class<?> getCallerType( String... aCallees ) {
		final StackTraceElement theStackTraceElement = getCallerStackTraceElement( aCallees );
		return toClass( theStackTraceElement );
	}

	/**
	 * Returns the type of the ({@link Class}) belonging to the caller of the
	 * callee. The callee can also be a package namespace where the matchee's
	 * must begin with the given package namespace.
	 * 
	 * @param aCallee The callee class which wants to find out who called it.
	 * 
	 * @return The type ({@link Class}) of the caller of the caller of this
	 *         method.
	 */
	public static Class<?> getCallerType( String aCallee ) {
		final StackTraceElement theStackTraceElement = getCallerStackTraceElement( aCallee );
		return toClass( theStackTraceElement );
	}

	/**
	 * Same as {@link #getCallerType(StackTraceElement)} with the difference
	 * that the passed callees are tried out one after the other until the first
	 * caller determined for a callee is returned.
	 *
	 * @param aCallees the callees
	 * 
	 * @return the caller type
	 */
	public static Class<?> getCallerType( StackTraceElement... aCallees ) {
		final StackTraceElement theStackTraceElement = getCallerStackTraceElement( aCallees );
		return toClass( theStackTraceElement );
	}

	/**
	 * Retrieves the {@link Class} type to which the {@link StackTraceElement}
	 * belongs.
	 * 
	 * @param aStackTraceElement The {@link StackTraceElement} for which to get
	 *        the according {@link Class}.
	 * 
	 * @return The type ({@link Class}) of the according
	 *         {@link StackTraceElement}.
	 */
	public static Class<?> toClass( StackTraceElement aStackTraceElement ) {
		try {
			return Class.forName( aStackTraceElement.getClassName() );
		}
		catch ( ClassNotFoundException e ) {
			throw new InternalError( e );
		}
	}

	/**
	 * Retrieves the method name from a {@link StackTraceElement}.
	 * 
	 * @param aStackTraceElement The {@link StackTraceElement} from which to
	 *        retrieve the method name.
	 * 
	 * @return The method name or null in case the {@link StackTraceElement} was
	 *         null.
	 */
	public static String toMethodName( StackTraceElement aStackTraceElement ) {
		if ( aStackTraceElement == null ) {
			return null;
		}
		return aStackTraceElement.getMethodName();
	}

	/**
	 * Returns the class name part from a {@link StackTraceElement}. Retrieves
	 * the fully qualified class name from a {@link StackTraceElement}.
	 * 
	 * @param aStackTraceElement The {@link StackTraceElement} from which to
	 *        retrieve the class name.
	 * 
	 * @return The class name without the package declaration or null in case
	 *         the {@link StackTraceElement} was null.
	 */
	public static String toClassName( StackTraceElement aStackTraceElement ) {
		if ( aStackTraceElement == null ) {
			return null;
		}
		return toClassName( aStackTraceElement.getClassName() );
	}

	/**
	 * Retrieves the fully qualified class name from a
	 * {@link StackTraceElement}.
	 * 
	 * @param aStackTraceElement The {@link StackTraceElement} from which to
	 *        retrieve the fully qualified class name.
	 * 
	 * @return The fully qualified class name or null in case the stack trace
	 *         element was null.
	 */
	public static String toFullyQualifiedClassName( StackTraceElement aStackTraceElement ) {
		if ( aStackTraceElement == null ) {
			return null;
		}
		return aStackTraceElement.getClassName();
	}

	/**
	 * Retrieves the fully qualified method name from a
	 * {@link StackTraceElement}. This adds the method name to the fully
	 * qualified path name separated by a hash "#".
	 * 
	 * @param aStackTraceElement The {@link StackTraceElement} from which to
	 *        retrieve the fully qualified method name.
	 * 
	 * @return The fully qualified method name or null in case the stack trace
	 *         element was null.
	 */
	public static String toFullyQualifiedMethodName( StackTraceElement aStackTraceElement ) {
		if ( aStackTraceElement == null ) {
			return null;
		}
		return aStackTraceElement.getClassName() + Delimiter.METHOD_NAME.getChar() + aStackTraceElement.getMethodName();
	}

	/**
	 * Retrieves the fully qualified method name of the caller of this method.
	 * This adds the method name to the caller's fully qualified path name
	 * separated by a hash "#".
	 * 
	 * @return The fully qualified method name.
	 */
	public static String toFullyQualifiedClassName() {
		final StackTraceElement theStackTraceElement = getCallerStackTraceElement();
		return theStackTraceElement.getClassName();
	}

	/**
	 * Retrieves the fully qualified method name of the caller of this method.
	 * This adds the method name to the caller's fully qualified path name
	 * separated by a hash "#".
	 * 
	 * @return The fully qualified method name.
	 */
	public static String toFullyQualifiedMethodName() {
		final StackTraceElement theStackTraceElement = getCallerStackTraceElement();
		return theStackTraceElement.getClassName() + Delimiter.METHOD_NAME.getChar() + theStackTraceElement.getMethodName();
	}

	/**
	 * Retrieves the fully qualified method name of the caller of this method.
	 * This adds the method name to the caller's fully qualified path name
	 * separated by a hash "#".
	 * 
	 * @return The fully qualified method name.
	 */
	public static String toMethodName() {
		final StackTraceElement theStackTraceElement = getCallerStackTraceElement();
		return theStackTraceElement.getMethodName();
	}

	/**
	 * Retrieves the class name of the caller of this method without the fully
	 * qualified package name part.
	 * 
	 * @return The class name.
	 */
	public static String toClassName() {
		return toClassName( toFullyQualifiedClassName() );
	}

	/**
	 * Retrieves the fully qualified package name of the caller of this method
	 * without the class name part.
	 * 
	 * @return The fully qualified package name.
	 */
	public static String toFullyQualifiedPackageName() {
		return toFullyQualifiedPackageName( toFullyQualifiedClassName() );
	}

	/**
	 * Retrieves the fully qualified package name from a
	 * {@link StackTraceElement}.
	 * 
	 * @param aStackTraceElement The {@link StackTraceElement} from which to
	 *        retrieve the fully qualified package name.
	 * 
	 * @return The fully qualified package name.
	 */
	public static String toFullyQualifiedPackageName( StackTraceElement aStackTraceElement ) {
		return toFullyQualifiedPackageName( toFullyQualifiedClassName( aStackTraceElement ) );
	}

	/**
	 * Returns the class name part from a fully qualified class name (which has
	 * the fully qualified package name as part of its name).
	 * 
	 * @param aFullyQualifiedClassName The fully qualified class name.
	 * 
	 * @return The class name without the package declaration.
	 */
	public static String toClassName( String aFullyQualifiedClassName ) {
		String theClassName;
		final int theClassIndex = aFullyQualifiedClassName.lastIndexOf( '.' );
		if ( theClassIndex != -1 ) {
			theClassName = aFullyQualifiedClassName.substring( theClassIndex + 1 );
		}
		else {
			theClassName = aFullyQualifiedClassName;
		}
		final int theInnerClassIndex = theClassName.indexOf( '$' );
		if ( theInnerClassIndex != -1 ) {
			theClassName = theClassName.substring( theInnerClassIndex + 1 );
		}
		return theClassName;
	}

	/**
	 * Returns the fully qualified package name part from a fully qualified
	 * class name (which has the fully qualified package name as part of its
	 * name).
	 * 
	 * @param aFullyQualifiedClassName The fully qualified class name.
	 * 
	 * @return The fully qualified package name without the class name.
	 */
	public static String toFullyQualifiedPackageName( String aFullyQualifiedClassName ) {
		final int theIndex = aFullyQualifiedClassName.lastIndexOf( '.' );
		if ( theIndex != -1 ) {
			return aFullyQualifiedClassName.substring( 0, theIndex );
		}
		return "";
	}

	/**
	 * A {@link Cloneable} object cannot directly be cloned by casting it to be
	 * {@link Cloneable} :-( Thereforee this method does the job. Citation From
	 * Josh Bloch's Effective Java: "The {@link Cloneable} interface was
	 * intended as a mixin interface for objects to advertise that they permit
	 * cloning. Unfortunately it fails to serve this purpose ... This is a
	 * highly atypical use of interfaces and not one to be emulated ... In order
	 * for implementing the interface to have any effect on a class, it and all
	 * of its superclasses must obey a fairly complex, unenforceable and largely
	 * undocumented protocol"
	 *
	 * @param <T> the generic type
	 * @param aObj The object to be cloned.
	 * 
	 * @return The cloned object.
	 * 
	 * @throws CloneNotSupportedException in case the object cannot be cloned.
	 * 
	 * @see "http://stackoverflow.com/questions/1138769/why-is-the-clone-method-protected-in-java-lang-object"
	 */

	@SuppressWarnings("unchecked")
	public static <T> T toClone( T aObj ) throws CloneNotSupportedException {
		if ( aObj instanceof Cloneable ) {
			try {
				return (T) aObj.getClass().getMethod( "clone" ).invoke( aObj );
			}
			catch ( IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e ) {
				throw new CloneNotSupportedException( Trap.asMessage( e ) );
			}
		}
		else {
			throw new CloneNotSupportedException( "The type \"" + aObj.getClass().getName() + "}\" does not implement the \"" + Cloneable.class.getName() + "\" interface." );
		}
	}

	/**
	 * Creates a string of a super class's {@link Object#toString()} method and
	 * the provided "toString" text.
	 * 
	 * @param aToString The provided "toString" text.
	 * @param aSuperToString A super class's {@link Object#toString()} method's
	 *        {@link String}.
	 * 
	 * @return The "concatenated" and formatted new {@link String} to be
	 *         returned by an implementing class's {@link Object#toString()}
	 *         method.
	 */
	public static String toString( String aToString, String aSuperToString ) {
		return aToString + " (" + aSuperToString + ")";
	}

	/**
	 * Gatherers all annotations annotating a provided {@link Class}.
	 *
	 * @param aClass the {@link Class} for which to gather all annotations.
	 * 
	 * @return the {@link Set} containing all according annotations annotating
	 *         the given {@link Class}
	 */
	public static Set<Annotation> annotations( Class<?> aClass ) {
		return annotations( aClass.getAnnotations(), new HashSet<>() );
	}

	/**
	 * Gatherers all annotations annotating a provided {@link Field}.
	 *
	 * @param aField the {@link Field} for which to gather all annotations.
	 * 
	 * @return the {@link Set} containing all according annotations annotating
	 *         the given {@link Field}
	 */
	public static Set<Annotation> annotations( Field aField ) {
		return annotations( aField.getAnnotations(), new HashSet<>() );
	}

	/**
	 * Gatherers all annotations annotating a provided {@link Method}.
	 *
	 * @param aMethod the {@link Method} for which to gather all annotations.
	 * 
	 * @return the {@link Set} containing all according annotations annotating
	 *         the given {@link Method}
	 */
	public static Set<Annotation> annotations( Method aMethod ) {
		return annotations( aMethod.getAnnotations(), new HashSet<>() );
	}

	/**
	 * Gatherers all annotations annotating a provided {@link Object}.
	 *
	 * @param aObj the {@link Object} for which to gather all annotations.
	 * 
	 * @return the {@link Set} containing all according annotations annotating
	 *         the given {@link Object}
	 */
	public static Set<Annotation> annotations( Object aObj ) {
		return annotations( aObj.getClass().getAnnotations(), new HashSet<>() );
	}

	/**
	 * Seeks for an {@link Annotation} of the given type annotating a provided
	 * {@link Class}, also reckoning annotations nested inside annotations
	 * (recursively).
	 *
	 * @param aAnnotation the {@link Annotation} type for which to seek.
	 * @param aClass the {@link Class} which's annotations to crawl.
	 * 
	 * @return the {@link Annotation} annotating the given {@link Class} or null
	 *         if none was found
	 */
	public static Annotation findAnnotation( Class<? extends Annotation> aAnnotation, Class<?> aClass ) {
		return findAnnotation( aAnnotation, aClass.getAnnotations(), new HashSet<>() );
	}

	/**
	 * Seeks for an {@link Annotation} of the given type annotating a provided
	 * {@link Field}, also reckoning annotations nested inside annotations
	 * (recursively).
	 *
	 * @param aAnnotation the {@link Annotation} type for which to seek.
	 * @param aField the {@link Field} which's annotations to crawl.
	 * 
	 * @return the {@link Annotation} annotating the given {@link Field} or null
	 *         if none was found
	 */
	public static Annotation findAnnotation( Class<? extends Annotation> aAnnotation, Field aField ) {
		return findAnnotation( aAnnotation, aField.getAnnotations(), new HashSet<>() );
	}

	/**
	 * Seeks for an {@link Annotation} of the given type annotating a provided
	 * {@link Method}, also reckoning annotations nested inside annotations
	 * (recursively).
	 *
	 * @param aAnnotation the {@link Annotation} type for which to seek.
	 * @param aMethod the {@link Method} which's annotations to crawl.
	 * 
	 * @return the {@link Annotation} annotating the given {@link Method} or
	 *         null if none was found
	 */
	public static Annotation findAnnotation( Class<? extends Annotation> aAnnotation, Method aMethod ) {
		return findAnnotation( aAnnotation, aMethod.getAnnotations(), new HashSet<>() );
	}

	/**
	 * Seeks for an {@link Annotation} of the given type annotating a provided
	 * {@link Object}, also reckoning annotations nested inside annotations
	 * (recursively).
	 *
	 * @param aAnnotation the {@link Annotation} type for which to seek.
	 * @param aObj the {@link Object} which's annotations to crawl.
	 * 
	 * @return the {@link Annotation} annotating the given {@link Object} or
	 *         null if none was found
	 */
	public static Annotation findAnnotation( Class<? extends Annotation> aAnnotation, Object aObj ) {
		return findAnnotation( aAnnotation, aObj.getClass().getAnnotations(), new HashSet<>() );
	}

	/**
	 * Tests if an {@link Annotation} of the given type annotates a provided
	 * {@link Class}, also reckoning annotations nested inside annotations
	 * (recursively).
	 *
	 * @param aAnnotation the {@link Annotation} type for which to seek.
	 * @param aClass the {@link Class} which's annotations to crawl.
	 * 
	 * @return True in case the {@link Annotation} annotates the given
	 *         {@link Class}.
	 */
	public static boolean hasAnnotation( Class<? extends Annotation> aAnnotation, Class<?> aClass ) {
		return hasAnnotation( aAnnotation, aClass.getAnnotations(), new HashSet<>() );
	}

	/**
	 * Tests if an {@link Annotation} of the given type annotates a provided
	 * {@link Field}, also reckoning annotations nested inside annotations
	 * (recursively).
	 *
	 * @param aAnnotation the {@link Annotation} type for which to seek.
	 * @param aField the {@link Field} which's annotations to crawl.
	 * 
	 * @return True in case the {@link Annotation} annotates the given
	 *         {@link Field}.
	 */
	public static boolean hasAnnotation( Class<? extends Annotation> aAnnotation, Field aField ) {
		return hasAnnotation( aAnnotation, aField.getAnnotations(), new HashSet<>() );
	}

	/**
	 * Tests if an {@link Annotation} of the given type annotates a provided
	 * {@link Method}, also reckoning annotations nested inside annotations
	 * (recursively).
	 *
	 * @param aAnnotation the {@link Annotation} type for which to seek.
	 * @param aMethod the {@link Method} which's annotations to crawl.
	 * 
	 * @return True in case the {@link Annotation} annotates the given
	 *         {@link Method}.
	 */
	public static boolean hasAnnotation( Class<? extends Annotation> aAnnotation, Method aMethod ) {
		return hasAnnotation( aAnnotation, aMethod.getAnnotations(), new HashSet<>() );
	}

	/**
	 * Tests if an {@link Annotation} of the given type annotates a provided
	 * {@link Object}, also reckoning annotations nested inside annotations
	 * (recursively).
	 *
	 * @param aAnnotation the {@link Annotation} type for which to seek.
	 * @param aObj the {@link Object} which's annotations to crawl.
	 * 
	 * @return True in case the {@link Annotation} annotates the given
	 *         {@link Object}.
	 */
	public static boolean hasAnnotation( Class<? extends Annotation> aAnnotation, Object aObj ) {
		return hasAnnotation( aAnnotation, aObj.getClass().getAnnotations(), new HashSet<>() );
	}

	/**
	 * This method tests whether the given java beans getter attribute is found
	 * for the given object.
	 *
	 * @param anObject The object which is to be tested.
	 * @param anAttributeName The attribute name.
	 * @param aReturnType The expected (sub-) aClass of the return value
	 * 
	 * @return True if the object has a method with the given java beans name
	 *         which returns the given (super-) aClass.
	 */
	public static boolean hasGetterAttribute( Object anObject, String anAttributeName, Class<?> aReturnType ) {

		String theMethodName;
		final Method[] theMethodArray = anObject.getClass().getMethods();

		try {
			// ------------------------
			// Look at all the methods:
			// ------------------------
			for ( Method aTheMethodArray : theMethodArray ) {
				// Look at the signature: 0 arguments, given return aClass:
				// ------------------------------------------------------
				if ( ( aTheMethodArray.getParameterTypes().length == 0 ) && ( aTheMethodArray.getReturnType().equals( aReturnType ) ) ) {

					theMethodName = aTheMethodArray.getName();

					if ( ( aTheMethodArray.getModifiers() == Modifier.PUBLIC ) || ( aTheMethodArray.getModifiers() == 9 ) ) {

						// -------------------
						// Find 'get' methods:
						// -------------------
						if ( ( theMethodName.indexOf( ALIAS_GET ) == 0 ) && Character.isUpperCase( theMethodName.charAt( ALIAS_GET.length() ) ) ) {
							theMethodName = theMethodName.substring( ALIAS_GET.length(), theMethodName.length() );
							// ------------------
							// Bean method found:
							// ------------------
							if ( anAttributeName.equalsIgnoreCase( theMethodName ) ) {
								return true;
							}
						}
						// -------------------
						// Find 'has' methods:
						// -------------------
						else if ( ( theMethodName.indexOf( ALIAS_HAS ) == 0 ) && Character.isUpperCase( theMethodName.charAt( ALIAS_HAS.length() ) ) ) {
							theMethodName = theMethodName.substring( ALIAS_HAS.length(), theMethodName.length() );
							// ------------------
							// Bean method found:
							// ------------------
							if ( anAttributeName.equalsIgnoreCase( theMethodName ) ) {
								return true;
							}
						}
						// -------------------
						// Find 'is' methods:
						// -------------------
						else if ( ( theMethodName.indexOf( ALIAS_IS ) == 0 ) && Character.isUpperCase( theMethodName.charAt( ALIAS_IS.length() ) ) ) {
							theMethodName = theMethodName.substring( ALIAS_IS.length(), theMethodName.length() );
							// ------------------
							// Bean method found:
							// ------------------
							if ( anAttributeName.equalsIgnoreCase( theMethodName ) ) {
								return true;
							}
						}
						// ------------------------
						// Find 'toString' methods:
						// ------------------------
						else if ( ( theMethodName.indexOf( ALIAS_TOSTRING ) == 0 ) && Character.isUpperCase( theMethodName.charAt( ALIAS_TOSTRING.length() ) ) ) {
							theMethodName = theMethodName.substring( ALIAS_TOSTRING.length(), theMethodName.length() );
							// ------------------
							// Bean method found:
							// ------------------
							if ( anAttributeName.equalsIgnoreCase( theMethodName ) ) {
								return true;
							}
						}
					}
				}
			}
		}
		catch ( Exception exc ) {
			System.err.println( "ReflectionUtility.setAttribute(): Caught an exception of aClass <" + exc.getClass().getName() + "> with message = " + exc.getMessage() + "." );
			exc.printStackTrace();
		}
		return false;
	}

	/**
	 * This method tests whether the given java beans setter attribute is found
	 * for the given object.
	 *
	 * @param anObject The object which is to be tested.
	 * @param anAttributeName The attribute name.
	 * @param anAttributeType The expected (sub-) aClass of the attribute
	 * 
	 * @return True if the object has a method with the given java beans name
	 *         which can be called with an argument of the given aClass.
	 */
	public static boolean hasSetterAttribute( Object anObject, String anAttributeName, Class<?> anAttributeType ) {
		String theMethodName;
		final Method[] theMethodArray = anObject.getClass().getMethods();

		try {
			// ------------------------
			// Look at all the methods:
			// ------------------------
			for ( Method aTheMethodArray : theMethodArray ) {
				// Look at the signature: 1 argument, return aClass == void:
				// -------------------------------------------------------
				if ( ( aTheMethodArray.getParameterTypes().length == 1 ) && ( aTheMethodArray.getParameterTypes()[0].isAssignableFrom( anAttributeType ) ) && ( aTheMethodArray.getReturnType().equals( Void.TYPE ) ) ) {

					theMethodName = aTheMethodArray.getName();

					if ( ( aTheMethodArray.getModifiers() == Modifier.PUBLIC ) || ( aTheMethodArray.getModifiers() == 9 ) ) {

						// --------------------
						// Find setter methods:
						// --------------------
						if ( ( theMethodName.indexOf( ALIAS_SET ) == 0 ) && Character.isUpperCase( theMethodName.charAt( ALIAS_SET.length() ) ) ) {
							theMethodName = theMethodName.substring( ALIAS_SET.length(), theMethodName.length() );
							// ------------------
							// Bean method found:
							// ------------------
							if ( anAttributeName.equalsIgnoreCase( theMethodName ) ) {
								return true;
							}
						}
					}
				}
			}
		}
		catch ( Exception exc ) {
			System.err.println( "ReflectionUtility.setAttribute(): Caught an exception of aClass <" + exc.getClass().getName() + "> with message = " + exc.getMessage() + "." );
			exc.printStackTrace();
		}
		return false;
	}

	/**
	 * This method sets a java beans attribute for the given object.
	 * 
	 * @param anObject The object which's attribute is to be set.
	 * @param anAttributeValueStruct The name-to-value struct for the attrubute
	 *        to be set.
	 * 
	 * @exception NoSuchMethodException Description of the Exception
	 */
	public static void setAttribute( Object anObject, Attribute anAttributeValueStruct ) throws NoSuchMethodException {
		setAttribute( anObject, anAttributeValueStruct.getKey(), anAttributeValueStruct.getValue() );
	}

	/**
	 * This method sets a java beans attribute for the given object. The
	 * corresponding java beans method must begin with a 'set', the following
	 * letter must be in upper case and it must only take one argument being of
	 * the same (or super-) aClass as the attribute's aClass.
	 * 
	 * @param anObject The object which's java beans method is to be called.
	 * @param anAttributeName The attribute name of the java beans method.
	 * @param anAttributeValue The value of the attribute to be set.
	 * 
	 * @exception NoSuchMethodException Description of the Exception
	 */
	public static void setAttribute( Object anObject, String anAttributeName, Object anAttributeValue ) throws NoSuchMethodException {

		String theMethodName;
		boolean isCalled = false;
		final Method[] theMethodArray = anObject.getClass().getMethods();

		try {
			// ------------------------
			// Look at all the methods:
			// ------------------------
			for ( Method aTheMethodArray : theMethodArray ) {
				// Look at the signature: 1 argument, return aClass == void:
				// -------------------------------------------------------
				if ( ( aTheMethodArray.getParameterTypes().length == 1 ) && ( aTheMethodArray.getParameterTypes()[0].isAssignableFrom( anAttributeValue.getClass() ) ) && ( aTheMethodArray.getReturnType().equals( Void.TYPE ) ) ) {
					theMethodName = aTheMethodArray.getName();

					if ( ( aTheMethodArray.getModifiers() == Modifier.PUBLIC ) || ( aTheMethodArray.getModifiers() == 9 ) ) {
						// --------------------
						// Find setter methods:
						// --------------------
						if ( ( theMethodName.indexOf( ALIAS_SET ) == 0 ) && Character.isUpperCase( theMethodName.charAt( ALIAS_SET.length() ) ) ) {
							theMethodName = theMethodName.substring( ALIAS_SET.length(), theMethodName.length() );
							if ( anAttributeName.equalsIgnoreCase( theMethodName ) ) {
								try {
									// ----------------
									// Call the method:
									// ----------------
									aTheMethodArray.invoke( anObject, new Object[] { anAttributeValue } );
									isCalled = true;
								}
								catch ( Exception e ) {
									System.err.println( "ReflectionUtility.setAttribute(): Caught an exception of aClass <" + e.getClass().getName() + "> while invoking the method <" + theMethodName + "> with message = " + e.getMessage() + "." );
									e.printStackTrace();
									break;
								}
								break;
							}
						}
					}
				}
			}
		}
		catch ( Exception exc ) {
			System.err.println( "ReflectionUtility.setAttribute(): Caught an exception of aClass <" + exc.getClass().getName() + "> with message = " + exc.getMessage() + "." );
			exc.printStackTrace();
		}

		if ( !isCalled ) {
			throw new NoSuchMethodException( "ReflectionUtility.setAttribute(): The method for the attribute <" + anAttributeName + "> for the class <" + anObject.getClass().getName() + "> cannot be found." );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Contains dir.
	 *
	 * @param aDir the dir
	 * @param aDirs the dirs
	 * 
	 * @return true, if successful
	 */
	@SafeVarargs
	protected static boolean containsDir( File aDir, List<File>... aDirs ) {
		for ( List<File> eDirs : aDirs ) {
			if ( eDirs.contains( aDir ) ) {
				return true;
			}
		}
		return false;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static boolean hasTestAnnotation( StackTraceElement aElement ) {
		final Class<?> aClass;
		try {
			aClass = Class.forName( aElement.getClassName() );

			final String methodName = aElement.getMethodName();
			Annotation[] eAnnotations;
			final Method[] methods = aClass.getMethods();
			for ( Method method : methods ) {
				if ( method.getName().equals( methodName ) ) {
					eAnnotations = method.getDeclaredAnnotations();
					for ( Annotation eAnnotation : eAnnotations ) {
						final String theSimpleName = eAnnotation.annotationType().getSimpleName();
						if ( "Test".equals( theSimpleName ) ) {
							return true;
						}
					}
				}
			}
		}
		catch ( ClassNotFoundException ignore ) {}
		return false;
	}

	private static boolean isSkipStackTraceElement( StackTraceElement aStackTraceElement ) {
		return aStackTraceElement.getClassName().equals( Thread.class.getName() ) || aStackTraceElement.getClassName().equals( Execution.class.getName() ) || aStackTraceElement.getLineNumber() <= 1;
	}

	private static PrintStream toAnsiOut() {
		if ( !_isAnsi ) {
			return System.out;
		}
		doInstallAnsi();
		return new PrintStream( AnsiWrapper.toAnsiOut() ) {
			@Override
			public void close() {
				// Do not close me in auto close try() !
			};
		};
	}

	private static PrintStream toAnsiErr() {
		if ( !_isAnsi ) {
			return System.err;
		}
		doInstallAnsi();
		return new PrintStream( AnsiWrapper.toAnsiErr() ) {
			@Override
			public void close() {
				// Do not close me in auto close try() !
			};
		};
	}

	private static void doInstallAnsi() {
		if ( _isAnsi ) {
			if ( !_isAnsiInstalled ) {
				synchronized ( Execution.class ) {
					if ( !_isAnsiInstalled ) {
						AnsiWrapper.systemInstall();
						try {
							final Thread theHook = new Thread( () -> {
								try {
									doUninstallAnsi();
								}
								catch ( Exception | Error ignore ) {}
							} );
							theHook.setDaemon( true );
							Execution.addShutdownHook( theHook );
						}
						catch ( Exception ignore ) {}
						_isAnsiInstalled = true;
					}
				}
			}
		}
	}

	private static void doUninstallAnsi() {
		if ( !_isAnsi ) {
			return;
		}
		try {
			// Min-Loop-Time for any asynchronous log to do its job |-->
			Thread.sleep( SleepLoopTime.MIN.getTimeMillis() );
			// Min-Loop-Time for any asynchronous log to do its job <--|
		}
		catch ( InterruptedException ignore ) {}
		toSystemOut().flush();
		toSystemErr().flush();
		AnsiWrapper.systemUninstall();
	}

	/**
	 * This method uses reflection on order to analyze a given object. The java
	 * beans attributes and their values are retrieved and returned in an array
	 * of name-to-value pairs.
	 * 
	 * @param anObject The object to be analyzed.
	 * 
	 * @return An array of objects containing the java beans name-to-value
	 *         pairs.
	 */
	public static Attribute[] toBeanAttributes( Object anObject ) {
		String eMethodName;
		boolean isBeanMethod;
		Object eInvokeReturn;
		final ArrayList<Attribute> theAttributeValueStructList = new ArrayList<>();
		final Method[] theMethodArray = anObject.getClass().getMethods();

		try {
			// ------------------------
			// Look at all the methods:
			// ------------------------
			for ( Method aTheMethodArray : theMethodArray ) {
				// Look at the signature: 0 arguments, return aClass != void:
				// --------------------------------------------------------
				if ( ( aTheMethodArray.getParameterTypes().length == 0 ) && ( !aTheMethodArray.getReturnType().equals( Void.TYPE ) ) ) {
					isBeanMethod = false;
					eInvokeReturn = NON_EXISTING_VALUE;
					eMethodName = aTheMethodArray.getName();

					// --------------
					// Public method?
					// --------------
					if ( ( aTheMethodArray.getModifiers() == Modifier.PUBLIC ) || ( aTheMethodArray.getModifiers() == 9 ) ) {

						// ---------------
						// Getter methods?
						// ---------------
						if ( ( eMethodName.indexOf( ALIAS_GET ) == 0 ) && Character.isUpperCase( eMethodName.charAt( ALIAS_GET.length() ) ) ) {
							isBeanMethod = true;
							eMethodName = eMethodName.substring( ALIAS_GET.length(), eMethodName.length() );
						}
						else if ( ( eMethodName.indexOf( ALIAS_HAS ) == 0 ) && Character.isUpperCase( eMethodName.charAt( ALIAS_HAS.length() ) ) ) {
							isBeanMethod = true;
							eMethodName = eMethodName.substring( ALIAS_HAS.length(), eMethodName.length() );
						}
						else if ( ( eMethodName.indexOf( ALIAS_IS ) == 0 ) && Character.isUpperCase( eMethodName.charAt( ALIAS_IS.length() ) ) ) {
							isBeanMethod = true;
							eMethodName = eMethodName.substring( ALIAS_IS.length(), eMethodName.length() );
						}
						else if ( eMethodName.equals( ALIAS_TOSTRING ) ) {
							isBeanMethod = true;
						}

						// ------------------
						// Bean method found?
						// ------------------
						if ( isBeanMethod ) {

							try {
								// ------------------
								// Invoke the method:
								// ------------------
								eInvokeReturn = aTheMethodArray.invoke( anObject, new Object[] {} );
							}
							catch ( Exception e ) {
								System.err.println( "ReflectionUtility.setAttribute(): Caught an exception of aClass <" + e.getClass().getName() + "> while invoking the method <" + eMethodName + "> with message = " + e.getMessage() + "." );
								e.printStackTrace();
							}

							theAttributeValueStructList.add( new AttributeImpl( eMethodName, eInvokeReturn ) );
						}
					}
				}
			}

		}
		catch ( Exception exc ) {
			System.err.println( "ReflectionUtility.setAttribute(): Caught an exception of aClass <" + exc.getClass().getName() + "> with message = " + exc.getMessage() + "." );
			exc.printStackTrace();
		}

		return theAttributeValueStructList.toArray( new AttributeImpl[theAttributeValueStructList.size()] );
	}

	/**
	 * Try to skip any logging framework's factory or whatsoever
	 * {@link StackTraceElement} instances.
	 * 
	 * @param aCallees The callees for which to skip the unwanted
	 *        {@link StackTraceElement} instances.
	 * 
	 * @return A non logging framework {@link StackTraceElement} or the one
	 *         passed.
	 */
	public static StackTraceElement probeTillNoneLoggerElement( Class<?>... aCallees ) {
		StackTraceElement theCaller = Execution.toHeurisitcCallerStackTraceElement( aCallees );
		StackTraceElement eCaller = theCaller;
		while ( eCaller != null && containsLoggerPackagePart( eCaller ) ) {
			eCaller = Execution.toHeurisitcCallerStackTraceElement( eCaller );
		}
		if ( eCaller != null ) {
			theCaller = eCaller;
		}
		return theCaller;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns true if the caller's class name contains any logger related
	 * package name part in its package path.
	 * 
	 * @param aCaller The caller for which to test.
	 * 
	 * @return True in case the caller's package declaration contains any logger
	 *         package name part.
	 */
	private static boolean containsLoggerPackagePart( StackTraceElement aCaller ) {
		final String theClassName = aCaller.getClassName();
		for ( String ePart : LOGGER_PACKAGE_PARTS ) {
			if ( theClassName.contains( ePart ) ) {
				return true;
			}
		}
		return false;
	}

	private static Set<Annotation> annotations( Annotation[] aAnnotations, Set<Annotation> aVisted ) {
		for ( Annotation eAnno : aAnnotations ) {
			if ( !aVisted.contains( eAnno ) ) {
				aVisted.add( eAnno );
				annotations( eAnno.annotationType().getAnnotations(), aVisted );
			}
		}
		return aVisted;
	}

	private static Annotation findAnnotation( Class<? extends Annotation> aAnnotation, Annotation[] aAnnotations, Set<Annotation> aVisted ) {
		for ( Annotation eAnno : aAnnotations ) {
			if ( !aVisted.contains( eAnno ) ) {
				if ( aAnnotation.isAssignableFrom( eAnno.getClass() ) ) {
					return eAnno;
				}
				aVisted.add( eAnno );
				final Annotation result = findAnnotation( aAnnotation, eAnno.annotationType().getAnnotations(), aVisted );
				if ( result != null ) {
					return result;
				}
			}
		}
		return null;
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	private static boolean hasAnnotation( Class<? extends Annotation> aAnnotation, Annotation[] aAnnotations, Set<Annotation> aVisted ) {
		for ( Annotation eAnno : aAnnotations ) {
			if ( !aVisted.contains( eAnno ) ) {
				if ( aAnnotation.isAssignableFrom( eAnno.getClass() ) ) {
					return true;
				}
				aVisted.add( eAnno );
				if ( hasAnnotation( aAnnotation, eAnno.annotationType().getAnnotations(), aVisted ) ) {
					return true;
				}
			}
		}
		return false;
	}
}
