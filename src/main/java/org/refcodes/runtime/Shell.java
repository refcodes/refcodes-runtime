// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.runtime;

import org.refcodes.mixin.DetectedAccessor;

/**
 * Enumeration with the (relevant) shells (command line interpreters).
 *
 * @author steiner
 */
public enum Shell implements DetectedAccessor {
	/** Unix / Linux / BSD like shell. */
	SHELL,
	/**
	 * MS Windows WIN_CMD.EXE
	 */
	WIN_CMD,
	/** MS Windows PowerShell:. */
	POWER_SHELL,

	NONE,

	UNKNOWN;

	private static final String CMD_EXE = "cmd.exe";

	private static final String COM_SPEC = "ComSpec";

	/**
	 * Tries to determine the command line interpreter (CLI) - if any - this
	 * process is running in, e.g. whether we are running in a DOS console
	 * environment or a Linux alike Shell.
	 * 
	 * @return The CLI type, in case we are in a DOS / WIN_CMD console
	 *         environment, {@link Shell#WIN_CMD} is returned (for example).
	 */
	public static Shell toShell() {
		if ( Terminal.isCygwinTerminal() ) {
			return Shell.SHELL;
		}
		final boolean hasConsole = System.console() != null;
		switch ( OperatingSystem.toOperatingSystem() ) {
		case WINDOWS:
			if ( hasConsole ) {
				final String theComSpec = System.getenv( COM_SPEC );
				if ( theComSpec != null && theComSpec.endsWith( CMD_EXE ) ) {
					return Shell.WIN_CMD;
				}
				return Shell.POWER_SHELL;
			}
			break;
		case UNIX:
			if ( hasConsole ) {
				return Shell.SHELL;
			}
		default:
		}
		return hasConsole ? Shell.UNKNOWN : Shell.NONE;
	}

	/**
	 * Determines whether this enumeration represents the current {@link Shell}
	 * (e.g. this {@link Shell} has be detected to be running your application).
	 * 
	 * @return True in case this enumeration (most probably) represents this
	 *         session's {@link Shell}.
	 */
	@Override
	public boolean isDetected() {
		return toShell() == this;
	}
}