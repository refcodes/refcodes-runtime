// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.runtime;

import java.io.OutputStreamWriter;
import java.util.function.IntSupplier;

import org.refcodes.data.ConsoleDimension;
import org.refcodes.data.DaemonLoopSleepTime;
import org.refcodes.data.Encoding;
import org.refcodes.exception.UnhandledEnumBugException;
import org.refcodes.mixin.AliasAccessor;
import org.refcodes.mixin.DetectedAccessor;
import org.refcodes.numerical.NumericalUtility;

/**
 * Enumeration with the (relevant) terminals as well as interpolated terminal
 * metrics.
 */
public enum Terminal implements AliasAccessor, DetectedAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// IDENTIFIERS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * xterm: a generic terminal emulator for the X Window System.
	 */
	XTERM("xterm"),

	/**
	 * cygwin: The cygwin terminal on Windows.
	 */
	CYGWIN("cygwin"),

	/**
	 * vt100: a classic DEC terminal
	 */
	VT100("vt100"),

	/**
	 * linux: the console terminal in a Linux system.
	 */
	LINUX("linux"),

	/**
	 * rxvt:a terminal emulator for the X Window System.
	 */
	RXVT("rxvt"),

	/**
	 * ansi: a terminal using the ANSI escape codes for color and formatting.
	 */
	ANSI("ansi"),

	/**
	 * xterm-256color: an extended version of xterm with support for 256 colors.
	 */
	XTERM_256COLOR("xterm-256color"),

	/**
	 * vt220: a popular terminal from Digital Equipment Corporation (DEC).
	 */
	VT220("vt220"),

	/**
	 * gnome-terminal: the default terminal emulator for the GNOME desktop
	 * environment.
	 */
	GNOME_TERMINAL("gnome-terminal"),

	/**
	 * konsole: the default terminal emulator for the KDE desktop environment.
	 */
	KONSOLE("konsole"),

	/**
	 * screen: a full-screen window manager that multiplexes a physical terminal
	 * between several processes.
	 */
	SCREEN("screen"),

	/**
	 * tmux: a terminal multiplexer that allows multiple terminals to be created
	 * and managed from a single console or SSH session.
	 */
	TMUX("tmux"),

	/**
	 * Unknown, not one of the others.
	 */
	UNKNOWN(null);

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The {@link CharSetCapability} defines three categories of character
	 * capabilities as observed on various platform. The method
	 * {@link Terminal#toCharSetCapability()} tries to determine at best will
	 * the capabilities of the current terminal session.
	 *
	 */
	public enum CharSetCapability {

		/**
		 * Congratulations, the terminal seems to be capable of unicode (UTF-8).
		 */
		UNICODE,

		/**
		 * A Windows terminal without (full) UTF-8 support. Make sure you also
		 * take {@link Terminal#getLineBreak()} into account as well as the odd
		 * line break behavior when being at the end of a line.
		 */
		WINDOWS,

		/**
		 * The least common denominator which the current terminal session seems
		 * to be capable of.
		 */
		ASCII
	}

	private static Boolean _isAnsiTerminal = null;
	private static Boolean _isCygwinTerminal = null;
	private static Integer _terminalHeight = null;
	private static Integer _terminalWidth = null;
	private static long _lastTerminalHeightUpdate = -1;
	private static long _lastTerminalWidthUpdate = -1;
	private static boolean _canTput = true;
	private static boolean _canBashTput = true;
	private static boolean _canModeCon = true;
	private static boolean _canCmdModeCon = true;
	private static boolean _canPowershell = true;
	private static boolean _canStty = true;
	private static boolean _canBashStty = true;
	private static boolean _canNativeWindows = true;
	private static boolean _canBashTputExe = true;
	private static boolean _canTputExe = true;
	private static boolean _canBashSttyExe = true;
	private static boolean _canSttyExe = true;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int WIDTH_ABOVE_UNREALSITIC = 1024;
	private static final int HEIGHT_ABOVE_UNREALSITIC = 256;
	private static final int PRESET_TERMINAL_HEIGHT = 25;
	private static final int PRESET_TERMINAL_WIDTH = 80;
	private static final int PRESET_WINCMD_WIDTH = 120;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _alias;

	private static final boolean _isAnsi = System.console() != null && isAnsiTerminalEnabled(); // Jansi crashes when run without console (javaw), see https://github.com/fusesource/jansi/issues/216

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private Terminal( String aAlias ) {
		_alias = aAlias;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the alias by which this {@link Terminal} type is known.
	 * 
	 * @return The according {@link Terminal} types alias.
	 */
	@Override
	public String getAlias() {
		return _alias;
	}

	/**
	 * Determines the operating system as of
	 * {@link OperatingSystem#toOperatingSystem()} and in case a
	 * {@link OperatingSystem#WINDOWS} is being detected, then \r\n" (CRLF) is
	 * returned, else "\n" (LF) is returned. Can be overridden with the
	 * {@link SystemProperty#CONSOLE_LINE_BREAK}
	 * (<code>java -Dconsole.lineBreak=...</code>) and the
	 * {@link EnvironmentVariable#CONSOLE_LINE_BREAK}
	 * ("<code>export CONSOLE_LINE_BREAK=...</code>).
	 * 
	 * @return The operating system's specific line break; on Windows it is
	 *         "\r\n" (CRLF) and on all other operating systems it is "\n" (LF).
	 */
	public static String getLineBreak() {
		String theLineBreak = System.lineSeparator();
		if ( theLineBreak == null || theLineBreak.isEmpty() ) {
			SystemProperty.CONSOLE_LINE_BREAK.getValue();
			if ( theLineBreak == null || theLineBreak.isEmpty() ) {
				theLineBreak = EnvironmentVariable.CONSOLE_LINE_BREAK.getValue();
			}
		}
		if ( theLineBreak == null || theLineBreak.isEmpty() ) {
			theLineBreak = OperatingSystem.toOperatingSystem() == OperatingSystem.WINDOWS ? "\r\n" : "\n";
		}
		return theLineBreak;
	}

	/**
	 * Determines the encoding of the system (system's console) in use as of
	 * {@link System#out}.
	 * 
	 * @return The encoding (of the console).
	 */
	public static String getEncoding() {
		final String theEncoding = EnvironmentVariable.TERMINAL_ENCODING.getValue();
		if ( theEncoding != null && theEncoding.length() != 0 ) {
			return theEncoding;
		}
		final OutputStreamWriter theOutWriter = new OutputStreamWriter( System.out );
		return theOutWriter.getEncoding();
	}

	/**
	 * Determines the height in characters of the system's terminal in use.
	 * 
	 * @return The height of the terminal in characters or -1 if the height
	 *         cannot be determined.
	 */
	public static int getHeight() {
		return getHeight( -1 );
	}

	/**
	 * Determines the width in characters of the system's terminal in use.
	 * 
	 * @return The width of the terminal in characters or -1 if the width cannot
	 *         be determined.
	 */
	public static int getWidth() {
		return getWidth( -1 );
	}

	/**
	 * Determines the current session's terminal in use. Known terminal types
	 * can be retrieved via {@link #toTerminal()}.
	 * 
	 * @return The current session's terminal in use.
	 */
	public static String getTerminalSession() {
		return EnvironmentVariable.TERM.getValue();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Determines the current height in characters of the system's terminal in
	 * use. This operation is expensive as it always does a full blown height
	 * calculation. To be less expensive, either use {@link #getHeight()} or
	 * {@link #getHeight(int)}.
	 * 
	 * @return The height of the terminal in characters or -1 if the height
	 *         cannot be determined.
	 */
	static int refreshHeight() {
		return getHeight( 0 );
	}

	/**
	 * Determines the current width in characters of the system's terminal in
	 * use. This operation is expensive as it always does a full blown width
	 * calculation. To be less expensive, either use {@link #getWidth()} or
	 * {@link #getWidth(int)}.
	 * 
	 * @return The height of the terminal in characters or -1 if the height
	 *         cannot be determined.
	 */
	static int refreshWidth() {
		return getWidth( 0 );
	}

	/**
	 * Determines whether we need an explicit line-break for the given width on
	 * the current operating system and used terminal. E.g. on "win.cmd" we must
	 * not use a line-break in case our line is a s long as the console's width.
	 * There are some more such cases which this method tries to take into
	 * consideration. This operation is expensive as it always does a full blown
	 * width calculation. To be less expensive, either use
	 * {@link #isLineBreakRequired(int)} or
	 * {@link #isLineBreakRequired(int, int)}.
	 * 
	 * @param aRowWidth The row width you want to use when printing out to a
	 *        console.
	 * 
	 * @return True in case you should use a line-break.
	 */
	static boolean refreshLineBreakRequired( int aRowWidth ) {
		return isLineBreakRequired( aRowWidth, 0 );
	}

	/**
	 * Determines the width in characters of the system's terminal in use. Idea:
	 * For ANSI-Terminals this could do the trick. Found at:
	 * "https://stackoverflow.com/questions/1286461/can-i-find-the-console-width-with-java"
	 * "...There's a trick that you can use based on ANSI Escape Codes. They
	 * don't provide a direct way to query the console size, but they do have a
	 * command for requesting the current position size. By moving the cursor to
	 * a really high row and column and then requesting the console size you can
	 * get an accurate measurement. Send the following sequences to the terminal
	 * (stdout) "\u001b[s" // save cursor position "\u001b[5000;5000H" // move
	 * to col 5000 row 5000 "\u001b[6n" // request cursor position "\u001b[u" //
	 * restore cursor position Now watch stdin, you should receive a sequence
	 * that looks like "\u001b[25;80R", where 25 is the row count, and 80 the
	 * columns ..." This would be an idea to implement :-)
	 * 
	 * @param aValidityTimeMillis The time period till last determination of
	 *        this value is valid, as determining this value is expensive.
	 * 
	 * @return The width of the terminal in characters or -1 if the width cannot
	 *         be determined.
	 */
	private static int getWidth( int aValidityTimeMillis ) {

		// Use last update? |-->
		if ( _terminalWidth != null && aValidityTimeMillis != 0 ) {
			if ( aValidityTimeMillis == -1 || _lastTerminalWidthUpdate + aValidityTimeMillis > System.currentTimeMillis() ) {
				return _terminalWidth;
			}
		}
		// Use last update? <--|

		int theTerminalWidth = -1;

		// ANSICON |-->
		theTerminalWidth = toTerminalWidth( theTerminalWidth, Terminal::getAnsiconWidth );
		// ANSICON <--|
		// COLUMNS |-->
		theTerminalWidth = toTerminalWidth( theTerminalWidth, Terminal::getTerminalColumns );
		// COLUMNS <--|

		// Native |-->
		theTerminalWidth = toTerminalWidth( theTerminalWidth, Terminal::getNativeWindowsWidth );
		// Native <--|

		if ( Terminal.isCygwinTerminal() ) {
			theTerminalWidth = toTerminalWidth( theTerminalWidth, Terminal::getTputExeWidth );
			theTerminalWidth = toTerminalWidth( theTerminalWidth, Terminal::getSttyExeWidth );
			theTerminalWidth = toTerminalWidth( theTerminalWidth, Terminal::getPowershellWidth );
			theTerminalWidth = toTerminalWidth( theTerminalWidth, Terminal::getModeConWidth );
		}

		final OperatingSystem theOs = OperatingSystem.toOperatingSystem();
		switch ( theOs ) {
		case MAC -> {
			theTerminalWidth = toTerminalWidth( theTerminalWidth, Terminal::getTputWidth );
			theTerminalWidth = toTerminalWidth( theTerminalWidth, Terminal::getSttyWidth );
		}
		case UNIX -> {
			theTerminalWidth = toTerminalWidth( theTerminalWidth, Terminal::getTputWidth );
			theTerminalWidth = toTerminalWidth( theTerminalWidth, Terminal::getSttyWidth );
		}
		case UNKNOWN -> {
			theTerminalWidth = toTerminalWidth( theTerminalWidth, Terminal::getTputWidth );
			theTerminalWidth = toTerminalWidth( theTerminalWidth, Terminal::getTputExeWidth );
			theTerminalWidth = toTerminalWidth( theTerminalWidth, Terminal::getSttyWidth );
			theTerminalWidth = toTerminalWidth( theTerminalWidth, Terminal::getSttyExeWidth );
			theTerminalWidth = toTerminalWidth( theTerminalWidth, Terminal::getPowershellWidth );
			theTerminalWidth = toTerminalWidth( theTerminalWidth, Terminal::getModeConWidth );
		}
		case WINDOWS -> {
			theTerminalWidth = toTerminalWidth( theTerminalWidth, Terminal::getPowershellWidth );
			theTerminalWidth = toTerminalWidth( theTerminalWidth, Terminal::getModeConWidth );
		}
		default -> throw new UnhandledEnumBugException( theOs );
		}

		if ( isUnrealsiticWidth( theTerminalWidth ) ) {
			theTerminalWidth = -1;
		}
		_terminalWidth = theTerminalWidth;
		_lastTerminalWidthUpdate = System.currentTimeMillis();
		return theTerminalWidth;
	}

	/**
	 * Determines the height in characters of the system's terminal in use.
	 * Idea: For ANSI-Terminals this could do the trick. Found at:
	 * "https://stackoverflow.com/questions/1286461/can-i-find-the-console-width-with-java"
	 * "...There's a trick that you can use based on ANSI Escape Codes. They
	 * don't provide a direct way to query the console size, but they do have a
	 * command for requesting the current position size. By moving the cursor to
	 * a really high row and column and then requesting the console size you can
	 * get an accurate measurement. Send the following sequences to the terminal
	 * (stdout) "\u001b[s" // save cursor position "\u001b[5000;5000H" // move
	 * to col 5000 row 5000 "\u001b[6n" // request cursor position "\u001b[u" //
	 * restore cursor position Now watch stdin, you should receive a sequence
	 * that looks like "\u001b[25;80R", where 25 is the row count, and 80 the
	 * columns ..." This would be an idea to implement :-)
	 * 
	 * @param aValidityTimeMillis The time period till last determination of
	 *        this value is valid, as determining this value is expensive.
	 * 
	 * @return The height of the terminal in characters or -1 if the height
	 *         cannot be determined.
	 */
	private static int getHeight( int aValidityTimeMillis ) {

		// Use last update? |-->
		if ( _terminalHeight != null && aValidityTimeMillis != 0 ) {
			if ( aValidityTimeMillis == -1 || _lastTerminalHeightUpdate + aValidityTimeMillis > System.currentTimeMillis() ) {
				return _terminalHeight;
			}
		}
		// Use last update? <--|

		int theTerminalHeight = -1;

		// ANSICON |-->
		theTerminalHeight = toTerminalHeight( theTerminalHeight, Terminal::getAnsiconHeight );
		// ANSICON <--|

		// LINES |-->
		theTerminalHeight = toTerminalHeight( theTerminalHeight, Terminal::getTerminalLines );
		// LINES <--|

		// Native |-->
		// theTerminalHeight = toTerminalHeight( theTerminalHeight, Terminal::getNativeWindowsHeight );
		// Native <--|

		if ( Terminal.isCygwinTerminal() ) {
			theTerminalHeight = toTerminalHeight( theTerminalHeight, Terminal::getTputExeHeight );
			theTerminalHeight = toTerminalHeight( theTerminalHeight, Terminal::getSttyExeHeight );
			theTerminalHeight = toTerminalHeight( theTerminalHeight, Terminal::getPowershellHeight );
			theTerminalHeight = toTerminalHeight( theTerminalHeight, Terminal::getModeConHeight );
		}

		final OperatingSystem theOs = OperatingSystem.toOperatingSystem();
		switch ( theOs ) {
		case MAC -> {
			theTerminalHeight = toTerminalHeight( theTerminalHeight, Terminal::getTputHeight );
			theTerminalHeight = toTerminalHeight( theTerminalHeight, Terminal::getSttyHeight );
		}
		case UNIX -> {
			theTerminalHeight = toTerminalHeight( theTerminalHeight, Terminal::getTputHeight );
			theTerminalHeight = toTerminalHeight( theTerminalHeight, Terminal::getSttyHeight );
		}
		case UNKNOWN -> {
			theTerminalHeight = toTerminalHeight( theTerminalHeight, Terminal::getTputHeight );
			theTerminalHeight = toTerminalHeight( theTerminalHeight, Terminal::getTputExeHeight );
			theTerminalHeight = toTerminalHeight( theTerminalHeight, Terminal::getSttyHeight );
			theTerminalHeight = toTerminalHeight( theTerminalHeight, Terminal::getSttyExeHeight );
			theTerminalHeight = toTerminalHeight( theTerminalHeight, Terminal::getPowershellHeight );
			theTerminalHeight = toTerminalHeight( theTerminalHeight, Terminal::getModeConHeight );
		}
		case WINDOWS -> {
			theTerminalHeight = toTerminalHeight( theTerminalHeight, Terminal::getPowershellHeight );
			theTerminalHeight = toTerminalHeight( theTerminalHeight, Terminal::getModeConHeight );
		}
		default -> throw new UnhandledEnumBugException( theOs );
		}

		if ( isUnrealsiticHeight( theTerminalHeight ) ) {
			theTerminalHeight = -1;
		}

		_terminalHeight = theTerminalHeight;
		_lastTerminalHeightUpdate = System.currentTimeMillis();
		return theTerminalHeight;
	}

	private static int getTputExeHeight() {
		if ( _canTputExe ) {
			try {
				final String theRersult = Host.exec( DaemonLoopSleepTime.MIN.getTimeMillis(), "tput.exe lines" );
				if ( theRersult != null ) {
					return Integer.parseInt( theRersult.trim() );
				}
			}
			catch ( Exception ignore ) {
				_canTputExe = false;
			}
		}

		if ( _canBashTputExe ) {
			try {
				final String theRersult = Host.exec( DaemonLoopSleepTime.MIN.getTimeMillis(), "bash.exe", "-c", "tput lines" );
				if ( theRersult != null ) {
					return Integer.parseInt( theRersult.trim() );
				}
			}
			catch ( Exception ignore ) {
				_canBashTputExe = false;
			}
		}

		return -1;
	}

	private static int getTputExeWidth() {
		if ( _canTputExe ) {
			try {
				final String theRersult = Host.exec( DaemonLoopSleepTime.MIN.getTimeMillis(), "tput.exe cols" );
				if ( theRersult != null ) {
					return Integer.parseInt( theRersult.trim() );
				}
			}
			catch ( Exception ignore ) {
				_canTputExe = false;
			}
		}

		if ( _canBashTputExe ) {
			try {
				final String theRersult = Host.exec( DaemonLoopSleepTime.MIN.getTimeMillis(), "bash.exe", "-c", "tput cols" );
				if ( theRersult != null ) {
					return Integer.parseInt( theRersult.trim() );
				}
			}
			catch ( Exception ignore ) {
				_canBashTputExe = false;
			}
		}

		return -1;
	}

	private static int getTputHeight() {
		if ( _canTput ) {
			try {
				final String theRersult = Host.exec( DaemonLoopSleepTime.MIN.getTimeMillis(), "tput lines" );
				if ( theRersult != null ) {
					return Integer.parseInt( theRersult.trim() );
				}
			}
			catch ( Exception ignore ) {
				_canTput = false;
			}
		}

		if ( _canBashTput ) {
			try {
				final String theRersult = Host.exec( DaemonLoopSleepTime.MIN.getTimeMillis(), "bash", "-c", "tput lines" );
				if ( theRersult != null ) {
					return Integer.parseInt( theRersult.trim() );
				}
			}
			catch ( Exception ignore ) {
				_canBashTput = false;
			}
		}

		return -1;
	}

	private static int getTputWidth() {
		if ( _canTput ) {
			try {
				final String theRersult = Host.exec( DaemonLoopSleepTime.MIN.getTimeMillis(), "tput cols" );
				if ( theRersult != null ) {
					return Integer.parseInt( theRersult.trim() );
				}
			}
			catch ( Exception ignore ) {
				_canTput = false;
			}
		}

		if ( _canBashTput ) {
			try {
				final String theRersult = Host.exec( DaemonLoopSleepTime.MIN.getTimeMillis(), "bash", "-c", "tput cols" );
				if ( theRersult != null ) {
					return Integer.parseInt( theRersult.trim() );
				}
			}
			catch ( Exception ignore ) {
				_canBashTput = false;
			}
		}

		return -1;
	}

	/**
	 * Determines whether ANSI escape sequences are supported by the terminal.
	 * 
	 * @return True in case ANSI escape sequences are supported, else false.
	 */
	public static boolean isAnsiTerminal() {
		if ( _isAnsiTerminal != null ) {
			return _isAnsiTerminal;
		}
		if ( EnvironmentVariable.ANSICON.getValue() != null && EnvironmentVariable.ANSICON.getValue().length() != 0 ) {
			_isAnsiTerminal = true;
		}
		else if ( isCygwinTerminal() ) {
			_isAnsiTerminal = true;
		}
		else if ( Shell.toShell() == Shell.POWER_SHELL ) {
			_isAnsiTerminal = true;
		}
		else if ( Shell.toShell() == Shell.WIN_CMD ) {
			_isAnsiTerminal = true;
		}
		else {
			if ( _canTput ) {
				try {
					final String theRersult = Host.exec( DaemonLoopSleepTime.MIN.getTimeMillis(), "tput colors" );
					if ( theRersult != null ) {
						final int theColors = Integer.parseInt( theRersult.trim() );
						if ( theColors > 4 ) {
							_isAnsiTerminal = true;
						}
					}
				}
				catch ( Exception ignore ) {
					_canTput = false;
				}
			}
			if ( _isAnsiTerminal == null ) {
				if ( _canBashTput ) {
					try {
						final String theRersult = Host.exec( DaemonLoopSleepTime.MIN.getTimeMillis(), "bash", "-c", "tput colors" );
						if ( theRersult != null ) {
							final int theColors = Integer.parseInt( theRersult.trim() );
							if ( theColors > 4 ) {
								_isAnsiTerminal = true;
							}
						}
					}
					catch ( Exception ignore ) {
						_canBashTput = false;
					}
				}
			}
		}
		if ( _isAnsiTerminal == null ) {
			_isAnsiTerminal = false;
		}
		return _isAnsiTerminal;
	}

	/**
	 * Determines whether ANSI escape sequences are forced to be supported or
	 * not by REFCODES.ORG artifacts. This overrules {@link #isAnsiTerminal()}
	 * for the REFCODES.ORG artifacts. Various settings such as
	 * {@link SystemProperty#CONSOLE_ANSI} and (in this order)
	 * {@link EnvironmentVariable#CONSOLE_ANSI} overrule
	 * {@link #isAnsiTerminal()}.
	 * 
	 * @return True in case ANSI escape sequences are forced to be used by
	 *         REFCODES.ORG artifacts or not.
	 */
	public static boolean isAnsiTerminalEnabled() {
		final String theResult = SystemProperty.toPropertyValue( SystemProperty.CONSOLE_ANSI, EnvironmentVariable.CONSOLE_ANSI, EnvironmentVariable.CONSOLE_CONEMU_ANSI );
		if ( theResult != null ) {
			try {
				return NumericalUtility.toBoolean( theResult );
			}
			catch ( IllegalArgumentException e ) {}
		}
		return isAnsiTerminal();
	}

	/**
	 * Tries to determine whether the command line interpreter (CLI) is a Cygwin
	 * one.
	 * 
	 * @return True in case we think we are running in Cygwin. Use
	 *         {@link #getCommandLineInterpreter()} to test for the type of CLI,
	 *         in case you got to distinguish the {@link Shell#SHELL} type, then
	 *         use this {@link #isCygwinTerminal()} method.
	 */
	static boolean isCygwinTerminal() {

		if ( _isCygwinTerminal != null ) {
			return _isCygwinTerminal;
		}

		_isCygwinTerminal = false;
		// if ( OperatingSystem.toOperatingSystem() == OperatingSystem.WINDOWS ) {
		if ( "cygwin".equalsIgnoreCase( EnvironmentVariable.TERM.getValue() ) ) {
			_isCygwinTerminal = true;
		}
		// String theUname = SystemUtility.getUname();
		// if ( theUname != null && theUname.toLowerCase().indexOf( "cygwin"
		// ) != -1 ) {
		// // "PWD" is only set by cygwin, not in CMD.EXE:
		// String thePwd = System.getenv( "PWD" );
		// _isCygwinTerminal = (thePwd != null && thePwd.length() != 0);
		// }

		// }
		return _isCygwinTerminal;
	}

	/**
	 * Determines whether we need an explicit line-break for the given width on
	 * the current operating system and used terminal. E.g. on "win.cmd" we must
	 * not use a line-break in case our line is a s long as the console's width.
	 * There are some more such cases which this method tries to take into
	 * consideration.
	 * 
	 * @param aRowWidth The row width you want to use when printing out to a
	 *        console.
	 * 
	 * @return True in case you should use a line-break.
	 */
	public static boolean isLineBreakRequired( int aRowWidth ) {
		return isLineBreakRequired( aRowWidth, -1 );
	}

	/**
	 * Determines whether we need an explicit line-break for the given width on
	 * the current operating system and used terminal. E.g. on "win.cmd" we must
	 * not use a line-break in case our line is a s long as the console's width.
	 * There are some more such cases which this method tries to take into
	 * consideration.
	 * 
	 * @param aRowWidth The row width you want to use when printing out to a
	 *        console.
	 * @param aValidityTimeMillis The time period till last determination of
	 *        this value is valid, as determining this value is expensive.
	 * 
	 * @return True in case you should use a line-break.
	 */
	private static boolean isLineBreakRequired( int aRowWidth, int aValidityTimeMillis ) {

		// Heurisitc |-->
		if ( Execution.isUnderTest() && ( aRowWidth == PRESET_TERMINAL_WIDTH || aRowWidth == PRESET_WINCMD_WIDTH ) ) {
			return true;
			// Heurisitc <--|
		}

		if ( Shell.toShell() == Shell.NONE ) {
			return true;
		}
		final int theWidth = getWidth( aValidityTimeMillis );
		if ( ( OperatingSystem.toOperatingSystem() == OperatingSystem.WINDOWS ) && aRowWidth == theWidth ) {
			try {
				final float theVersion = Float.parseFloat( OperatingSystem.WINDOWS.getVersion() );
				if ( theVersion < 10F ) {
					return false;
				}
			}
			catch ( Exception ignore ) {}
		}
		return true;
	}

	/**
	 * Determines whether the given terminal height is to be reconsidered.
	 * 
	 * @param eTerminalHeight The calculated height of the terminal.
	 * 
	 * @return True in case that height is to be reconsidered.
	 */
	private static boolean isReconsiderableHeight( int eTerminalHeight ) {
		return ( eTerminalHeight <= 1 || eTerminalHeight > HEIGHT_ABOVE_UNREALSITIC || eTerminalHeight == PRESET_TERMINAL_HEIGHT );
	}

	/**
	 * Determines whether the given terminal width is to be reconsidered.
	 * 
	 * @param eTerminalWidth The calculated width of the terminal.
	 * 
	 * @return True in case that width is to be reconsidered.
	 */
	private static boolean isReconsiderableWidth( int eTerminalWidth ) {
		return ( eTerminalWidth <= 1 || eTerminalWidth > WIDTH_ABOVE_UNREALSITIC || eTerminalWidth == PRESET_TERMINAL_WIDTH );
	}

	/**
	 * Determines whether the given terminal height is to be reconsidered.
	 * 
	 * @param eTerminalHeight The calculated height of the terminal.
	 * 
	 * @return True in case that height is to be reconsidered.
	 */
	private static boolean isUnrealsiticHeight( int eTerminalHeight ) {
		return ( eTerminalHeight <= 1 || eTerminalHeight > HEIGHT_ABOVE_UNREALSITIC );
	}

	/**
	 * Determines whether the given terminal width is to be reconsidered.
	 * 
	 * @param eTerminalWidth The calculated width of the terminal.
	 * 
	 * @return True in case that width is to be reconsidered.
	 */
	private static boolean isUnrealsiticWidth( int eTerminalWidth ) {
		return ( eTerminalWidth <= 1 || eTerminalWidth > WIDTH_ABOVE_UNREALSITIC );
	}

	/**
	 * Extracts the height from an "ANSICON" environment variable's value.
	 * 
	 * @param aAnsiconEnvVarValue The "ANSICON" formatted variable (e.g.
	 *        "91x19999 (91x51)").
	 * 
	 * @return The determined height.
	 * 
	 * @throws NumberFormatException Thrown in case no width was determinable
	 *         from the provided value.
	 */
	static int toAnsiconHeight( String aAnsiconEnvVarValue ) {
		if ( aAnsiconEnvVarValue != null ) {
			final int index = aAnsiconEnvVarValue.lastIndexOf( "x" );
			if ( index != -1 && aAnsiconEnvVarValue.length() > index + 1 ) {
				aAnsiconEnvVarValue = aAnsiconEnvVarValue.substring( index + 1 );
				if ( aAnsiconEnvVarValue.length() != 0 && aAnsiconEnvVarValue.endsWith( ")" ) ) {
					aAnsiconEnvVarValue = aAnsiconEnvVarValue.substring( 0, aAnsiconEnvVarValue.length() - 1 );
				}
				return Integer.valueOf( aAnsiconEnvVarValue );
			}
		}
		throw new NumberFormatException( "Cannot extract width from ANSICON formatted value <" + aAnsiconEnvVarValue + ">." );
	}

	/**
	 * Extracts the width from an "ANSICON" environment variable's value.
	 * 
	 * @param aAnsiconEnvVarValue The "ANSICON" formatted variable (e.g.
	 *        "91x19999 (91x51)").
	 * 
	 * @return The determined width.
	 * 
	 * @throws NumberFormatException Thrown in case no width was determinable
	 *         from the provided value.
	 */
	static int toAnsiconWidth( String aAnsiconEnvVarValue ) {
		if ( aAnsiconEnvVarValue != null ) {
			final int index = aAnsiconEnvVarValue.indexOf( "x" );
			if ( index != -1 ) {
				aAnsiconEnvVarValue = aAnsiconEnvVarValue.substring( 0, index );
				return Integer.valueOf( aAnsiconEnvVarValue );
			}
		}
		throw new NumberFormatException( "Cannot extract width from ANSICON formatted value <" + aAnsiconEnvVarValue + ">." );
	}

	/**
	 * Uses {@link #isLineBreakRequired(int)} to retrieve the character sequence
	 * required to suffix to a line in order to get a line break without risking
	 * any empty lines as of automatic line wrapping. Automatic line wrapping
	 * can happen on Windows environment when the console width is reached. A
	 * line break would cause a superfluous empty line (ugly).
	 * 
	 * @param aRowWidth The row width you want to use when printing out to a
	 *        console.
	 * 
	 * @return The system's line break characters when a line break is
	 *         emphasized or an empty string if no line break sequence is
	 *         recommended.
	 */
	public static String toLineBreak( int aRowWidth ) {
		if ( isLineBreakRequired( aRowWidth ) ) {
			return getLineBreak();
		}
		return "\r";
	}

	/**
	 * Does some calculation to always return a sound console height (never
	 * returns -1). Anything in the REFCODES.ORG artifacts which is intended to
	 * print to the console uses this method to determine some heuristic best
	 * console height.
	 * 
	 * @return A sound heuristic console height.
	 */
	public static int toHeuristicHeight() {
		int theHeight = -1;
		final String theResult = SystemProperty.toPropertyValue( SystemProperty.CONSOLE_HEIGHT, EnvironmentVariable.CONSOLE_HEIGHT );
		if ( theResult != null ) {
			try {
				theHeight = Integer.valueOf( theResult );
			}
			catch ( NumberFormatException e ) {}
		}
		if ( theHeight <= 1 ) {
			theHeight = getHeight();
		}
		// Above unrealistic? |-->
		if ( theHeight < ConsoleDimension.MIN_HEIGHT.getValue() || theHeight > HEIGHT_ABOVE_UNREALSITIC ) {
			theHeight = ConsoleDimension.MIN_HEIGHT.getValue();
		}
		// Above unrealistic? <--|
		return theHeight;
	}

	/**
	 * Does some calculation to always return a sound console width (never
	 * returns -1). Anything in the REFCODES.ORG artifacts which is intended to
	 * print to the console uses this method to determine some heuristic best
	 * console width.
	 * 
	 * @return A sound heuristic console width.
	 */
	public static int toHeuristicWidth() {
		int theWidth = -1;
		final String theResult = SystemProperty.toPropertyValue( SystemProperty.CONSOLE_WIDTH, EnvironmentVariable.CONSOLE_WIDTH );
		if ( theResult != null ) {
			try {
				theWidth = Integer.valueOf( theResult );
			}
			catch ( NumberFormatException e ) {}
		}
		if ( theWidth <= 1 ) {
			theWidth = getWidth();
		}
		// Above unrealistic? |-->
		if ( theWidth < ConsoleDimension.MIN_WIDTH.getValue() || theWidth > WIDTH_ABOVE_UNREALSITIC ) {
			theWidth = ConsoleDimension.MIN_WIDTH.getValue();
		}
		// Above unrealistic? <--|
		return theWidth;
	}

	/**
	 * Determines the {@link CharSetCapability} of the current terminal session.
	 * The {@link CharSetCapability} is used to determine which characters to
	 * use when printing tables and boxes to the terminal (see the
	 * <code>TableStyle</code> class's <code>toExecutionTableStyle</code> method
	 * as well as the <code>TextBoxGrid</code> class's
	 * <code>toExecutionTextBoxGrid()</code> method.
	 * 
	 * @return The {@link CharSetCapability} determined at best knowledge.
	 */
	public static CharSetCapability toCharSetCapability() {
		/* 1st */ if ( Execution.isUnderTest() ) {
			return CharSetCapability.ASCII;
		}
		/* 2nd */ if ( Terminal.getEncoding() != null && Terminal.getEncoding().toLowerCase().contains( Encoding.CP1252.getCode().toLowerCase() ) ) {
			return CharSetCapability.ASCII;
		}
		/* 3rd */ if ( OperatingSystem.toOperatingSystem() == OperatingSystem.WINDOWS ) {
			return CharSetCapability.WINDOWS;
		}
		/* 4th */ if ( EnvironmentVariable.LANG.getValue() != null && EnvironmentVariable.LANG.getValue().toLowerCase().contains( "utf" ) ) {
			return CharSetCapability.UNICODE;
		}
		/* 5th */ if ( Terminal.toTerminal().getAlias() != null && Terminal.toTerminal().getAlias().toLowerCase().contains( "unicode" ) ) {
			return CharSetCapability.UNICODE;
		}
		/* 6th */ if ( Terminal.getEncoding() != null && Terminal.getEncoding().toLowerCase().contains( "utf" ) ) {
			return CharSetCapability.UNICODE;
		}
		/* 7th */ if ( Terminal.toTerminal() == Terminal.XTERM ) {
			return CharSetCapability.ASCII;
		}
		/* 8th */ if ( Terminal.toTerminal() == Terminal.XTERM_256COLOR ) {
			return CharSetCapability.ASCII;
		}
		/* 9th */ return CharSetCapability.ASCII;
	}

	/**
	 * Determines the terminal your application is currently in:
	 * 
	 * @return The {@link Terminal} being detected or {@link #UNKNOWN} if
	 *         detection was not possible.
	 */
	public static Terminal toTerminal() {
		String theTerminal = EnvironmentVariable.TERM.getValue();
		if ( theTerminal != null ) {
			theTerminal = theTerminal.toLowerCase();
			for ( Terminal eTerminal : values() ) {
				if ( eTerminal.getAlias() != null && theTerminal.toLowerCase().contains( eTerminal.getAlias() ) ) {
					return eTerminal;
				}
			}
		}
		return Terminal.UNKNOWN;
	}

	/**
	 * Determines whether this enumeration represents the current
	 * {@link Terminal} (e.g. this {@link Terminal} has be detected to be
	 * running your application).
	 * 
	 * @return True in case this enumeration (most probably) represents this
	 *         session's {@link Terminal}.
	 */
	@Override
	public boolean isDetected() {
		return toTerminal() == this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Extracts the width from a "mode con" call's result on "cmd.exe" to
	 * determine the console's height.
	 * 
	 * @param aModeConOutput "mode con" call's result.
	 * 
	 * @return The determined height.
	 * 
	 * @throws NumberFormatException Thrown in case no width was determinable
	 *         from the provided value.
	 */
	static int toModeConHeight( String aModeConOutput ) {
		return toModeConValue( aModeConOutput, 3 );
	}

	/**
	 * Extracts the width from a "mode con" call's result on "cmd.exe" to
	 * determine the console's width.
	 * 
	 * @param aModeConOutput "mode con" call's result.
	 * 
	 * @return The determined width.
	 * 
	 * @throws NumberFormatException Thrown in case no width was determinable
	 *         from the provided value.
	 */
	static int toModeConWidth( String aModeConOutput ) {
		return toModeConValue( aModeConOutput, 4 );
	}

	/**
	 * Extracts the value from a "mode con" call result's specific row on
	 * "cmd.exe".
	 * 
	 * @param aModeConOutput "mode con" call's result.
	 * @param aValueIndex The index of the value to retrieve.
	 * 
	 * @return The determined value for the given row.
	 * 
	 * @throws NumberFormatException Thrown in case no value was determinable
	 *         from the provided value.
	 */
	private static int toModeConValue( String aModeConOutput, int aValueIndex ) {
		try {
			int index = 0;
			for ( int i = 0; i < aValueIndex - 1; i++ ) {
				index = aModeConOutput.indexOf( ':' );
				if ( index == -1 || index == aModeConOutput.length() - 1 ) {
					return -1;
				}
				aModeConOutput = aModeConOutput.substring( index + 1 );
			}
			aModeConOutput = aModeConOutput.trim();
			index = -1;
			for ( int i = 0; i < aModeConOutput.length(); i++ ) {
				if ( Character.isDigit( aModeConOutput.charAt( i ) ) ) {
					index = i;
				}
				else {
					break;
				}
			}
			if ( index == -1 ) {
				return -1;
			}
			aModeConOutput = aModeConOutput.substring( 0, index + 1 );
			final int theValue = Integer.parseInt( aModeConOutput );
			return theValue;
		}
		catch ( Exception ignore ) {}
		throw new NumberFormatException( "Cannot extract the value from \"mode con\" formatted value <" + aModeConOutput + "> for row <" + aValueIndex + ">." );
	}

	/**
	 * Determines the right value to be returned when encountering the given
	 * current evaluated terminal height and the new terminal height candidate.
	 * 
	 * @param aCurrentTerminalHeight The height to evaluate.
	 * 
	 * @return The "right" value.
	 */
	private static int toTerminalHeight( int aCurrentTerminalHeight, IntSupplier aNewHeightCandidate ) {
		if ( isReconsiderableHeight( aCurrentTerminalHeight ) ) {
			final int theNewHeight = aNewHeightCandidate.getAsInt();
			if ( !isUnrealsiticHeight( theNewHeight ) ) {
				return theNewHeight;
			}
		}
		return aCurrentTerminalHeight;
	}

	/**
	 * Determines the right value to be returned when encountering the given
	 * current evaluated terminal width and the new terminal width candidate.
	 * 
	 * @param aCurrentTerminalWidth The width to evaluate.
	 * 
	 * @return The "right" value.
	 */
	private static int toTerminalWidth( int aCurrentTerminalWidth, IntSupplier aNewWidthCandidate ) {
		if ( isReconsiderableWidth( aCurrentTerminalWidth ) ) {
			final int theNewWidthCandidate = aNewWidthCandidate.getAsInt();
			if ( !isUnrealsiticWidth( theNewWidthCandidate ) ) {
				return theNewWidthCandidate;
			}
		}
		return aCurrentTerminalWidth;
	}

	/**
	 * Extracts the height from an "ANSICON" environment variable's value. The
	 * "ANSICON" formatted variable looks something like this: "91x19999
	 * (91x51)").
	 * 
	 * @return The determined height or -1 if we have no such variable.
	 */
	private static int getAnsiconHeight() {
		try {
			return toAnsiconHeight( EnvironmentVariable.ANSICON.getValue() );
		}
		catch ( NumberFormatException ignore ) {}
		return -1;
	}

	/**
	 * Extracts the width from an "ANSICON" environment variable's value. The
	 * "ANSICON" formatted variable looks something like this: "91x19999
	 * (91x51)").
	 * 
	 * @return The determined width or -1 if we have no such variable.
	 */
	private static int getAnsiconWidth() {
		try {
			return toAnsiconWidth( EnvironmentVariable.ANSICON.getValue() );
		}
		catch ( NumberFormatException ignore ) {}
		return -1;
	}

	private static int getModeConHeight() {
		if ( _canModeCon ) {
			try {
				final String theRersult = Host.exec( DaemonLoopSleepTime.MIN.getTimeMillis(), "mode.com con" );
				return toModeConHeight( theRersult );
			}
			catch ( Exception ignore ) {
				_canModeCon = false;
			}
		}

		if ( _canCmdModeCon ) {
			try {

				final String theRersult = Host.exec( DaemonLoopSleepTime.MIN.getTimeMillis(), "cmd.exe", "/c", "mode con" );
				return toModeConHeight( theRersult );
			}
			catch ( Exception ignore ) {
				_canCmdModeCon = false;
			}
		}

		return -1;
	}

	private static int getModeConWidth() {
		if ( _canModeCon ) {
			try {
				final String theRersult = Host.exec( DaemonLoopSleepTime.MIN.getTimeMillis(), "mode.com con" );
				return toModeConWidth( theRersult );
			}
			catch ( Exception ignore ) {
				_canModeCon = false;
			}
		}

		if ( _canCmdModeCon ) {
			try {
				final String theRersult = Host.exec( DaemonLoopSleepTime.MIN.getTimeMillis(), "cmd.exe", "/c", "mode con" );
				return toModeConWidth( theRersult );
			}
			catch ( Exception ignore ) {
				_canCmdModeCon = false;
			}
		}
		return -1;
	}

	private static int getNativeWindowsWidth() {
		if ( _isAnsi ) {
			if ( _canNativeWindows ) {
				try {
					return AnsiWrapper.getTerminalWidth();
				}
				catch ( Error e ) {
					_canNativeWindows = false;
				}
			}
		}
		return -1;
	}

	private static int getPowershellHeight() {
		if ( _canPowershell ) {
			try {
				final String theRersult = Host.exec( DaemonLoopSleepTime.MIN.getTimeMillis(), "powershell.exe", "/c", "$host.UI.RawUI.WindowSize.Height" );
				if ( theRersult != null ) {
					return Integer.parseInt( theRersult.trim() );
				}
			}
			catch ( Exception ignore ) {
				_canPowershell = false;
			}
		}
		return -1;
	}

	private static int getPowershellWidth() {
		if ( _canPowershell ) {
			try {
				final String theRersult = Host.exec( DaemonLoopSleepTime.MIN.getTimeMillis(), "powershell.exe", "/c", "$host.UI.RawUI.WindowSize.Width" );
				if ( theRersult != null ) {
					return Integer.parseInt( theRersult.trim() );
				}
			}
			catch ( Exception ignore ) {
				_canPowershell = false;
			}
		}
		return -1;
	}

	private static int getSttyExeHeight() {
		if ( _canSttyExe ) {
			try {
				final String theRersult = Host.exec( DaemonLoopSleepTime.MIN.getTimeMillis(), "stty.exe size" );
				if ( theRersult != null ) {
					final String[] theSize = theRersult.trim().split( " " );
					if ( theSize.length == 2 ) {
						return Integer.parseInt( theSize[0] );
					}
				}

			}
			catch ( Exception ignore ) {
				_canSttyExe = false;
			}
		}

		if ( _canBashSttyExe ) {
			try {
				final String theRersult = Host.exec( DaemonLoopSleepTime.MIN.getTimeMillis(), "bash.exe", "-c", "stty size" );
				if ( theRersult != null ) {
					final String[] theSize = theRersult.trim().split( " " );
					if ( theSize.length == 2 ) {
						return Integer.parseInt( theSize[0] );
					}
				}
			}
			catch ( Exception ignore ) {
				_canBashSttyExe = false;
			}
		}

		return -1;
	}

	private static int getSttyExeWidth() {
		if ( _canSttyExe ) {
			try {
				final String theRersult = Host.exec( DaemonLoopSleepTime.MIN.getTimeMillis(), "stty.exe size" );
				if ( theRersult != null ) {
					final String[] theSize = theRersult.trim().split( " " );
					if ( theSize.length == 2 ) {
						return Integer.parseInt( theSize[1] );
					}
				}

			}
			catch ( Exception ignore ) {
				_canSttyExe = false;
			}
		}

		if ( _canBashSttyExe ) {
			try {
				final String theRersult = Host.exec( DaemonLoopSleepTime.MIN.getTimeMillis(), "bash.exe", "-c", "stty size" );
				if ( theRersult != null ) {
					final String[] theSize = theRersult.trim().split( " " );
					if ( theSize.length == 2 ) {
						return Integer.parseInt( theSize[1] );
					}
				}
			}
			catch ( Exception ignore ) {
				_canBashSttyExe = false;
			}
		}

		return -1;
	}

	private static int getSttyHeight() {
		if ( _canStty ) {
			try {
				final String theRersult = Host.exec( DaemonLoopSleepTime.MIN.getTimeMillis(), "stty size" );
				if ( theRersult != null ) {
					final String[] theSize = theRersult.trim().split( " " );
					if ( theSize.length == 2 ) {
						return Integer.parseInt( theSize[0] );
					}
				}

			}
			catch ( Exception ignore ) {
				_canStty = false;
			}
		}

		if ( _canBashStty ) {
			try {
				final String theRersult = Host.exec( DaemonLoopSleepTime.MIN.getTimeMillis(), "bash", "-c", "stty size" );
				if ( theRersult != null ) {
					final String[] theSize = theRersult.trim().split( " " );
					if ( theSize.length == 2 ) {
						return Integer.parseInt( theSize[0] );
					}
				}
			}
			catch ( Exception ignore ) {
				_canBashStty = false;
			}
		}

		return -1;
	}

	private static int getSttyWidth() {
		if ( _canStty ) {
			try {
				final String theRersult = Host.exec( DaemonLoopSleepTime.MIN.getTimeMillis(), "stty size" );
				if ( theRersult != null ) {
					final String[] theSize = theRersult.trim().split( " " );
					if ( theSize.length == 2 ) {
						return Integer.parseInt( theSize[1] );
					}
				}

			}
			catch ( Exception ignore ) {
				_canStty = false;
			}
		}

		if ( _canBashStty ) {
			try {
				final String theRersult = Host.exec( DaemonLoopSleepTime.MIN.getTimeMillis(), "bash", "-c", "stty size" );
				if ( theRersult != null ) {
					final String[] theSize = theRersult.trim().split( " " );
					if ( theSize.length == 2 ) {
						return Integer.parseInt( theSize[1] );
					}
				}
			}
			catch ( Exception ignore ) {
				_canBashStty = false;
			}
		}

		return -1;
	}

	private static int getTerminalColumns() {
		final String theResult = EnvironmentVariable.TERMINAL_COLUMNS.getValue();
		if ( theResult != null ) {
			try {
				return Integer.valueOf( theResult );
			}
			catch ( NumberFormatException ignore ) {}
		}
		return -1;
	}

	private static int getTerminalLines() {
		final String theResult = EnvironmentVariable.TERMINAL_LINES.getValue();
		if ( theResult != null ) {
			try {
				return Integer.valueOf( theResult );
			}
			catch ( NumberFormatException ignore ) {}
		}
		return -1;
	}
}
