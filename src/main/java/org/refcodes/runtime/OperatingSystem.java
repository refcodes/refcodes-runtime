// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.runtime;

import org.refcodes.mixin.DetectedAccessor;

/**
 * Enumeration with the (supported) operating systems.
 */
public enum OperatingSystem implements DetectedAccessor {

	/**
	 * Unix / Linux / BSD like operating system.
	 */
	UNIX(SystemProperty.OPERATING_SYSTEM_NAME.getValue()),

	WINDOWS(SystemProperty.OPERATING_SYSTEM_NAME.getValue()),

	MAC(SystemProperty.OPERATING_SYSTEM_NAME.getValue()),

	/**
	 * Unknown operating system.
	 */
	UNKNOWN(null);

	private String _version;

	private OperatingSystem( String aName ) {
		_version = SystemProperty.OPERATING_SYSTEM_VERSION.getValue();
		if ( aName != null ) {
			if ( _version == null || _version.isEmpty() ) {
				for ( int i = 0; i < aName.length(); i++ ) {
					if ( Character.isDigit( aName.charAt( i ) ) || aName.charAt( i ) == '.' ) {
						_version += aName.charAt( i );
					}
				}
			}
		}
	}

	/**
	 * Determines the operating system your application is currently running on.
	 * 
	 * @return The {@link OperatingSystem} being detected.
	 */
	public static OperatingSystem toOperatingSystem() {
		String theOperationSystem = SystemProperty.OPERATING_SYSTEM_NAME.getValue();
		if ( theOperationSystem != null ) {
			theOperationSystem = theOperationSystem.toLowerCase();
			if ( theOperationSystem.contains( "win" ) ) {
				return OperatingSystem.WINDOWS;
			}
			if ( theOperationSystem.contains( "mac" ) ) {
				return OperatingSystem.MAC;
			}
			if ( theOperationSystem.contains( "linux" ) ) {
				return OperatingSystem.UNIX;
			}
			if ( theOperationSystem.contains( "sunos" ) ) {
				return OperatingSystem.UNIX;
			}
			if ( theOperationSystem.contains( "aix" ) ) {
				return OperatingSystem.UNIX;
			}
			if ( theOperationSystem.contains( "nix" ) ) {
				return OperatingSystem.UNIX;
			}
		}
		return OperatingSystem.UNKNOWN;
	}

	/**
	 * Tries at best will to get a version number of the currently operating OS.
	 * Useful to get the OS version of the {@link OperatingSystem} enumeration
	 * returned by {@link #toOperatingSystem()}.
	 * 
	 * @return The version number at best will, returns null if the the
	 *         {@link OperatingSystem} enumeration on which this method is being
	 *         called does not correspond to the actual operating OS for the
	 *         executing machine.
	 */
	public String getVersion() {
		return this == toOperatingSystem() ? _version : null;
	}

	/**
	 * Determines whether this enumeration represents the current
	 * {@link OperatingSystem} (e.g. this OS has be detected to be running
	 * currently).
	 * 
	 * @return True in case this enumeration (most probably) represents this
	 *         machine's {@link OperatingSystem}.
	 */
	@Override
	public boolean isDetected() {
		return toOperatingSystem() == this;
	}
}