// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.runtime;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.refcodes.mixin.Dumpable;
import org.refcodes.mixin.ObjectAccessor.ObjectBuilder;
import org.refcodes.mixin.ObjectAccessor.ObjectProperty;

/**
 * The {@link DumpBuilder} assists you in inspecting the content of an object.
 * Creates a {@link Map} containing the dump of an object's the member
 * variable's values, the names being the keys and the values being the member
 * variables' values.
 */
public class DumpBuilder implements ObjectProperty, ObjectBuilder<DumpBuilder> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Object _dumpable = null;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getObject() {
		return _dumpable;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setObject( Object aObject ) {
		_dumpable = aObject;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DumpBuilder withObject( Object aObject ) {
		setObject( aObject );
		return this;
	}

	/**
	 * Dumps the state of the provided object into a {@link Map} which is
	 * returned as result.
	 * 
	 * @param aObj The object for which to create the dump.
	 * 
	 * @return The {@link Map} with the dumped properties of the implementing
	 *         instance.
	 */
	public Map<String, String> toDump( Object aObj ) {
		return asDump( aObj );
	}

	/**
	 * Dumps the state of the contained object (as of
	 * {@link #setObject(Object)}) into a {@link Map} which is returned as
	 * result.
	 * 
	 * @return The {@link Map} with the dumped properties of the implementing
	 *         instance.
	 */
	public Map<String, String> toDump() {
		return toDump( _dumpable );
	}

	/**
	 * Dumps the the provided object into a {@link Map} which is returned as
	 * result (the builder needs not to be instantiated).
	 * 
	 * @param aObj The object for which to create the dump.
	 * 
	 * @return The {@link Map} with the dumped properties of the implementing
	 *         instance.
	 */
	public static Map<String, String> asDump( Object aObj ) {
		return toDump( aObj, new HashMap<>() );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Dumps the state of the implementing instance into the provided
	 * {@link Map} which is returned as result. This default implementation uses
	 * reflection to create a basic dump. {@link Map} properties are processed
	 * but not recursively digged into. Existing properties are overwritten in
	 * case shadowing super-class's properties.
	 *
	 * @param aObj The object to be dumped.
	 * @param aDump the {@link Map} where to dump to.
	 * 
	 * @return The {@link Map} with the dumped properties of the implementing
	 *         instance.
	 */
	protected static Map<String, String> toDump( Object aObj, Map<String, String> aDump ) {
		Field[] eFields;
		String eFieldName;
		Object eValue;
		Map<?, ?> eMap;
		Map<String, String> eDump;
		String eStringValue;
		Class<?> eClass = aObj.getClass();
		String eKeyString;
		if ( aObj instanceof Map ) {
			eMap = (Map<?, ?>) aObj;
			for ( Object eKey : eMap.keySet() ) {
				eValue = eMap.get( eKey );
				try {
					aDump.put( eKey != null ? eKey.toString() : null, eValue != null ? eValue.toString() : null );
				}
				// ---------------------------------------------
				// In case the Map does not support null values:
				// ---------------------------------------------
				catch ( NullPointerException e ) {
					eKeyString = eKey != null ? eKey.toString() : "null";
					eStringValue = eValue != null ? eValue.toString() : "null";
					aDump.put( eKeyString, eStringValue );
				}
				// ---------------------------------------------
			}
		}

		do {
			eFields = eClass.getDeclaredFields();
			for ( Field eField : eFields ) {
				eFieldName = eField.getName();
				try {
					// Don't in Java 9 |-->
					try {
						eField.setAccessible( true );
					}
					catch ( Exception ignore ) {}
					// Don't in Java 9 <--|
					eValue = eField.get( aObj );
					if ( eValue instanceof Dumpable ) {
						eDump = ( (Dumpable) eValue ).toDump();
						for ( String eKey : eDump.keySet() ) {
							aDump.put( eFieldName + "." + eKey, eDump.get( eKey ) );
						}
					}
					else {
						if ( eValue instanceof Map ) {
							eMap = (Map<?, ?>) eValue;
							for ( Object eKey : eMap.keySet() ) {
								eValue = eMap.get( eKey );
								try {
									eKeyString = eKey != null ? eKey.toString() : null;
									eStringValue = eValue != null ? eValue.toString() : null;
									aDump.put( eFieldName + "." + eKeyString, eStringValue );
								}
								// ---------------------------------------------
								// In case the Map does not support null values:
								// ---------------------------------------------
								catch ( NullPointerException e ) {
									eKeyString = eKey != null ? eKey.toString() : "null";
									eStringValue = eValue != null ? eValue.toString() : "null";
									aDump.put( eFieldName + "." + eKeyString, eStringValue );
								}
								// ---------------------------------------------
							}
						}
						else {
							eStringValue = eValue != null ? eValue.toString() : null;
							aDump.put( eFieldName, eStringValue );
						}
					}
				}
				catch ( IllegalArgumentException | IllegalAccessException e ) {
					aDump.put( eFieldName, "(unaccessible field)" );
				}
			}
			eClass = eClass.getSuperclass();
		} while ( eClass != null );
		return aDump;
	}
}
