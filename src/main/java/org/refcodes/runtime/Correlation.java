// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.runtime;

import org.refcodes.data.Delimiter;
import org.refcodes.data.Literal;
import org.refcodes.generator.Generator;
import org.refcodes.generator.UniqueIdGenerator;
import org.refcodes.mixin.IdAccessor.IdProperty;

/**
 * The {@link Correlation} assigns an according (as of the enumeration)
 * Correlation-TID to the invoking process, this TID is used in log files and is
 * part of the process's request or response pay-load in order to track process
 * execution throughout a JVM.
 */
public enum Correlation implements IdProperty<String> {

	/**
	 * A single request spanning over multiple systems.
	 */
	REQUEST(24),

	/**
	 * Identifying a session for multiple requests spanning over multiple
	 * systems. traced.
	 */
	SESSION(24);

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private ThreadLocal<String> _correlationId = new ThreadLocal<>();

	private Generator<String> _generator;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new correlation.
	 *
	 * @param aIdlength the idlength
	 */
	private Correlation( int aIdlength ) {
		_generator = new UniqueIdGenerator( aIdlength );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Assigns the given Correlation-TID to the current {@link Thread}.
	 * 
	 * @param aCorrelationId The Correlation-TID to be assigned to the current
	 *        {@link Thread}.
	 */
	@Override
	public void setId( String aCorrelationId ) {
		_correlationId.set( aCorrelationId );
	}

	/**
	 * Retrieves the Correlation-TID assigned to the current {@link Thread}.
	 * Call {@link #pullId()} to make sure a Correlation-TID is created for the
	 * current thread if none exists yet or call {@link #nextId()} in order to
	 * create a new Correlation-TID.
	 * 
	 * @return The Correlation-TID of the current {@link Thread}.
	 */
	@Override
	public String getId() {
		return _correlationId.get();
	}

	/**
	 * Returns a Correlation-TID in any case: Creates a new Correlation-TID in
	 * case there is none yet for the current thread. Else the current
	 * Correlation-TID is returned.
	 * 
	 * @return The Correlation-TID as of now.
	 */
	public String pullId() {
		String theId = _correlationId.get();
		if ( theId == null ) {
			theId = _generator.next();
			_correlationId.set( theId );
		}
		return theId;
	}

	/**
	 * Creates a new Correlation-TID no matter whether there is a
	 * Correlation-TID already assigned to the current thread or not.
	 *
	 * @return the string
	 */
	public String nextId() {
		final String theNext = _generator.next();
		_correlationId.set( theNext );
		return theNext;
	}

	/**
	 * Returns a {@link String} with all Correlation-IDs found separated by a
	 * {@link Delimiter#CORRELATION_ID} character from each other. Any portion
	 * of the full Correlation-TID not being set is represented by a "&lt;?&gt;"
	 * sequence defined by {@link Literal#UNKNOWN}.
	 * 
	 * @return The full Correlation-TID.
	 */
	public static String toFullCorrelationId() {
		String theFullCorrelationId = "";
		String eId;
		final Correlation[] correlations = values();
		for ( int i = correlations.length - 1; i >= 0; i-- ) {
			if ( theFullCorrelationId.length() != 0 ) {
				theFullCorrelationId += Delimiter.CORRELATION_ID.getChar();
			}
			eId = correlations[i].getId();
			if ( eId == null ) {
				eId = Literal.UNKNOWN.getValue();
			}
			theFullCorrelationId += eId;
		}
		return theFullCorrelationId;
	}

	/**
	 * Evaluates whether any of the given Correlation-IDs is present (not null).
	 * 
	 * @return True if at least one Correlation-TID is not null.
	 */
	public static boolean hasAnyCorrelationId() {
		for ( Correlation aCorrelation : values() ) {
			if ( aCorrelation.getId() != null ) {
				return true;
			}
		}
		return false;
	}
}
