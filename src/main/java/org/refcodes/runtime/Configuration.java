// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.runtime;

import org.refcodes.data.CharSet;
import org.refcodes.data.Delimiter;
import org.refcodes.mixin.KeyAccessor.KeyBuilder;
import org.refcodes.mixin.KeyAccessor.KeyProperty;
import org.refcodes.struct.PathMap;

/**
 * Converts a key to the format of a system property (camel-case) or an
 * environment variable (snake-case in upper-case).
 */
public class Configuration implements KeyProperty<String>, KeyBuilder<String, Configuration> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _key = null;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getKey() {
		return _key;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setKey( String aKey ) {
		_key = aKey;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Configuration withKey( String aKey ) {
		setKey( aKey );
		return this;
	}

	/**
	 * Converts the key as of {@link #getKey()} to a path (as of
	 * {@link PathMap}). A path begins with a path delimiter "/" (as of
	 * {@link Delimiter#PATH} and with all non alphanumeric digits being
	 * replaced by the path delimiter. Any non alphanumeric sequence is
	 * truncated to a single path delimiter.
	 * 
	 * @return The path representation of the according key.
	 */
	public String toPath() {
		return toPath( getKey() );
	}

	/**
	 * Converts the provided key as of {@link #getKey()} to a path (as of
	 * {@link PathMap}). A path begins with a path delimiter "/" (as of
	 * {@link Delimiter#PATH} and with all non alphanumeric digits being
	 * replaced by the path delimiter. Any non alphanumeric sequence is
	 * truncated to a single path delimiter.
	 * 
	 * @param aKey The key to be converted.
	 * 
	 * @return The path representation of the according key.
	 */
	public String toPath( String aKey ) {
		if ( aKey == null || aKey.isEmpty() ) {
			return aKey;
		}
		final String theKey = Configuration.asNormalized( aKey, Delimiter.PATH.getChar() );
		return theKey != null ? Delimiter.PATH.getChar() + theKey.toLowerCase() : theKey;
	}

	/**
	 * Converts the key as of {@link #getKey()} to a system property. A system
	 * property is the camel-case version of the according key.
	 * 
	 * @return The camel-case representation of the according key.
	 */
	public String toSystemProperty() {
		return toSystemProperty( getKey() );
	}

	/**
	 * Converts the key as of {@link #getKey()} to an environment variable. An
	 * environment variable is the upper-case snake-case version of the
	 * according key.
	 * 
	 * @return The upper-case snake-case representation of the according key.
	 */
	public String toEnvironmentVariable() {
		return toEnvironmentVariable( getKey() );
	}

	/**
	 * Converts the provided key to a system property. A system property is the
	 * camel-case version of the according key.
	 * 
	 * @param aKey The key to be converted.
	 * 
	 * @return The camel-case representation of the according key.
	 */
	public String toSystemProperty( String aKey ) {
		if ( aKey == null || aKey.isEmpty() ) {
			return aKey;
		}
		final String theKey = Configuration.asNormalized( aKey, Delimiter.NAMESPACE.getChar() );
		return theKey != null ? theKey.toLowerCase() : theKey;
	}

	/**
	 * Converts the provided key to an environment variable. An environment
	 * variable is the upper-case snake-case version of the according key.
	 * 
	 * @param aKey The key to be converted.
	 * 
	 * @return The upper-case snake-case representation of the according key.
	 */
	public String toEnvironmentVariable( String aKey ) {
		if ( aKey == null || aKey.isEmpty() ) {
			return aKey;
		}
		final String theKey = Configuration.asNormalized( aKey, Delimiter.SNAKE_CASE.getChar() );
		return theKey != null ? theKey.toUpperCase() : theKey;
	}

	/**
	 * Returns the according system's property after having converted the
	 * provided key to a system property. A system property is the camel-case
	 * version of the according key.
	 * 
	 * @param aKey The value of the key to be converted.
	 * 
	 * @return The system's property of the camel-case representation of the
	 *         according key.
	 */
	public String getSystemProperty( String aKey ) {
		final String theKey = toSystemProperty( aKey );
		return System.getProperty( theKey != null ? theKey : aKey );
	}

	/**
	 * Returns the according environment variable after having converted the
	 * provided key to an environment variable. An environment variable is the
	 * upper-case snake-case version of the according key.
	 * 
	 * @param aKey The value of the key to be converted.
	 * 
	 * @return The environment variable of the upper-case snake-case
	 *         representation of the according key.
	 */
	public String getEnvironmentVariable( String aKey ) {
		final String theKey = toEnvironmentVariable( aKey );
		return System.getenv( theKey != null ? theKey : aKey );
	}

	/**
	 * Normalizes the provided text.
	 * 
	 * @param aText The text to be normalized.
	 * @param aSeparator The character to be used for normalization.
	 * 
	 * @return The normalized {@link String} (can be null).
	 */
	protected static String asNormalized( String aText, char aSeparator ) {

		if ( aText == null || aText.isEmpty() ) {
			return null;
		}

		final String theRegexSeparator = CharSet.REGEX_SPECIAL_CHARS.hasChar( aSeparator ) ? "\\" + aSeparator : "" + aSeparator;
		final String theRegexDoubleSeparator = theRegexSeparator + theRegexSeparator;
		final String theDoubleSeparator = aSeparator + "" + aSeparator;
		final String theSeparator = aSeparator + "";
		if ( aText == null || aText.isEmpty() ) {
			return aText;
		}
		final StringBuilder theBuffer = new StringBuilder();
		while ( aText.length() > 0 && !Character.isAlphabetic( aText.charAt( 0 ) ) && !Character.isDigit( aText.charAt( 0 ) ) ) {
			aText = aText.substring( 1 );
		}
		while ( aText.length() > 0 && !Character.isAlphabetic( aText.charAt( aText.length() - 1 ) ) && !Character.isDigit( aText.charAt( aText.length() - 1 ) ) ) {
			aText = aText.substring( 0, aText.length() - 1 );
		}

		if ( aText == null || aText.isEmpty() ) {
			return null;
		}

		theBuffer.append( aText.charAt( 0 ) );
		for ( int i = 1; i < aText.length(); i++ ) {
			if ( Character.isLowerCase( aText.charAt( i - 1 ) ) && Character.isUpperCase( aText.charAt( i ) ) ) {
				theBuffer.append( aSeparator );
			}
			if ( !Character.isAlphabetic( aText.charAt( i ) ) && !Character.isDigit( aText.charAt( i ) ) ) {
				theBuffer.append( aSeparator );
			}
			else {
				if ( Character.isDigit( aText.charAt( i ) ) && theBuffer.length() != 0 && Character.isAlphabetic( theBuffer.charAt( theBuffer.length() - 1 ) ) ) {
					theBuffer.append( aSeparator );
				}
				else if ( Character.isAlphabetic( aText.charAt( i ) ) && theBuffer.length() != 0 && Character.isDigit( theBuffer.charAt( theBuffer.length() - 1 ) ) ) {
					theBuffer.append( aSeparator );
				}

				theBuffer.append( aText.charAt( i ) );
			}
		}
		String theNormalited = theBuffer.toString();
		while ( theNormalited.contains( theDoubleSeparator ) ) {
			theNormalited = theNormalited.replaceAll( theRegexDoubleSeparator, theSeparator );
		}
		return theNormalited.toLowerCase();
	}
}
