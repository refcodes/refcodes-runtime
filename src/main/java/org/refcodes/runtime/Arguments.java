// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.runtime;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.refcodes.data.ArgsPrefix;
import org.refcodes.data.Delimiter;
import org.refcodes.data.Literal;

/**
 * This utility class provides method useful for the refcodes-cli artifact and
 * whose implementation has been motivated by the implementation of the
 * refcodes-cli artifact.
 */
public final class Arguments {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new console utility.
	 */
	private Arguments() {}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates a {@link Map} from the provided command line arguments using the
	 * {@link ArgsPrefix#toPrefixes()} prefixes to identify the keys (and the
	 * values) from the provided arguments.
	 * 
	 * @param aArgs The command line arguments to convert to properties.
	 * 
	 * @return The {@link Map} containing the determined properties.
	 */
	public static Map<String, String> toProperties( String[] aArgs ) {
		return toProperties( aArgs, ArgsPrefix.toPrefixes(), Delimiter.INDEX.getChar() );

	}

	/**
	 * Creates a {@link Map} from the provided command line arguments using the
	 * provided prefixes to identify the keys (and the values) from the provided
	 * arguments.
	 * 
	 * @param aArgs The command line arguments to convert to properties.
	 * @param aPrefixes The prefixes to be used to identify options.
	 * @param aDelimiter The delimiter to use when generating non colliding
	 *        keys.
	 * 
	 * @return The {@link Map} containing the determined properties.
	 */
	public static Map<String, String> toProperties( String[] aArgs, Collection<String> aPrefixes, char aDelimiter ) {
		return toProperties( aArgs, aPrefixes.toArray( new String[aPrefixes.size()] ), aDelimiter );
	}

	/**
	 * Creates a {@link Map} from the provided command line arguments using the
	 * provided prefixes to identify the keys (and the values) from the provided
	 * arguments.
	 * 
	 * @param aArgs The command line arguments to convert to properties.
	 * @param aPrefixes The prefixes to be used to identify options.
	 * @param aDelimiter The delimiter to use when generating non colliding
	 *        keys.
	 * 
	 * @return The {@link Map} containing the determined properties.
	 */
	public static Map<String, String> toProperties( String[] aArgs, String[] aPrefixes, char aDelimiter ) {
		final Map<String, String> theProperties = new HashMap<>();
		String eOption = null;
		String eNextOption;
		String eArg;
		for ( int i = 0; i < aArgs.length; i++ ) {
			eArg = aArgs[i];
			final boolean isLast = i == aArgs.length - 1;
			eNextOption = toOption( eArg, aPrefixes );
			if ( eOption != null ) {
				if ( eNextOption == null ) {
					putProperty( theProperties, eOption, eArg, aDelimiter );
					eOption = null;
				}
				else {
					putProperty( theProperties, eOption, Literal.TRUE.getValue(), aDelimiter );
					eOption = eNextOption;
					if ( isLast ) {
						putProperty( theProperties, eOption, Literal.TRUE.getValue(), aDelimiter );
					}
				}
			}
			else {
				if ( eNextOption != null ) {
					eOption = eNextOption;
					if ( isLast ) {
						putProperty( theProperties, eOption, Literal.TRUE.getValue(), aDelimiter );
					}
				}
				else {
					putProperty( theProperties, null, eArg, aDelimiter );
				}
			}
		}
		return theProperties;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the next free key in the properties in case of key name
	 * collisions..
	 * 
	 * @param aProperties The properties for which to determine the next key.
	 * @param aKey The key for which to test for name collisions.
	 * @param aDelimiter The delimiter to use when creating non colliding keys.
	 * 
	 * @return The next non colliding key or null if there are no collisions.
	 */
	private static String toNextKey( Map<String, String> aProperties, String aKey, char aDelimiter ) {
		if ( aProperties.containsKey( aKey ) || aProperties.containsKey( aKey + aDelimiter + "0" ) ) {
			String eNextKey;
			int index = 0;
			eNextKey = aKey + aDelimiter + index;
			while ( aProperties.containsKey( eNextKey ) ) {
				index++;
				eNextKey = aKey + aDelimiter + index;
			}
			return eNextKey;
		}
		return null;
	}

	/**
	 * When the provided argument starts with one of the provided prefixes, then
	 * the portion of the argument without the prefix is returned, which
	 * represents the option in question.
	 * 
	 * @param aArg The argument for which to retrieve the option's name.
	 * @param aPrefixes The prefixes to use when determining options.
	 * 
	 * @return Either the option's name or null if the argument does not
	 *         represent an option.
	 */
	private static String toOption( String aArg, String... aPrefixes ) {
		// Make sure we have an order with the "longest" prefix first |-->
		Arrays.sort( aPrefixes, Collections.reverseOrder() );
		// Make sure we have an order with the "longest" prefix first <--|

		for ( String ePrefix : aPrefixes ) {
			if ( aArg.startsWith( ePrefix ) ) {
				return aArg.substring( ePrefix.length() );
			}
		}
		return null;
	}

	/**
	 * Puts a property and resolves the key in case of name collisions.
	 * 
	 * @param aProperties The properties which to modify.
	 * @param aKey The key which to put.
	 * @param aValue The value to put.
	 * @param aDelimiter The delimiter to use when generating non colliding
	 *        keys.
	 */
	private static void putProperty( Map<String, String> aProperties, String aKey, String aValue, char aDelimiter ) {
		if ( aProperties.containsKey( aKey ) ) {
			String eNextKey = toNextKey( aProperties, aKey, aDelimiter );
			aProperties.put( eNextKey, aProperties.remove( aKey ) );
			eNextKey = toNextKey( aProperties, aKey, aDelimiter );
			aProperties.put( eNextKey, aValue );
		}
		else if ( aProperties.containsKey( aKey + aDelimiter + "0" ) ) {
			final String eNextKey = toNextKey( aProperties, aKey, aDelimiter );
			aProperties.put( eNextKey, aValue );
		}
		else {
			aProperties.put( aKey, aValue );
		}
	}
}
