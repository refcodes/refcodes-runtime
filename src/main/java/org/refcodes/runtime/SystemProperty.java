// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.runtime;

import java.util.Map;

import org.refcodes.mixin.EnabledAccessor.EnabledProperty;
import org.refcodes.mixin.KeyAccessor;
import org.refcodes.mixin.ValueAccessor.ValueProperty;
import org.refcodes.struct.PathMap;
import org.refcodes.struct.StructureUtility;

/**
 * {@link SystemProperty} define values to be used at runtime. Them are passed
 * to a JVM by prefixing a "-D" to the actual "property=value" pair.
 */
public enum SystemProperty implements KeyAccessor<String>, ValueProperty<String>, EnabledProperty {

	/**
	 * When {@link #isEnabled()}, then REFCODES.ORG artifacts' unit tests are
	 * (more) verbose,
	 */
	LOG_TESTS("log.tests"),

	/**
	 * When {@link #isEnabled()}, then additional REFCODES.ORG artifacts' debug
	 * information may be logged.
	 */
	LOG_DEBUG("log.debug"),

	/**
	 * When set, then the <code>ArgsFilter</code> from artifact
	 * <code>refcodes-cli</code> (of group <code>org.refcodes</code>) with the
	 * according propertie's value is applied when parsing command line
	 * arguments via the <code>org.refcodes</code> command line parser (any
	 * implementation of the <code>ArgsParser</code> type).
	 * 
	 * Values as of the <code>ArgsFilter</code> enumeration may be
	 * <code>D</code>, <code>XX</code>, <code>D_XX</code> or <code>NONE</code>.
	 */
	ARGS_FILTER("args.filter"),

	/**
	 * When set, then the <code>SyntaxNotation</code> from artifact
	 * <code>refcodes-cli</code> (of group <code>org.refcodes</code>) with the
	 * according propertie's value is applied when parsing command line
	 * arguments via the <code>org.refcodes</code> command line parser (any
	 * implementation of the <code>ArgsParser</code> type).
	 * 
	 * Values as of the <code>SyntaxNotation</code> enumeration may be
	 * <code>LOGICAL</code>, <code>GNU_POSIX</code>, <code>WINDOWS</code> or
	 * <code>VERBOSE</code>.
	 */
	ARGS_NOTATION("args.notation"),

	/**
	 * Holds the string that is the name of the system property providing
	 * information about the context in which code is currently executing. If
	 * the property returns the string given by
	 * <code>ImageInfo.PROPERTY_IMAGE_CODE_VALUE_BUILDTIME</code> the code is
	 * executing in the context of image building (e.g. in a static initializer
	 * of a class that will be contained in the image). If the property returns
	 * the string given by
	 * <code>ImageInfo.PROPERTY_IMAGE_CODE_VALUE_RUNTIME</code> the code is
	 * executing at image runtime. Otherwise the property is not set.
	 * 
	 * Holds the string that will be returned by the system property for
	 * <code>ImageInfo.PROPERTY_IMAGE_CODE_KEY</code> if code is executing in
	 * the context of image building (e.g. in a static initializer of class that
	 * will be contained in the image).
	 * 
	 * <ul>
	 * <li><code>PROPERTY_IMAGE_CODE_VALUE_BUILDTIME</code> = "buildtime"
	 * <li><code>PROPERTY_IMAGE_CODE_VALUE_RUNTIME</code> = "runtime"
	 * </ul>
	 * 
	 * See also https://www.graalvm.org/sdk/javadoc/constant-values.html
	 */
	NATIVE_IMAGE_CODE("org.graalvm.nativeimage.imagecode"),

	/**
	 * Name of the system property that holds if this image is built as a shared
	 * library or an executable. If the property is
	 * <code>ImageInfo.PROPERTY_IMAGE_KIND_VALUE_EXECUTABLE</code> the image is
	 * built as an executable. If the property is
	 * <code>ImageInfo.PROPERTY_IMAGE_KIND_VALUE_SHARED_LIBRARY</code> the image
	 * is built as a shared library.
	 * 
	 * <ul>
	 * <li><code>PROPERTY_IMAGE_KIND_VALUE_EXECUTABLE</code> = "executable"
	 * <li><code>PROPERTY_IMAGE_KIND_VALUE_SHARED_LIBRARY</code> = "shared"
	 * </ul>
	 * 
	 * See also https://www.graalvm.org/sdk/javadoc/constant-values.html
	 */
	NATIVE_IMAGE_KIND("org.graalvm.nativeimage.kind"),

	/**
	 * To set up headless mode, set the appropriate system property
	 * ("https://www.oracle.com/technetwork/articles/javase/headless-136834.html").
	 */
	JAVA_AWT_HEADLESS("java.awt.headless"),

	/**
	 * HTTP-Proxy host without a port ("http://my.company.org")
	 */
	HTTP_PROXY_HOST("http.proxyHost"),

	/**
	 * HTTP-Proxy port ("3128")
	 */
	HTTP_PROXY_PORT("http.proxyPort"),

	/**
	 * HTTPS-Proxy host without a port ("http://my.company.org")
	 */
	HTTPS_PROXY_HOST("https.proxyHost"),

	/**
	 * HTTP-Proxy port ("3128")
	 */
	HTTPS_PROXY_PORT("https.proxyPort"),

	/**
	 * SOCKS-Proxy host without a port ("http://my.company.org")
	 */
	SOCKS_PROXY_HOST("socksProxyHost"),

	/**
	 * SOCKS-Proxy port ("3128")
	 */
	SOCKS_PROXY_PORT("socksProxyPort"),

	/**
	 * When set to "true" then {@link Map} instances in data-structures are
	 * preserved instead of converting them to arrays if possible by indirectly
	 * calling {@link StructureUtility#toDataStructure(PathMap, String)} when
	 * invoking {@link PathMap#toDataStructure()}.
	 */
	STRUCT_KEEP_MAPS(StructureUtility.SYSTEM_PROPERTY_STRUCT_KEEP_MAPS), // ~ "struct.keepMaps"

	/**
	 * System property "host.seed" for the Host-Seed for host-related IDs.
	 */
	HOST_SEED("host.seed"),

	/**
	 * No-Proxy settings for the {@link #HTTP_PROXY_HOST}
	 * ({@link #HTTP_PROXY_PORT}) settings in a comma separated list
	 * ("localhost,127.0.0.0/8,127.0.1.1,127.0.1.1*,...")
	 */
	HTTP_NON_PROXY_HOSTS("http.nonProxyHosts"),

	/**
	 * No-Proxy settings for the {@link #HTTPS_PROXY_HOST}
	 * ({@link #HTTPS_PROXY_PORT}) settings in a comma separated list
	 * ("localhost,127.0.0.0/8,127.0.1.1,127.0.1.1*,...")
	 */
	HTTPS_NON_PROXY_HOSTS("https.nonProxyHosts"),

	/**
	 * Processor architecture on some systems (e.g. "AMD64").
	 */
	OS_ARCH("os.arch"),

	/**
	 * OS Name, might be something like "Linux" or "Windows"
	 */
	OS_NAME("os.name"),

	/**
	 * Pass as JVM argument via "-Dconfig.dir=path_to_your_config_dir" (where
	 * path_to_your_config_dir stands for the path to the directory where you
	 * placed configuration files such as the "<code>runtimelogger.ini</code>"
	 * file).
	 */
	CONFIG_DIR("config.dir"),

	/**
	 * The OS specific file separator is retrieved by this system property.
	 */
	FILE_SEPARATOR("file.separator"),

	/**
	 * The OS specific file encoding is retrieved by this system property.
	 */
	FILE_ENCODING("file.encoding"),

	/**
	 * The OS specific temp folder path.
	 */
	TEMP_DIR("java.io.tmpdir"),

	/** At least set on Ubuntu-Linux:. */
	PROCESS_ID("PID"),

	/**
	 * Sequence used by operating system to separate lines in text files.
	 */
	LINE_SEPARATOR("line.separator"),

	/**
	 * Operating system name.
	 */
	OPERATING_SYSTEM_NAME("os.name"),

	/**
	 * Operating system name.
	 */
	OPERATING_SYSTEM_VERSION("os.version"),

	/**
	 * Specifies the path to your application's launcher dir, e.g. the place
	 * where your JAR resides in. Pass as JVM argument via
	 * "-Dlauncher.dir=/path/to/launcher/dir".
	 */
	LAUNCHER_DIR("launcher.dir"),

	/**
	 * Pass as JVM argument via "-Dconsole.height=n" (where n stands for the
	 * number of lines).
	 */
	CONSOLE_HEIGHT("console.height"),

	/**
	 * Pass as JVM argument via "-Dconsole.width=n" (where n stands for the
	 * number of chars per row).
	 */
	CONSOLE_WIDTH("console.width"),

	/**
	 * Pass as JVM argument via <code>-Dconsole.ansi=&lt;true|false&gt;</code>
	 * where "true" or "false" forces ANSI to be used / not used by REFCODES.ORG
	 * artifacts, no matter what capabilities were detected for the hosting
	 * terminal.
	 */
	CONSOLE_ANSI("console.ansi"),

	/**
	 * The console's line-break property. Used to override any default line
	 * breaks for the REFCODES-ORG artifacts.
	 */
	CONSOLE_LINE_BREAK("console.lineBreak"),

	/**
	 * Pass as JVM argument via "<code>-Dlogger.layout=&lt;layout&gt;</code>"
	 * (where <code>&lt;layout&gt;</code> stands for the chosen layout for the
	 * REFCODES.ORG logger artifacts.).
	 */
	LOGGER_LAYOUT("logger.layout"),

	/**
	 * Pass as JVM argument via "<code>-Dlogger.style=&lt;style&gt;</code>"
	 * (where <code>&lt;style&gt;</code> stands for the chosen logger-style for
	 * the REFCODES.ORG logger artifacts.) (see
	 * <code>org.refcodes.textual.TextBoxStyle</code>).
	 */
	LOGGER_STYLE("logger.style"),

	/**
	 * System variable holding the user's home folder path.
	 */
	USER_HOME("user.home"),

	/**
	 * System variable holding the user's login name.
	 */
	USER_NAME("user.name"),

	/**
	 * System variable holding the user's login name.
	 */
	USER_LANGUAGE("user.language"),

	/**
	 * System variable holding the user's login name.
	 */
	USER_COUNTRY("user.country"),

	/**
	 * System variable holding the user's current working directory.
	 */
	USER_DIR("user.dir"),

	/**
	 * Location of the Java keystore file containing an application process's
	 * own certificate and private key.
	 */
	KEY_STORE_FILE("javax.net.ssl.keyStore"),

	/**
	 * Password to access the private key from the keystore file specified by
	 * {@link #KEY_STORE_FILE}.
	 */
	KEY_STORE_PASSWORD("javax.net.ssl.keyStorePassword"),

	/**
	 * For Java keystore file format, this property has the value "jks" (or
	 * "JKS"). Its default value is already "jks".
	 */
	KEY_STORE_TYPE("javax.net.ssl.keyStoreType"),

	/**
	 * Location of the Java keystore file containing the collection of CA
	 * certificates trusted by this application process (trust store).
	 */
	TRUST_STORE_FILE("javax.net.ssl.trustStore"),

	/**
	 * Password to unlock the keystore file (store password) specified by
	 * {@link #TRUST_STORE_FILE}.
	 */
	TRUST_STORE_PASSWORD("javax.net.ssl.trustStorePassword"),

	/**
	 * For Java keystore file format, this property has the value "jks" (or
	 * "JKS"). Its default value is already "jks".
	 */
	TRUST_STORE_TYPE("javax.net.ssl.trustStoreType"),

	/**
	 * When set to <code>true</code> then unmarshaling (XML) documents will
	 * preserve the preserve the root element (envelope) when even when merely
	 * acting as an envelope. As an (XML) document requires a root element, the
	 * root element often is provided merely as of syntactic reasons and must be
	 * omitted as of semantic reasons. Unmarshaling functionality therefore may
	 * by default skip the root elelemt, as this is considered merely to serve
	 * as an envelope. This behavior can be overridden by setting this property
	 * to <code>true</code>.
	 */
	DOCUMENT_ENVELOPE("document.envelope"),

	/**
	 * When set to <code>true</code> then marshaling (XML) documents will
	 * preserve array index information by using an index attribute for array
	 * elements in the produced XML.
	 */
	DOCUMENT_ARRAY_INDEX("document.array.index");

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _systemPropertyName;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new system property.
	 *
	 * @param aSystemPropertyName the system property name
	 */
	private SystemProperty( String aSystemPropertyName ) {
		_systemPropertyName = aSystemPropertyName;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getKey() {
		return _systemPropertyName;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getValue() {
		return System.getProperty( _systemPropertyName );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setValue( String aValue ) {
		System.setProperty( _systemPropertyName, aValue );

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isEnabled() {
		return Boolean.getBoolean( _systemPropertyName );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setEnabled( boolean isEnabled ) {
		setValue( Boolean.toString( isEnabled ) );
	}

	/**
	 * Gets the value for the provided properties, if non was found then the
	 * default value is taken. A {@link SystemProperty} elements wins over the
	 * {@link EnvironmentVariable} elements. The preceding
	 * {@link EnvironmentVariable} element wins over the succeeding
	 * {@link EnvironmentVariable} element. The default value is taken if non
	 * property had a value (a String with length &gt; 0).
	 * 
	 * @param aDefaultValue The default value to take when none other value was
	 *        set.
	 * @param aSystemProperty The system-property passed via <code>java
	 *        -D&lt;name&gt;=&lt;value&gt;</code>
	 * @param aEnvironmentProperties The properties looked for in the system's
	 *        environment variables.
	 * 
	 * @return The best fitting value.
	 */
	public static String toPropertyValue( String aDefaultValue, SystemProperty aSystemProperty, EnvironmentVariable... aEnvironmentProperties ) {
		String theValue = aSystemProperty.getValue();
		if ( theValue != null && theValue.length() > 0 ) {
			return theValue;
		}
		if ( aEnvironmentProperties != null ) {
			for ( EnvironmentVariable eProperty : aEnvironmentProperties ) {
				theValue = eProperty.getValue();
				if ( theValue != null && theValue.length() > 0 ) {
					return theValue;
				}
			}
		}
		if ( theValue != null && theValue.length() > 0 ) {
			return theValue;
		}
		return aDefaultValue;
	}

	/**
	 * Gets the value for the provided properties, if non was found then null is
	 * returned. A {@link SystemProperty} elements wins over the
	 * {@link EnvironmentVariable} elements. The preceding
	 * {@link EnvironmentVariable} element wins over the succeeding
	 * {@link EnvironmentVariable} element. A null is taken if non property had
	 * a value (a String with length &gt; 0).
	 * 
	 * @param aSystemProperty The system-property passed via <code>java
	 *        -D&lt;name&gt;=&lt;value&gt;</code>
	 * @param aEnvironmentProperties The properties looked for in the system's
	 *        environment variables.
	 * 
	 * @return The best fitting value or null if none was detected.
	 */
	public static String toPropertyValue( SystemProperty aSystemProperty, EnvironmentVariable... aEnvironmentProperties ) {
		String theValue = null;
		if ( aSystemProperty != null ) {
			theValue = aSystemProperty.getValue();
			if ( theValue != null && theValue.length() != 0 ) {
				return theValue;
			}
		}
		if ( aEnvironmentProperties != null ) {
			for ( EnvironmentVariable eProperty : aEnvironmentProperties ) {
				theValue = eProperty.getValue();
				if ( theValue != null && theValue.length() != 0 ) {
					return theValue;
				}
			}
		}
		return null;
	}
}
