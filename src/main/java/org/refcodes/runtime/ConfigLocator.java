// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.runtime;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.refcodes.data.Folder;

/**
 * This enumeration represents configuration folder locations (of type
 * {@link File}) and configuration file retrieval as of the chosen configuration
 * mode.
 */
public enum ConfigLocator {

	/**
	 * Any of {@link #USER_WORKING_DIRECTORY}, {@link #APPLICATION_DIR},
	 * {@link #APPLICATION_DIR_SETTINGS}, {@link #APPLICATION_DIR_ETC},
	 * {@link #APPLICATION_DIR_CONFIG}, {@link #USER_HOME_DOT_CONFIG} or
	 * {@link #HOST_ETC} in this precedence.
	 */
	DEFAULT(true, true, true, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, true),

	/**
	 * Representation of the (in Unix alike systems) <code>/etc/</code> folder.
	 */
	HOST_ETC(true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false),

	/**
	 * Representation of the folder in which the application resides (e.g. the
	 * `JAR` file).
	 */

	APPLICATION_DIR(false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false),

	/**
	 * Representation of the <code>config</code> folder relative to the folder
	 * in which the application resides (e.g. the `JAR` file).
	 */

	APPLICATION_DIR_CONFIG(false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false),

	/**
	 * Representation of the <code>etc</code> folder relative to the folder in
	 * which the application resides (e.g. the `JAR` file).
	 */
	APPLICATION_DIR_ETC(false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false),

	/**
	 * Representation of the <code>settings</code> folder relative to the folder
	 * in which the application resides (e.g. the `JAR` file).
	 */
	APPLICATION_DIR_SETTINGS(false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false),

	/**
	 * Representation of the <code>.config</code> folder relative to the folder
	 * in which the application resides (e.g. the `JAR` file).
	 */
	APPLICATION_DIR_DOT_CONFIG(false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false),

	/**
	 * Representation of the <code>.etc</code> folder relative to the folder in
	 * which the application resides (e.g. the `JAR` file).
	 */
	APPLICATION_DIR_DOT_ETC(false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false),

	/**
	 * Representation of the <code>.settings</code> folder relative to the
	 * folder in which the application resides (e.g. the `JAR` file).
	 */
	APPLICATION_DIR_DOT_SETTINGS(false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false),

	/**
	 * Representation of all the <code>APPLICATION_DIR_*</code> folders as of
	 * the {@link ConfigLocator} enumeration.
	 */
	APPLICATION_DIR_ALL(false, true, true, true, true, true, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false),

	/**
	 * Representation of the <code>config</code> folder in the parent directory
	 * of the folder in which the application resides (e.g. the `JAR` file).
	 */
	APPLICATION_PARENT_CONFIG(false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false),

	/**
	 * Representation of the <code>etc</code> folder in the parent directory of
	 * the folder in which the application resides (e.g. the `JAR` file).
	 */
	APPLICATION_PARENT_ETC(false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false),

	/**
	 * Representation of the <code>settings</code> folder in the parent
	 * directory of the folder in which the application resides (e.g. the `JAR`
	 * file).
	 */
	APPLICATION_PARENT_SETTINGS(false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false),

	/**
	 * Representation of the <code>.config</code> folder in the parent directory
	 * of the folder in which the application resides (e.g. the `JAR` file).
	 */
	APPLICATION_PARENT_DOT_CONFIG(false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false),

	/**
	 * Representation of the <code>.etc</code> folder in the parent directory of
	 * the folder in which the application resides (e.g. the `JAR` file).
	 */
	APPLICATION_PARENT_DOT_ETC(false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false),

	/**
	 * Representation of the <code>.settings</code> folder in the parent
	 * directory of the folder in which the application resides (e.g. the `JAR`
	 * file).
	 */
	APPLICATION_PARENT_DOT_SETTINGS(false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false),

	/**
	 * Representation of all the <code>APPLICATION_PARRENT_*</code> folders as
	 * of the {@link ConfigLocator} enumeration.
	 */
	APPLICATION_PARENT_ALL(false, false, false, false, false, false, false, false, true, true, true, true, true, true, false, false, false, false, false, false, false, false),

	/**
	 * Representation of all the <code>APPLICATION_*</code> folders.
	 */
	APPLICATION_ALL(false, true, true, true, true, true, true, true, true, true, true, true, true, true, false, false, false, false, false, false, false, false),

	/**
	 * Representation of the executing user's home folder.
	 */
	USER_HOME(false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false),

	/**
	 * Representation of the <code>config</code> folder relative to the
	 * executing user's home folder.
	 */
	USER_HOME_CONFIG(false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false),

	/**
	 * Representation of the <code>etc</code> folder relative to the executing
	 * user's home folder.
	 */
	USER_HOME_ETC(false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false),

	/**
	 * Representation of the <code>settings</code> folder relative to the
	 * executing user's home folder.
	 */
	USER_HOME_SETTINGS(false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false),

	/**
	 * Representation of the <code>.config</code> folder relative to the
	 * executing user's home folder.
	 */
	USER_HOME_DOT_CONFIG(false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false),

	/**
	 * Representation of the <code>.etc</code> folder relative to the executing
	 * user's home folder.
	 */
	USER_HOME_DOT_ETC(false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false),

	/**
	 * Representation of the <code>.settings</code> folder relative to the
	 * executing user's home folder.
	 */
	USER_HOME_DOT_SETTINGS(false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false),

	/**
	 * Representation of all the <code>USER_HOME_*</code> folders.
	 */
	USER_HOME_ALL(false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, true, true, true, true, true, true, false),

	/**
	 * Consider just the current user's current working directory.
	 */
	USER_WORKING_DIRECTORY(false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true),

	/**
	 * Representation of all the <code>USER_HOME_*</code> folders as well as the
	 * current user's current working directory
	 * <code>USER_WORKING_DIRECTORY</code>.
	 */
	USER_HOME_WORKING_DIRECTORY_ALL(false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, true, true, true, true, true, true, true),

	/**
	 * Representation of folders as of the {@link ConfigLocator} enumeration.
	 */
	ALL(true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, false),

	/**
	 * None as of the folders as of the {@link ConfigLocator} enumeration are
	 * considered.
	 */
	ABSOLUTE(false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false);

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private boolean _isHostEtc;
	private boolean _isApplicationBase;
	private boolean _isApplicationBaseConfig;
	private boolean _isApplicationBaseEtc;
	private boolean _isApplicationBaseSettings;
	private boolean _isApplicationBaseDotConfig;
	private boolean _isApplicationBaseDotEtc;
	private boolean _isApplicationBaseDotSettings;
	private boolean _isApplicationParentConfig;
	private boolean _isApplicationParentEtc;
	private boolean _isApplicationParentSettings;
	private boolean _isApplicationParentDotConfig;
	private boolean _isApplicationParentDotEtc;
	private boolean _isApplicationParentDotSettings;
	private boolean _isUserHome;
	private boolean _isUserHomeConfig;
	private boolean _isUserHomeEtc;
	private boolean _isUserHomeSettings;
	private boolean _isUserHomeDotConfig;
	private boolean _isUserHomeDotEtc;
	private boolean _isUserHomeDotSettings;
	private boolean _isUserWorkingDir;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private ConfigLocator( boolean isHostEtc, boolean isApplicationBase, boolean isApplicationBaseConfig, boolean isApplicationBaseEtc, boolean isApplicationBaseSettings, boolean isApplicationBaseDotConfig, boolean isApplicationBaseDotEtc, boolean isApplicationBaseDotSettings, boolean isApplicationParentConfig, boolean isApplicationParentEtc, boolean isApplicationParentSettings, boolean isApplicationParentDotConfig, boolean isApplicationParentDotEtc, boolean isApplicationParentDotSettings, boolean isUserHome, boolean isUserHomeConfig, boolean isUserHomeEtc, boolean isUserHomeSettings, boolean isUserHomeDotConfig, boolean isUserHomeDotEtc, boolean isUserHomeDotSettings, boolean isUserWorkingDir ) {
		_isHostEtc = isHostEtc;
		_isApplicationBase = isApplicationBase;
		_isApplicationBaseConfig = isApplicationBaseConfig;
		_isApplicationBaseEtc = isApplicationBaseEtc;
		_isApplicationBaseSettings = isApplicationBaseSettings;
		_isApplicationBaseDotConfig = isApplicationBaseDotConfig;
		_isApplicationBaseDotEtc = isApplicationBaseDotEtc;
		_isApplicationBaseDotSettings = isApplicationBaseDotSettings;
		_isApplicationParentConfig = isApplicationParentConfig;
		_isApplicationParentEtc = isApplicationParentEtc;
		_isApplicationParentSettings = isApplicationParentSettings;
		_isApplicationParentDotConfig = isApplicationParentDotConfig;
		_isApplicationParentDotEtc = isApplicationParentDotEtc;
		_isApplicationParentDotSettings = isApplicationParentDotSettings;
		_isUserHome = isUserHome;
		_isUserHomeConfig = isUserHomeConfig;
		_isUserHomeEtc = isUserHomeEtc;
		_isUserHomeSettings = isUserHomeSettings;
		_isUserHomeDotConfig = isUserHomeDotConfig;
		_isUserHomeDotEtc = isUserHomeDotEtc;
		_isUserHomeDotSettings = isUserHomeDotSettings;
		_isUserWorkingDir = isUserWorkingDir;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Determines whether to consider the host's "/etc" folder.
	 * 
	 * @return True in case the according folder is considered.
	 */
	public boolean isHostEtc() {
		return _isHostEtc;
	}

	/**
	 * Determines whether to consider the application's base (the folder where
	 * the JAR resides) folder.
	 * 
	 * @return True in case the according folder is considered.
	 */
	public boolean isApplicationBase() {
		return _isApplicationBase;
	}

	/**
	 * Determines whether to consider the application's base (the folder where
	 * the JAR resides) "config" subfolder.
	 * 
	 * @return True in case the according folder is considered.
	 */
	public boolean isApplicationBaseConfig() {
		return _isApplicationBaseConfig;
	}

	/**
	 * Determines whether to consider the application's base (the folder where
	 * the JAR resides) "etc" subfolder.
	 * 
	 * @return True in case the according folder is considered.
	 */
	public boolean isApplicationBaseEtc() {
		return _isApplicationBaseEtc;
	}

	/**
	 * Determines whether to consider the application's base (the folder where
	 * the JAR resides) "settings" subfolder.
	 * 
	 * @return True in case the according folder is considered.
	 */
	public boolean isApplicationBaseSettings() {
		return _isApplicationBaseSettings;
	}

	/**
	 * Determines whether to consider the application's base (the folder where
	 * the JAR resides) ".config" subfolder.
	 * 
	 * @return True in case the according folder is considered.
	 */
	public boolean isApplicationBaseDotConfig() {
		return _isApplicationBaseDotConfig;
	}

	/**
	 * Determines whether to consider the application's base (the folder where
	 * the JAR resides) ".etc" subfolder.
	 * 
	 * @return True in case the according folder is considered.
	 */
	public boolean isApplicationBaseDotEtc() {
		return _isApplicationBaseDotEtc;
	}

	/**
	 * Determines whether to consider the application's base (the folder where
	 * the JAR resides) ".settings" subfolder.
	 * 
	 * @return True in case the according folder is considered.
	 */
	public boolean isApplicationBaseDotSettings() {
		return _isApplicationBaseDotSettings;
	}

	/**
	 * Determines whether to consider the application's parent (the parent
	 * folder of the folder where the JAR resides) "config" subfolder.
	 * 
	 * @return True in case the according folder is considered.
	 */
	public boolean isApplicationParentConfig() {
		return _isApplicationParentConfig;
	}

	/**
	 * Determines whether to consider the application's parent (the parent
	 * folder of the folder where the JAR resides) "etc" subfolder.
	 * 
	 * @return True in case the according folder is considered.
	 */
	public boolean isApplicationParentEtc() {
		return _isApplicationParentEtc;
	}

	/**
	 * Determines whether to consider the application's parent (the parent
	 * folder of the folder where the JAR resides) "settings" subfolder.
	 * 
	 * @return True in case the according folder is considered.
	 */
	public boolean isApplicationParentSettings() {
		return _isApplicationParentSettings;
	}

	/**
	 * Determines whether to consider the application's parent (the parent
	 * folder of the folder where the JAR resides) ".config" subfolder.
	 * 
	 * @return True in case the according folder is considered.
	 */
	public boolean isApplicationParentDotConfig() {
		return _isApplicationParentDotConfig;
	}

	/**
	 * Determines whether to consider the application's parent (the parent
	 * folder of the folder where the JAR resides) ".etc" subfolder.
	 * 
	 * @return True in case the according folder is considered.
	 */
	public boolean isApplicationParentDotEtc() {
		return _isApplicationParentDotEtc;
	}

	/**
	 * Determines whether to consider the application's parent (the parent
	 * folder of the folder where the JAR resides) ".settings" subfolder.
	 * 
	 * @return True in case the according folder is considered.
	 */
	public boolean isApplicationParentDotSettings() {
		return _isApplicationParentDotSettings;
	}

	/**
	 * Determines whether to consider the user's home "~" folder.
	 * 
	 * @return True in case the according folder is considered.
	 */
	public boolean isUserHome() {
		return _isUserHome;
	}

	/**
	 * Determines whether to consider the user's home "~" subfolder "config".
	 * 
	 * @return True in case the according folder is considered.
	 */
	public boolean isUserHomeConfig() {
		return _isUserHomeConfig;
	}

	/**
	 * Determines whether to consider the user's home "~" subfolder "etc".
	 * 
	 * @return True in case the according folder is considered.
	 */
	public boolean isUserHomeEtc() {
		return _isUserHomeEtc;
	}

	/**
	 * Determines whether to consider the user's home "~" subfolder "settings".
	 * 
	 * @return True in case the according folder is considered.
	 */
	public boolean isUserHomeSettings() {
		return _isUserHomeSettings;
	}

	/**
	 * Determines whether to consider the user's home "~" subfolder ".config".
	 * 
	 * @return True in case the according folder is considered.
	 */
	public boolean isUserHomeDotConfig() {
		return _isUserHomeDotConfig;
	}

	/**
	 * Determines whether to consider the user's home "~" subfolder ".etc".
	 * 
	 * @return True in case the according folder is considered.
	 */
	public boolean isUserHomeDotEtc() {
		return _isUserHomeDotEtc;
	}

	/**
	 * Determines whether to consider the user's home "~" subfolder ".settings".
	 * 
	 * @return True in case the according folder is considered.
	 */
	public boolean isUserHomeDotSettings() {
		return _isUserHomeDotSettings;
	}

	/**
	 * Determines whether to consider the user's current working directory (as
	 * for example bash's <code>pwd</code> would print out).
	 * 
	 * @return True in case the according folder is considered.
	 */
	public boolean isUserWorkingDir() {
		return _isUserWorkingDir;
	}

	/**
	 * Retrieves the first {@link File} identified by the given filename located
	 * in one of the according enumeration's configuration locations. If
	 * provided, regards the folder as provided by the system property
	 * {@link SystemProperty#CONFIG_DIR} with highest priority. Also takes build
	 * environment folders into account (least highest priority) when the launch
	 * folder points to an according build environment (such as maven's "target"
	 * folder).
	 * 
	 * @param aFileName The filename for which to determine the according
	 *        {@link File}.
	 * 
	 * @return The according {@link File} or null if none such file was
	 *         detected.
	 */
	public File toFile( String aFileName ) {
		return toFile( aFileName, (File[]) null );
	}

	/**
	 * Retrieves the first {@link File} identified by the given filename located
	 * in one of the according enumeration's configuration locations. If
	 * provided, regards the folder as provided by the system property
	 * {@link SystemProperty#CONFIG_DIR} with highest priority. The
	 * programmatically provided folders are regarded with second highest
	 * priority. Also takes build environment folders into account (least
	 * highest priority) when the launch folder points to an according build
	 * environment (such as maven's "target" folder).
	 * 
	 * @param aFileName The filename for which to determine the according
	 *        {@link File}.
	 * @param aFolders The folders to also take into account.
	 * 
	 * @return The according {@link File} or null if none such file was
	 *         detected.
	 */
	public File toFile( String aFileName, File... aFolders ) {
		File eFile = new File( aFileName );
		// |--> Absolute path?
		if ( this == ABSOLUTE ) {
			if ( eFile.isAbsolute() && eFile.exists() && eFile.isFile() && eFile.canRead() ) {
				return eFile;
			}
		}
		// Absolute path? <--|
		else {

			for ( File eFolder : getFolders( aFolders ) ) {
				eFile = toFile( aFileName, eFolder );
				if ( eFile != null ) {
					return eFile;
				}
			}
		}
		return null;
	}

	/**
	 * Returns the folders to be examined by the according enumeration's
	 * configuration in the order as of evaluation. If provided, regards the
	 * folder as provided by the system property
	 * {@link SystemProperty#CONFIG_DIR} with highest priority. Also takes build
	 * environment folders into account (least highest priority) when the launch
	 * folder points to an according build environment (such as maven's "target"
	 * folder).
	 * 
	 * @return The folders to be examined.
	 */
	public File[] getFolders() {
		return getFolders( (File[]) null );
	}

	/**
	 * Returns the folders to be examined by the according enumeration's
	 * configuration in the order as of evaluation. If provided, regards the
	 * folder as provided by the system property
	 * {@link SystemProperty#CONFIG_DIR} with highest priority. The
	 * programmatically provided folders are regarded with second highest
	 * priority. Also takes build environment folders into account (least
	 * highest priority) when the launch folder points to an according build
	 * environment (such as maven's "target" folder).
	 * 
	 * @param aFolders The folders to also take into account.
	 * 
	 * @return The folders to be examined.
	 */
	public File[] getFolders( File... aFolders ) {
		final List<File> theFolders = new ArrayList<>();

		addFolder( toFolder( SystemProperty.CONFIG_DIR.getValue() ), theFolders );

		if ( aFolders != null ) {
			for ( File eFile : aFolders ) {
				addFolder( eFile, theFolders );
			}
		}

		if ( _isUserWorkingDir ) {
			addFolder( Host.toUserWorkingDir(), theFolders );
		}

		final File theLauncherDir = Execution.toLauncherDir();
		if ( _isApplicationBase ) {
			addFolder( theLauncherDir, theFolders );
		}
		if ( _isApplicationBaseConfig ) {
			addFolder( theLauncherDir, Folder.CONFIG, theFolders );
		}
		if ( _isApplicationBaseEtc ) {
			addFolder( theLauncherDir, Folder.ETC, theFolders );
		}
		if ( _isApplicationBaseSettings ) {
			addFolder( theLauncherDir, Folder.SETTINGS, theFolders );
		}
		if ( _isApplicationBaseDotConfig ) {
			addFolder( theLauncherDir, Folder.DOT_CONFIG, theFolders );
		}
		if ( _isApplicationBaseDotEtc ) {
			addFolder( theLauncherDir, Folder.DOT_ETC, theFolders );
		}
		if ( _isApplicationBaseDotSettings ) {
			addFolder( theLauncherDir, Folder.DOT_SETTINGS, theFolders );
		}
		if ( _isApplicationParentConfig ) {
			addParentFolder( theLauncherDir, Folder.CONFIG, theFolders );
		}
		if ( _isApplicationParentEtc ) {
			addParentFolder( theLauncherDir, Folder.ETC, theFolders );
		}
		if ( _isApplicationParentSettings ) {
			addParentFolder( theLauncherDir, Folder.SETTINGS, theFolders );
		}
		if ( _isApplicationParentDotConfig ) {
			addParentFolder( theLauncherDir, Folder.DOT_CONFIG, theFolders );
		}
		if ( _isApplicationParentDotEtc ) {
			addParentFolder( theLauncherDir, Folder.DOT_ETC, theFolders );
		}
		if ( _isApplicationParentDotSettings ) {
			addParentFolder( theLauncherDir, Folder.DOT_SETTINGS, theFolders );
		}
		// |--> For Maven: Folder ends with "target"? Try "classes"
		final File theBuildFolder = theLauncherDir;
		if ( theBuildFolder != null ) {
			if ( theBuildFolder.getAbsolutePath().endsWith( Folder.TARGET.getName() ) ) {
				addFolder( new File( theBuildFolder, Folder.CLASSES.getName() ), theFolders );
				addFolder( new File( theBuildFolder, Folder.TEST_CLASSES.getName() ), theFolders );
			}
			else {
				addFolder( new File( new File( theBuildFolder, Folder.TARGET.getName() ), Folder.CLASSES.getName() ), theFolders );
				addFolder( new File( new File( theBuildFolder, Folder.TARGET.getName() ), Folder.TEST_CLASSES.getName() ), theFolders );
			}
		}
		// <--| For Maven: Folder ends with "target"? Try "classes"
		if ( _isUserHome ) {
			addUserHomeFolder( theFolders );
		}
		if ( _isUserHomeConfig ) {
			addUserHomeFolder( Folder.CONFIG, theFolders );
		}
		if ( _isUserHomeEtc ) {
			addUserHomeFolder( Folder.ETC, theFolders );
		}
		if ( _isUserHomeSettings ) {
			addUserHomeFolder( Folder.SETTINGS, theFolders );
		}
		if ( _isUserHomeDotConfig ) {
			addUserHomeFolder( Folder.DOT_CONFIG, theFolders );
		}
		if ( _isUserHomeDotEtc ) {
			addUserHomeFolder( Folder.DOT_ETC, theFolders );
		}
		if ( _isUserHomeDotSettings ) {
			addUserHomeFolder( Folder.DOT_SETTINGS, theFolders );
		}
		if ( _isHostEtc ) {
			addFolder( new File( "/" + Folder.ETC.getName() ), theFolders );
		}

		return theFolders.toArray( new File[theFolders.size()] );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static File toFile( String aFileName, File aFolder ) {
		if ( aFolder != null && aFolder.isDirectory() && aFolder.canRead() ) {
			final File theFile = new File( aFolder, aFileName );
			if ( theFile.exists() && theFile.isFile() && theFile.canRead() ) {
				return theFile;
			}
		}
		return null;
	}

	private static File toFolder( String aFolderPath ) {
		if ( aFolderPath != null ) {
			final File theFolder = new File( aFolderPath );
			if ( theFolder.exists() && theFolder.isDirectory() && theFolder.canRead() ) {
				return theFolder;
			}
		}
		return null;
	}

	/**
	 * Determines the user's home folder.
	 * 
	 * @return The user's home folder or null if none was detected.
	 */
	static File toHomeFolder() {
		final String theValue = SystemProperty.USER_HOME.getValue();
		if ( theValue != null && theValue.length() != 0 ) {
			final File theFolder = new File( theValue );
			if ( theFolder.exists() && theFolder.isDirectory() && theFolder.canRead() ) {
				return theFolder;
			}
		}
		return null;
	}

	private void addUserHomeFolder( List<File> aFolders ) {
		addFolder( toHomeFolder(), aFolders );
	}

	private void addUserHomeFolder( Folder aChildFolder, List<File> aFolders ) {
		addFolder( toHomeFolder(), aChildFolder, aFolders );
	}

	private void addFolder( File aFolder, List<File> aFolders ) {
		if ( aFolder != null && aFolder.exists() && aFolder.isDirectory() && aFolder.canRead() ) {
			aFolders.add( aFolder );
		}
	}

	private void addFolder( File aBaseFolder, Folder aChildFolder, List<File> aFolders ) {
		if ( aBaseFolder != null && aBaseFolder.exists() && aBaseFolder.isDirectory() && aBaseFolder.canRead() ) {
			aBaseFolder = new File( aBaseFolder, aChildFolder.getName() );
			addFolder( aBaseFolder, aFolders );
		}
	}

	private void addParentFolder( File aBaseFolder, Folder aChildFolder, List<File> aFolders ) {
		if ( aBaseFolder != null && aBaseFolder.exists() && aBaseFolder.isDirectory() && aBaseFolder.canRead() ) {
			File theParentFolder = aBaseFolder.getParentFile();
			if ( theParentFolder != null && theParentFolder.exists() && theParentFolder.isDirectory() && theParentFolder.canRead() ) {
				theParentFolder = new File( theParentFolder, aChildFolder.getName() );
				addFolder( theParentFolder, aFolders );
			}
		}
	}
}
