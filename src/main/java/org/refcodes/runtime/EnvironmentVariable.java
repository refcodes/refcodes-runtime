// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.runtime;

import java.util.Map;

import org.refcodes.mixin.KeyAccessor;
import org.refcodes.mixin.ValueAccessor.ValueProperty;
import org.refcodes.struct.PathMap;
import org.refcodes.struct.StructureUtility;

/**
 * {@link EnvironmentVariable} for retrieving some common environment variables
 * with ease.
 */
public enum EnvironmentVariable implements KeyAccessor<String>, ValueProperty<String> {

	/**
	 * The virtual and the physical console width height and environment
	 * variable. E.g. ANSICON=80x19999 (80x25)
	 */
	ANSICON("ANSICON"),

	/**
	 * The environment variable (on windows) holding the computer's name.
	 */
	COMPUTERNAME("COMPUTERNAME"),

	/**
	 * Set to "true" or "false", forces ANSI to be used / not used by
	 * REFCODES.ORG artifacts, no matter what capabilities were detected for the
	 * hosting terminal.
	 */
	CONSOLE_ANSI("CONSOLE_ANSI"),

	/**
	 * ConEmu's environment variable regarding ANSI support.
	 */
	CONSOLE_CONEMU_ANSI("ConEmuANSI"),

	/**
	 * Stands for the lines per column to be taken by REFCODES.ORG artifacts.
	 */
	CONSOLE_HEIGHT("CONSOLE_HEIGHT"),

	/**
	 * The console's line-break property. Used to override any default line
	 * breaks for the REFCODES-ORG artifacts.
	 */
	CONSOLE_LINE_BREAK("CONSOLE_LINE_BREAK"),

	/**
	 * Stands for the chars per row to be taken by REFCODES.ORG artifacts.
	 */
	CONSOLE_WIDTH("CONSOLE_WIDTH"),

	/**
	 * Environment variable "HOST_SEED" for the Host-Seed for host-related IDs.
	 */
	HOST_SEED("HOST_SEED"),

	/**
	 * The environment variable (on windows) holding the computer's name.
	 */
	HOSTNAME("HOSTNAME"),

	/**
	 * HTTP-Proxy setting in URL notation ("http://my.company.org:3128")
	 */
	HTTP_PROXY("HTTP_PROXY"),

	/**
	 * HTTPS-Proxy setting in URL notation ("http://my.company.org:3128")
	 */
	HTTPS_PROXY("HTTPS_PROXY"),

	/**
	 * LANG is the normal environment variable for specifying a locale.
	 */
	LANG("LANG"),

	/**
	 * Stands for the chosen layout for the REFCODES.ORG logger artifacts.
	 */
	LOGGER_LAYOUT("LOGGER_LAYOUT"),

	/**
	 * Stands for the chosen style for the REFCODES.ORG logger artifacts.
	 */
	LOGGER_STYLE("LOGGER_STYLE"),

	/**
	 * No-Proxy settings for the {@link #HTTP_PROXY} ({@link #HTTPS_PROXY})
	 * settings in a comma separated list
	 * ("localhost,127.0.0.0/8,127.0.1.1,127.0.1.1*,...")
	 */
	NO_PROXY("NO_PROXY"),

	/**
	 * Number of processors.
	 */
	NUMBER_OF_PROCESSORS("NUMBER_OF_PROCESSORS"),

	/**
	 * Processor architecture on some systems (e.g. "AMD64").
	 */
	PROCESSOR_ARCHITECTURE("PROCESSOR_ARCHITECTURE"),

	/**
	 * Processor architecture on some systems (e.g. "AMD64").
	 */
	PROCESSOR_ARCHITEW6432("PROCESSOR_ARCHITEW6432"),

	/**
	 * When set to "true" then {@link Map} instances in data-structures are
	 * preserved instead of converting them to arrays if possible by indirectly
	 * calling {@link StructureUtility#toDataStructure(PathMap, String)} when
	 * invoking {@link PathMap#toDataStructure()}.
	 */
	STRUCT_KEEP_MAPS(StructureUtility.ENVIRONMENT_VARIABLE_STRUCT_KEEP_MAPS), // ~ "STRUCT_KEEPMAPS"

	/**
	 * Environment variable holding the current session's terminal.
	 */
	TERM("TERM"),

	/**
	 * The console width environment variable on *ix like shells.
	 */
	TERMINAL_COLUMNS("COLUMNS"),

	/**
	 * Environment variable holding the current session's terminal encoding.
	 */
	TERMINAL_ENCODING("terminalEncoding"),

	/**
	 * The console height environment variable.
	 */
	TERMINAL_LINES("LINES"),

	/**
	 * TMPDIR is the canonical environment variable in Unix and POSIX that
	 * should be used to specify a temporary directory for scratch space.
	 */
	TMPDIR("TMPDIR"),

	/**
	 * Environment variable holding the user's current working directory path.
	 */
	USER_DIR("PWD"),

	/**
	 * Environment variable holding the user's home folder path.
	 */
	USER_HOME("HOME"),

	/**
	 * Environment variable holding the user's current working directory path.
	 */
	USER_NAME("USER");

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _envVariableName;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new "environment variable".
	 *
	 * @param aEnvVariableName the environment variable's name
	 */
	private EnvironmentVariable( String aEnvVariableName ) {
		_envVariableName = aEnvVariableName;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getKey() {
		return _envVariableName;
	}

	/**
	 * Retrieves the value, first the upper-case variant of the environment
	 * variable is tried out. If not set or empty, then the lower-case variant
	 * is used.
	 * 
	 * @return The value of the environment variable.
	 */
	@Override
	public String getValue() {
		String theValue = System.getenv( _envVariableName.toUpperCase() );
		if ( theValue == null || theValue.isEmpty() ) {
			theValue = System.getenv( _envVariableName.toLowerCase() );
			if ( theValue == null || theValue.isEmpty() ) {
				theValue = System.getenv( _envVariableName );
			}
		}
		return theValue;
	}

	/**
	 * Sets the value, first the upper-case variant of the environment variable
	 * is tried out. If not set or empty, then the lower-case variant is tried.
	 *
	 * @param aValue the new value
	 */
	@Override
	public void setValue( String aValue ) {
		String theValue = System.getenv( _envVariableName.toUpperCase() );
		if ( theValue == null || theValue.isEmpty() ) {
			theValue = System.getenv( _envVariableName.toLowerCase() );
			if ( theValue != null && theValue.length() != 0 ) {
				System.getenv().put( _envVariableName.toLowerCase(), aValue );
				return;
			}
			else {
				theValue = System.getenv( _envVariableName );
				if ( theValue != null && theValue.length() != 0 ) {
					System.getenv().put( _envVariableName, aValue );
					return;
				}
			}
		}
		System.getenv().put( _envVariableName.toUpperCase(), aValue );
	}
}