// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.runtime;

import org.refcodes.mixin.ValueAccessor;

/**
 * Defines the values of the {@link SystemProperty#NATIVE_IMAGE_CODE} system
 * property.
 */
public enum NativeImageCode implements ValueAccessor<String> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * If the property {@link SystemProperty#NATIVE_IMAGE_CODE} returns
	 * {@link #RUNTIME} the code is executing in the context of image building
	 * (e.g. in a static initializer of a class that will be contained in the
	 * image).
	 */
	RUNTIME("runtime"),

	/**
	 * If the property {@link SystemProperty#NATIVE_IMAGE_CODE} returns
	 * {@link #BUILDTIME} the code is executing at image runtime.
	 */
	BUILDTIME("buildtime");

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _value;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private NativeImageCode( String aValue ) {
		_value = aValue;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getValue() {
		return _value;
	}

	/**
	 * Determines whether we are inside an executing native image, e.g. the
	 * {@link SystemProperty#NATIVE_IMAGE_CODE}'s value evaluates to
	 * {@link #RUNTIME}'s value.
	 * 
	 * @return True in case we are inside an executing native image.
	 */
	public static boolean isRuntime() {
		return RUNTIME.getValue().equalsIgnoreCase( SystemProperty.NATIVE_IMAGE_CODE.getValue() );
	}

	/**
	 * Determines whether we are invoked upon buildtime (???), e.g. the
	 * {@link SystemProperty#NATIVE_IMAGE_CODE}'s value evaluates to
	 * {@link #BUILDTIME}'s value.
	 * 
	 * @return True in case we are invoked upon buildtime (???).
	 */
	public static boolean isBuildtime() {
		return BUILDTIME.getValue().equalsIgnoreCase( SystemProperty.NATIVE_IMAGE_CODE.getValue() );
	}

}
