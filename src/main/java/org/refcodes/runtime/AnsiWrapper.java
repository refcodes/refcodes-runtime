// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.runtime;

import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.refcodes.exception.BugException;

/**
 * Wraps required Jansi related functionality to separate it from any other
 * classes to handle {@link ClassNotFoundException} exceptions (and the like)
 * when excluding Jansi from this artifact.
 */
class AnsiWrapper {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	// private static final Logger LOGGER = Logger.getLogger(
	// AnsiWrapper.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String ANSI_CONSOLE_CLASS_NAME = "org.fusesource.jansi.AnsiConsole";
	private static final String OUT_METHOD = "out";
	private static final String ERR_METHOD = "err";
	private static final String SYSTEM_INSTALL_METHOD = "systemInstall";
	private static final String SYSTEM_UNINSTALL_METHOD = "systemUninstall";
	private static final String GET_TERMINAL_WIDTH_METHOD = "getTerminalWidth";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private static boolean _hasOutMethodFailGuard = false;
	private static boolean _hasErrMethodFailGuard = false;
	private static boolean _hasSystemInstallMethodFailGuard = false;
	private static boolean _hasSystemUninstallMethodFailGuard = false;
	private static boolean _hasGetTerminalWidthMethodFailGuard = false;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// INJECTION:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * A {@link ClassNotFoundException} safe delegate to
	 * {@link org.fusesource.jansi.AnsiConsole#out()}.
	 * 
	 * @return The according {@link PrintStream} or {@link System#out} if
	 *         invocation failed.
	 */
	public static PrintStream toAnsiOut() {
		// return AnsiConsole.out();
		if ( !_hasOutMethodFailGuard ) { // Prevent calling native methods over and over again in case of failure!
			try {
				final Class<?> theClass = Class.forName( ANSI_CONSOLE_CLASS_NAME );
				final Method theMethod = theClass.getMethod( OUT_METHOD );
				return (PrintStream) theMethod.invoke( null );
			}
			catch ( ClassNotFoundException e ) {
				_hasOutMethodFailGuard = true;
				// LOGGER.log( Level.INFO, "Class <" + ANSI_CONSOLE_CLASS_NAME + "> found, skipping usage of Jansi!" );
			}
			catch ( SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e ) {
				_hasOutMethodFailGuard = true;
				// LOGGER.log( Level.WARNING, "Cannot invoke static method <" + OUT_METHOD + "()> of class <" + ANSI_CONSOLE_CLASS_NAME + ">!", e );
			}
			catch ( NoSuchMethodException e ) {
				_hasOutMethodFailGuard = true;
				throw new BugException( "There is no (more) method <" + OUT_METHOD + "()> for class <" + ANSI_CONSOLE_CLASS_NAME + ">, please fix the code!", e );
			}
			catch ( Throwable e ) {
				_hasOutMethodFailGuard = true;
				// LOGGER.log( Level.WARNING, "Cannot invoke static method <" + OUT_METHOD + "()> of class <" + ANSI_CONSOLE_CLASS_NAME + ">!", e );
			}
		}
		return System.out;
	}

	/**
	 * A {@link ClassNotFoundException} safe delegate to
	 * {@link org.fusesource.jansi.AnsiConsole#err()}.
	 * 
	 * @return The according {@link PrintStream} or {@link System#err} if
	 *         invocation failed.
	 */
	public static PrintStream toAnsiErr() {
		// return AnsiConsole.err();
		if ( !_hasErrMethodFailGuard ) { // Prevent calling native methods over and over again in case of failure!
			try {
				final Class<?> theClass = Class.forName( ANSI_CONSOLE_CLASS_NAME );
				final Method theMethod = theClass.getMethod( ERR_METHOD );
				return (PrintStream) theMethod.invoke( null );
			}
			catch ( ClassNotFoundException e ) {
				_hasErrMethodFailGuard = true;
				// LOGGER.log( Level.INFO, "Class <" + ANSI_CONSOLE_CLASS_NAME + "> found, skipping usage of Jansi!" );
			}
			catch ( SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e ) {
				_hasErrMethodFailGuard = true;
				// LOGGER.log( Level.WARNING, "Cannot invoke static method <" + ERR_METHOD + "()> of class <" + ANSI_CONSOLE_CLASS_NAME + ">!", e );
			}
			catch ( NoSuchMethodException e ) {
				_hasErrMethodFailGuard = true;
				throw new BugException( "There is no (more) method <" + ERR_METHOD + "()> for class <" + ANSI_CONSOLE_CLASS_NAME + ">, please fix the code!", e );
			}
			catch ( Throwable e ) {
				_hasErrMethodFailGuard = true;
				// LOGGER.log( Level.WARNING, "Cannot invoke static method <" + ERR_METHOD + "()> of class <" + ANSI_CONSOLE_CLASS_NAME + ">!", e );
			}
		}
		return System.err;
	}

	/**
	 * A {@link ClassNotFoundException} safe delegate to
	 * {@link org.fusesource.jansi.AnsiConsole#systemInstall()}.
	 * 
	 * @return True if invocation succeeded.
	 */
	public static boolean systemInstall() {
		// AnsiConsole.systemInstall();
		if ( !_hasSystemInstallMethodFailGuard ) { // Prevent calling native methods over and over again in case of failure!
			try {
				final Class<?> theClass = Class.forName( ANSI_CONSOLE_CLASS_NAME );
				final Method theMethod = theClass.getMethod( SYSTEM_INSTALL_METHOD );
				theMethod.invoke( null );
				return true;
			}
			catch ( ClassNotFoundException e ) {
				_hasSystemInstallMethodFailGuard = true;
				// LOGGER.log( Level.INFO, "Class <" + ANSI_CONSOLE_CLASS_NAME + "> found, skipping usage of Jansi!" );
			}
			catch ( SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e ) {
				_hasSystemInstallMethodFailGuard = true;
				// LOGGER.log( Level.WARNING, "Cannot invoke static method <" + SYSTEM_INSTALL_METHOD + "()> of class <" + ANSI_CONSOLE_CLASS_NAME + ">!", e );
			}
			catch ( NoSuchMethodException e ) {
				_hasSystemInstallMethodFailGuard = true;
				throw new BugException( "There is no (more) method <" + SYSTEM_INSTALL_METHOD + "()> for class <" + ANSI_CONSOLE_CLASS_NAME + ">, please fix the code!", e );
			}
			catch ( Throwable e ) {
				_hasSystemInstallMethodFailGuard = true;
				// LOGGER.log( Level.WARNING, "Cannot invoke static method <" + SYSTEM_INSTALL_METHOD + "()> of class <" + ANSI_CONSOLE_CLASS_NAME + ">!", e );
			}
		}
		return false;
	}

	/**
	 * A {@link ClassNotFoundException} safe delegate to
	 * {@link org.fusesource.jansi.AnsiConsole#systemUninstall()}.
	 * 
	 * @return True if invocation succeeded.
	 */
	public static boolean systemUninstall() {
		// AnsiConsole.systemUninstall();
		if ( !_hasSystemUninstallMethodFailGuard ) { // Prevent calling native methods over and over again in case of failure!
			try {
				final Class<?> theClass = Class.forName( ANSI_CONSOLE_CLASS_NAME );
				final Method theMethod = theClass.getMethod( SYSTEM_UNINSTALL_METHOD );
				theMethod.invoke( null );
				return true;
			}
			catch ( ClassNotFoundException e ) {
				_hasSystemUninstallMethodFailGuard = true;
				// LOGGER.log( Level.INFO, "Class <" + ANSI_CONSOLE_CLASS_NAME + "> found, skipping usage of Jansi!" );
			}
			catch ( SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e ) {
				_hasSystemUninstallMethodFailGuard = true;
				// LOGGER.log( Level.WARNING, "Cannot invoke static method <" + SYSTEM_UNINSTALL_METHOD + "()> of class <" + ANSI_CONSOLE_CLASS_NAME + ">!", e );
			}
			catch ( NoSuchMethodException e ) {
				_hasSystemUninstallMethodFailGuard = true;
				throw new BugException( "There is no (more) method <" + SYSTEM_UNINSTALL_METHOD + "()> for class <" + ANSI_CONSOLE_CLASS_NAME + ">, please fix the code!", e );
			}
			catch ( Throwable e ) {
				_hasSystemUninstallMethodFailGuard = true;
				// LOGGER.log( Level.WARNING, "Cannot invoke static method <" + SYSTEM_UNINSTALL_METHOD + "()> of class <" + ANSI_CONSOLE_CLASS_NAME + ">!", e );
			}
		}
		return false;
	}

	/**
	 * A {@link ClassNotFoundException} safe delegate to
	 * {@link org.fusesource.jansi.AnsiConsole#getTerminalWidth()}.
	 * 
	 * @return The according terminal's width or -1 if invocation failed.
	 */
	public static int getTerminalWidth() {
		// return AnsiConsole.getTerminalWidth();
		if ( !_hasGetTerminalWidthMethodFailGuard ) { // Prevent calling native methods over and over again in case of failure!
			try {
				final Class<?> theClass = Class.forName( ANSI_CONSOLE_CLASS_NAME );
				final Method theMethod = theClass.getMethod( GET_TERMINAL_WIDTH_METHOD );
				return (Integer) theMethod.invoke( null );
			}
			catch ( ClassNotFoundException e ) {
				_hasGetTerminalWidthMethodFailGuard = true;
				// LOGGER.log( Level.INFO, "Class <" + ANSI_CONSOLE_CLASS_NAME + "> found, skipping usage of Jansi!" );
			}
			catch ( SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e ) {
				_hasGetTerminalWidthMethodFailGuard = true;
				// LOGGER.log( Level.WARNING, "Cannot invoke static method <" + GET_TERMINAL_WIDTH_METHOD + "()> of class <" + ANSI_CONSOLE_CLASS_NAME + ">!", e );
			}
			catch ( NoSuchMethodException e ) {
				_hasGetTerminalWidthMethodFailGuard = true;
				throw new BugException( "There is no (more) method <" + GET_TERMINAL_WIDTH_METHOD + "()> for class <" + ANSI_CONSOLE_CLASS_NAME + ">, please fix the code!", e );
			}
			catch ( Throwable e ) {
				_hasGetTerminalWidthMethodFailGuard = true;
				// LOGGER.log( Level.WARNING, "Cannot invoke static method <" + GET_TERMINAL_WIDTH_METHOD + "()> of class <" + ANSI_CONSOLE_CLASS_NAME + ">!", e );
			}
		}
		return -1;
	}

	/**
	 * Checks for failure calling the {@link org.fusesource.jansi.AnsiConsole}
	 * methods using reflection.
	 *
	 * @return True in case calling any of the
	 *         {@link org.fusesource.jansi.AnsiConsole} methods failed.
	 */
	public static boolean hasFailure() {
		return _hasErrMethodFailGuard || _hasGetTerminalWidthMethodFailGuard || _hasOutMethodFailGuard || _hasSystemInstallMethodFailGuard || _hasSystemUninstallMethodFailGuard;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

}
