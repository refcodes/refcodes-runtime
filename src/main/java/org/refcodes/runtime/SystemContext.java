// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.runtime;

import java.io.IOException;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Arrays;

import org.refcodes.data.Delimiter;
import org.refcodes.data.Text;
import org.refcodes.numerical.NumericalUtility;

/**
 * Describes the context within a system's environment regarding host, user and
 * application contexts as well as the currently invoked Java runtime session.
 */
public enum SystemContext {

	/**
	 * Specifies an application specific granularity.
	 */
	APPLICATION(false, false, true, false),

	/**
	 * Specifies a user specific granularity.
	 */
	USER_APPLICATION(false, true, true, false),

	/**
	 * Specifies an application for a user on a host specific granularity.
	 */
	HOST_USER_APPLICATION(true, true, true, false),

	/**
	 * Specifies an application on a host specific granularity.
	 */
	HOST_APPLICATION(true, false, true, false),

	/**
	 * Specifies a user specific granularity.
	 */
	USER(false, true, false, false),

	/**
	 * Specifies a user on a host specific granularity.
	 */
	HOST_USER(true, true, false, false),

	/**
	 * Specifies a host specific granularity.
	 */
	HOST(true, false, false, false),

	/**
	 * Specifies a Java runtime invokcation's session specific granularity,
	 * valid just for the current invocation of the Java runtime.
	 */
	SESSION(false, false, false, true),

	/**
	 * Specifies a Java runtime invokcation's session and an application
	 * specific granularity.
	 */
	APPLICATION_SESSION(false, false, true, true),

	/**
	 * Specifies a Java runtime invokcation's session and a user specific
	 * granularity.
	 */
	USER_APPLICATION_SESSION(false, true, true, true),

	/**
	 * Specifies a Java runtime invokcation's session for an application for a
	 * user on a host specific granularity.
	 */
	HOST_USER_APPLICATION_SESSION(true, true, true, true),

	/**
	 * Specifies a Java runtime invokcation's session for an application on a
	 * host specific granularity.
	 */
	HOST_APPLICATION_SESSION(true, false, true, true),

	/**
	 * Specifies a Java runtime invokcation's session for a user specific
	 * granularity.
	 */
	USER_SESSION(false, true, false, true),

	/**
	 * Specifies a Java runtime invokcation's session for a user on a host
	 * specific granularity.
	 */
	HOST_USER_SESSION(true, true, false, true),

	/**
	 * Specifies a Java runtime invokcation's session for a host specific
	 * granularity.
	 */
	HOST_SESSION(true, false, false, true);

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private static String _hostAddress = null;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private static volatile long SESSION_SEED = System.currentTimeMillis();
	private boolean _isHostLevel;
	private boolean _isUserLevel;
	private boolean _isApplicationLevel;
	private boolean _isSessionLevel;
	private String _ctxSequence = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private SystemContext( boolean isHostLevel, boolean isUserLevel, boolean isApplicationLevel, boolean isSessionLevel ) {
		_isHostLevel = isHostLevel;
		_isUserLevel = isUserLevel;
		_isApplicationLevel = isApplicationLevel;
		_isSessionLevel = isSessionLevel;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Determines whether the granularity is on a host level.
	 * 
	 * @return True in case of being on a host level, else false.
	 */
	public boolean isHostLevel() {
		return _isHostLevel;
	}

	/**
	 * Determines whether the granularity is on a user level.
	 * 
	 * @return True in case of being on a user level, else false.
	 */
	public boolean isUserLevel() {
		return _isUserLevel;
	}

	/**
	 * Determines whether the granularity is on an application level.
	 * 
	 * @return True in case of being on an application level, else false.
	 */
	public boolean isApplicationLevel() {
		return _isApplicationLevel;
	}

	/**
	 * Determines whether the granularity is on a session level, e.g. valid just
	 * for the lifetime of the current runtime Java's invocation.
	 * 
	 * @return True in case of being on a session level, else false.
	 */
	public boolean isSessionLevel() {
		return _isSessionLevel;
	}

	/**
	 * Creates a {@link String} most probably unique as specified by the
	 * enumeration instance this method is invoked on. Attention: The result my
	 * vary depending on the user under which the application is being executed!
	 * Supports the environment variable {@link EnvironmentVariable#HOST_SEED}
	 * as well as the system property {@link SystemProperty#HOST_SEED}. The
	 * enumeration value specifies the context for the created TID, e.g. which
	 * system properties to take into account.
	 * 
	 * @return The calculated system's {@link String}.
	 */
	public String toContextString() {
		return toContextBuffer( null ).toString();
	}

	/**
	 * Creates a {@link String} most probably unique as specified by the
	 * enumeration instance this method is invoked on. Attention: The result my
	 * vary depending on the user under which the application is being executed!
	 * Supports the environment variable {@link EnvironmentVariable#HOST_SEED}
	 * as well as the system property {@link SystemProperty#HOST_SEED}. The
	 * enumeration value specifies the context for the created TID, e.g. which
	 * system properties to take into account.
	 * 
	 * @param aSeed A seed to alter the TID individually.
	 * 
	 * @return The calculated system's {@link String}.
	 */
	public String toContextString( String aSeed ) {
		return toContextBuffer( aSeed != null ? aSeed.toCharArray() : ( null ) ).toString();
	}

	/**
	 * Creates a {@link String} most probably unique as specified by the
	 * enumeration instance this method is invoked on. Attention: The result my
	 * vary depending on the user under which the application is being executed!
	 * Supports the environment variable {@link EnvironmentVariable#HOST_SEED}
	 * as well as the system property {@link SystemProperty#HOST_SEED}. The
	 * enumeration value specifies the context for the created TID, e.g. which
	 * system properties to take into account.
	 * 
	 * @param aSeed A seed to alter the TID individually.
	 * 
	 * @return The calculated system's {@link String}.
	 */
	public String toContextString( char[] aSeed ) {
		return toContextBuffer( aSeed ).toString();
	}

	/**
	 * Creates a {@link String} most probably unique as specified by the
	 * enumeration instance this method is invoked on. Attention: The result my
	 * vary depending on the user under which the application is being executed!
	 * Supports the environment variable {@link EnvironmentVariable#HOST_SEED}
	 * as well as the system property {@link SystemProperty#HOST_SEED}. The
	 * enumeration value specifies the context for the created TID, e.g. which
	 * system properties to take into account.
	 * 
	 * @return The calculated system's {@link String}.
	 */
	public char[] toContextSequence() {
		return toContextSequence( (char[]) null );
	}

	/**
	 * Creates a {@link String} most probably unique as specified by the
	 * enumeration instance this method is invoked on. Attention: The result my
	 * vary depending on the user under which the application is being executed!
	 * Supports the environment variable {@link EnvironmentVariable#HOST_SEED}
	 * as well as the system property {@link SystemProperty#HOST_SEED}. The
	 * enumeration value specifies the context for the created TID, e.g. which
	 * system properties to take into account.
	 * 
	 * @param aSeed A seed to alter the TID individually.
	 * 
	 * @return The calculated system's {@link String}.
	 */
	public char[] toContextSequence( String aSeed ) {
		return toContextSequence( aSeed != null ? aSeed.toCharArray() : (char[]) null );
	}

	/**
	 * Creates a {@link String} most probably unique as specified by the
	 * enumeration instance this method is invoked on. Attention: The result my
	 * vary depending on the user under which the application is being executed!
	 * Supports the environment variable {@link EnvironmentVariable#HOST_SEED}
	 * as well as the system property {@link SystemProperty#HOST_SEED}. The
	 * enumeration value specifies the context for the created TID, e.g. which
	 * system properties to take into account.
	 * 
	 * @param aSeed A seed to alter the TID individually.
	 * 
	 * @return The calculated system's {@link String}.
	 */
	public char[] toContextSequence( char[] aSeed ) {
		final StringBuilder theBuffer = toContextBuffer( aSeed );
		final int length = theBuffer.length();
		final char[] theSequence = new char[length];
		theBuffer.getChars( 0, length, theSequence, 0 );
		return theSequence;
	}

	/**
	 * Creates an TID most probably unique as specified by the enumeration
	 * instance this method is invoked on. Succeeding calls of this method on
	 * the same machine returns the same value. Attention: The result my vary
	 * depending on the user under which the application is being executed!
	 * Supports the environment variable {@link EnvironmentVariable#HOST_SEED}
	 * as well as the system property {@link SystemProperty#HOST_SEED}. The
	 * enumeration value specifies the context for the created TID, e.g. which
	 * system properties to take into account.
	 * 
	 * @return The calculated system's TID.
	 */
	public int toContextId() {
		return toContextId( null );
	}

	/**
	 * Creates an TID most probably unique as specified by the enumeration
	 * instance this method is invoked on. Succeeding calls of this method on
	 * the same machine with the same arguments returns the same value.
	 * Attention: The result my vary depending on the user under which the
	 * application is being executed! Supports the environment variable
	 * {@link EnvironmentVariable#HOST_SEED} as well as the system property
	 * {@link SystemProperty#HOST_SEED}.
	 * System.out.println(System.getenv("PROCESSOR_ARCHITECTURE"));
	 * System.out.println(System.getenv("PROCESSOR_ARCHITEW6432"));
	 * System.getProperty("os.arch"); The enumeration value specifies the
	 * context for the created TID, e.g. which system properties to take into
	 * account.
	 * 
	 * @param aSeed A seed to alter the TID individually.
	 * 
	 * @return The calculated system's TID.
	 */
	public int toContextId( String aSeed ) {
		final String theSystemString = toContextString( aSeed );
		return NumericalUtility.toHashCode( theSystemString );
	}

	/**
	 * Creates an array of IDs most probably unique as specified by the provided
	 * argument this method is invoked on. Succeeding calls of this method on
	 * the same machine with the same arguments returns the same values.
	 * Attention: The result my vary depending on the user under which the
	 * application is being executed! Supports the environment variable
	 * {@link EnvironmentVariable#HOST_SEED} as well as the system property
	 * {@link SystemProperty#HOST_SEED}. The enumeration value specifies the
	 * context for the created TID, e.g. which system properties to take into
	 * account.
	 * 
	 * @param aIdLength The number of IDs to be generated.
	 * 
	 * @return The calculated system's TID.
	 */
	public int[] toContextIds( int aIdLength ) {
		return toContextIds( aIdLength, null );
	}

	/**
	 * Creates an array of IDs most probably unique as specified by the provided
	 * argument this method is invoked on. Succeeding calls of this method on
	 * the same machine with the same arguments returns the same values.
	 * Attention: The result my vary depending on the user under which the
	 * application is being executed! Supports the environment variable
	 * {@link EnvironmentVariable#HOST_SEED} as well as the system property
	 * {@link SystemProperty#HOST_SEED}. The enumeration value specifies the
	 * context for the created TID, e.g. which system properties to take into
	 * account.
	 * 
	 * @param aIdLength The number of IDs to be generated.
	 * @param aSeed A seed to alter the TID individually.
	 * 
	 * @return The calculated system's TID.
	 */
	public int[] toContextIds( int aIdLength, String aSeed ) {
		final String theSystemString = toContextString( aSeed );
		return NumericalUtility.toHashCodes( theSystemString, aIdLength );
	}

	/**
	 * Obfuscates (applies a simple invertible XOR function on) the given text
	 * with the {@link SystemContext} element's context {@link String} (as of
	 * {@link #toContextString()}). Applying this operation to the result of
	 * this operation results in the original text (invertible function).
	 * 
	 * @param aText The text to be obfuscated (inverted).
	 * 
	 * @return The obfuscated (inverted) text.
	 */
	public char[] toObfuscated( String aText ) {
		return toInvertible( aText.toCharArray() );
	}

	/**
	 * Obfuscates (applies a simple invertible XOR function on) the given text
	 * with the {@link SystemContext} element's context {@link String} (as of
	 * {@link #toContextString()}). Applying this operation to the result of
	 * this operation results in the original text (invertible function).
	 * 
	 * @param aText The text to be obfuscated (inverted).
	 * 
	 * @return The obfuscated (inverted) text.
	 */
	public char[] toInvertible( char[] aText ) {
		final byte[] theXorBytes = toContextString().getBytes();
		final char[] theObfuscated = new char[aText.length];
		for ( int i = 0; i < aText.length; i++ ) {
			theObfuscated[i] = (char) ( aText[i] ^ theXorBytes[i % theXorBytes.length] );
		}
		return theObfuscated;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private StringBuilder toContextBuffer( char[] aSeed ) {
		final StringBuilder theBuffer = new StringBuilder();
		if ( _ctxSequence != null ) {
			theBuffer.append( _ctxSequence );
		}
		else {
			if ( isSessionLevel() ) {
				// -------------------------------------------------------------
				// SESSION-SEED:
				// -------------------------------------------------------------
				if ( theBuffer.length() != 0 ) {
					theBuffer.append( Delimiter.LIST.getChar() );
				}
				// -------------------------------------------------------------
				// ENVIRONMENT VARIABLES:
				// -------------------------------------------------------------
				// We assume the environment variables to be immutable as as of Java
				// 9, "Field.setAccessible(true);" is of an illegal access and will
				// not be granted. This would be required in order to modify the
				// environment variables which the currently invoked Java runtime
				// has hold of.
				// -------------------------------------------------------------
				theBuffer.append( SESSION_SEED );
				for ( String eValue : System.getenv().keySet() ) {
					theBuffer.append( Delimiter.LIST.getChar() );
					theBuffer.append( System.getenv( eValue ) );
				}
			}

			if ( isHostLevel() ) {
				// -------------------------------------------------------------
				// OS-ARCH
				// -------------------------------------------------------------
				if ( theBuffer.length() != 0 ) {
					theBuffer.append( Delimiter.LIST.getChar() );
				}
				String theArch = SystemProperty.OS_ARCH.getValue();
				if ( theArch == null || theArch.isEmpty() ) {
					theArch = EnvironmentVariable.PROCESSOR_ARCHITECTURE.getKey();
					if ( theArch == null || theArch.isEmpty() ) {
						theArch = EnvironmentVariable.PROCESSOR_ARCHITEW6432.getKey();
						if ( theArch == null ) {
							theArch = Text.ARECIBO_MESSAGE.getText().substring( 32, 48 );
						}
					}
				}
				theBuffer.append( theArch );

				// MAC or IP (as them cost CPU cycles) |-->
				// -------------------------------------------------------------
				// HOST-ADDRESS
				// -------------------------------------------------------------
				if ( theBuffer.length() != 0 ) {
					theBuffer.append( Delimiter.LIST.getChar() );
				}
				if ( _hostAddress == null ) {
					try {
						_hostAddress = Arrays.toString( Host.toHostMacAddress() );
					}
					catch ( SocketException | UnknownHostException e ) {}
					if ( _hostAddress == null || _hostAddress.isEmpty() ) {
						try {
							_hostAddress = Arrays.toString( Host.toHostIpAddress() );
						}
						catch ( IOException ignore ) {}
						if ( _hostAddress == null || _hostAddress.isEmpty() ) {
							_hostAddress = Text.ARECIBO_MESSAGE.getText().substring( 64, 80 );
						}
					}
				}
				theBuffer.append( _hostAddress );
				// <--| MAC or IP (as them cost CPU cycles)

				// -------------------------------------------------------------
				// COMPUTER-FILE
				// -------------------------------------------------------------
				if ( theBuffer.length() != 0 ) {
					theBuffer.append( Delimiter.LIST.getChar() );
				}
				String theComputerName = Host.getComputerName();
				if ( theComputerName == null || theComputerName.isEmpty() ) {
					theComputerName = Text.ARECIBO_MESSAGE.getText().substring( 96, 112 );
				}
				theBuffer.append( theComputerName );

				// -------------------------------------------------------------
				// CORES
				// -------------------------------------------------------------
				if ( theBuffer.length() != 0 ) {
					theBuffer.append( Delimiter.LIST.getChar() );
				}
				String theCores;
				try {
					theCores = Integer.toString( java.lang.Runtime.getRuntime().availableProcessors() );
				}
				catch ( Exception ignore ) {
					theCores = EnvironmentVariable.NUMBER_OF_PROCESSORS.getValue();
				}
				if ( theCores == null || theCores.isEmpty() ) {
					theCores = Text.ARECIBO_MESSAGE.getText().substring( 112, 128 );
				}
				theBuffer.append( theCores );
			}

			if ( isUserLevel() ) {
				// -------------------------------------------------------------
				// USER_HOME-FILE
				// -------------------------------------------------------------
				if ( theBuffer.length() != 0 ) {
					theBuffer.append( Delimiter.LIST.getChar() );
				}
				String theUserName = SystemProperty.USER_NAME.getValue();
				if ( theUserName == null || theUserName.isEmpty() ) {
					theUserName = Text.ARECIBO_MESSAGE.getText().substring( 0, 16 );
				}
				theBuffer.append( theUserName );
				// -----------------------------------------------------------------
				// USER_HOME-HOME
				// -----------------------------------------------------------------
				if ( theBuffer.length() != 0 ) {
					theBuffer.append( Delimiter.LIST.getChar() );
				}
				String theUserHome = SystemProperty.USER_HOME.getValue();
				if ( theUserHome == null || theUserHome.isEmpty() ) {
					theUserHome = Text.ARECIBO_MESSAGE.getText().substring( 16, 32 );
				}
				theBuffer.append( theUserHome );

				// -------------------------------------------------------------
				// TEMP-DIR
				// -------------------------------------------------------------
				if ( theBuffer.length() != 0 ) {
					theBuffer.append( Delimiter.LIST.getChar() );
				}
				String theTemp = Host.getTempDir();
				if ( theTemp == null ) {
					theTemp = Text.ARECIBO_MESSAGE.getText().substring( 48, 64 );
				}
				theBuffer.append( theTemp );
			}

			if ( isApplicationLevel() ) {
				// -------------------------------------------------------------
				// MAIN
				// -------------------------------------------------------------
				if ( theBuffer.length() != 0 ) {
					theBuffer.append( Delimiter.LIST.getChar() );
				}
				final String theMainString;
				final Class<?> theMainClass = Execution.getMainClass();
				if ( theMainClass != null ) {
					theMainString = theMainClass.getName();
				}
				else {
					theMainString = Text.ARECIBO_MESSAGE.getText().substring( 128, 144 );
				}
				theBuffer.append( theMainString );
			}
			_ctxSequence = theBuffer.toString();
		}

		// ---------------------------------------------------------------------
		// SEED
		// ---------------------------------------------------------------------
		if ( aSeed != null && aSeed.length != 0 ) {
			if ( theBuffer.length() != 0 ) {
				theBuffer.append( Delimiter.LIST.getChar() );
			}
			theBuffer.append( aSeed );
		}
		String theSeed = EnvironmentVariable.HOST_SEED.getValue();
		if ( theSeed != null && theSeed.length() != 0 ) {
			if ( theBuffer.length() != 0 ) {
				theBuffer.append( Delimiter.LIST.getChar() );
			}
			theBuffer.append( theSeed );
		}
		theSeed = SystemProperty.HOST_SEED.getValue();
		if ( theSeed != null && theSeed.length() != 0 ) {
			if ( theBuffer.length() != 0 ) {
				theBuffer.append( Delimiter.LIST.getChar() );
			}
			theBuffer.append( theSeed );
		}
		// ---------------------------------------------------------------------
		return theBuffer;
	}
}
