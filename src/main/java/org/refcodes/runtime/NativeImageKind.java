// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.runtime;

import java.lang.reflect.Executable;

import org.refcodes.mixin.ValueAccessor;

/**
 * Defines the values of the {@link SystemProperty#NATIVE_IMAGE_KIND} system
 * property.
 */
public enum NativeImageKind implements ValueAccessor<String> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * If the property {@link SystemProperty#NATIVE_IMAGE_KIND} is
	 * {@link Executable} the image is built as an executable.
	 */
	EXECUTABLE("executable"),

	/**
	 * If the property{@link SystemProperty#NATIVE_IMAGE_KIND} is
	 * {@link #SHARED_LIBRARY} the image is built as a shared library.
	 */
	SHARED_LIBRARY("shared");

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _value;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private NativeImageKind( String aValue ) {
		_value = aValue;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getValue() {
		return _value;
	}

	/**
	 * Determines whether we have an executable native image, e.g. the
	 * {@link SystemProperty#NATIVE_IMAGE_KIND}'s value evaluates to
	 * {@link #EXECUTABLE}'s value.
	 * 
	 * @return True in case we have an executable native image.
	 */
	public static boolean isExecutable() {
		return EXECUTABLE.getValue().equalsIgnoreCase( SystemProperty.NATIVE_IMAGE_KIND.getValue() );
	}

	/**
	 * Determines whether we have a shared library, e.g. the
	 * {@link SystemProperty#NATIVE_IMAGE_KIND}'s value evaluates to
	 * {@link #SHARED_LIBRARY}'s value.
	 * 
	 * @return True in case we have a shared library
	 */
	public static boolean isSharedLibrary() {
		return SHARED_LIBRARY.getValue().equalsIgnoreCase( SystemProperty.NATIVE_IMAGE_KIND.getValue() );
	}
}
