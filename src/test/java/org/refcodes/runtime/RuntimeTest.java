// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.runtime;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.junit.jupiter.api.Test;
import org.refcodes.data.Delimiter;

public class RuntimeTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testAnnotations() {
		assertEquals( 5, Execution.annotations( MyTest.class ).size() );
		assertEquals( 6, Execution.annotations( YourTest.class ).size() );
	}

	@Test
	public void testFindAnnotation() {
		assertNotNull( Execution.findAnnotation( MyAnnotation.class, MyTest.class ) );
		assertNotNull( Execution.findAnnotation( MyAnnotation.class, YourTest.class ) );
		assertNull( Execution.findAnnotation( YourAnnotation.class, MyTest.class ) );
		assertNotNull( Execution.findAnnotation( YourAnnotation.class, YourTest.class ) );
	}

	@Test
	public void testIsUnderTest() {
		assertTrue( Execution.isUnderTest() );
	}

	@Test
	public void testGetStackTraceElement() {
		final StackTraceElement theStackTraceElement = Execution.getCallerStackTraceElement();
		final String theFullyQualifiedMethodName = Execution.toFullyQualifiedMethodName( theStackTraceElement );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theFullyQualifiedMethodName );
		}
		assertEquals( RuntimeTest.class.getName() + Delimiter.METHOD_NAME.getChar() + "testGetStackTraceElement", theFullyQualifiedMethodName );
	}

	@Test
	public void testToFullyQualifiedMethodName() {
		final StackTraceElement theStackTraceElement = Execution.getCallerStackTraceElement( RuntimeTest.class );
		final String theFullyQualifiedMethodName = Execution.toFullyQualifiedMethodName( theStackTraceElement );
		assertNotEquals( RuntimeTest.class.getName() + Delimiter.METHOD_NAME.getChar() + "testToFullyQualifiedMethodName", theFullyQualifiedMethodName );
	}

	@Test
	public void testToFullyQualifiedPackageName() {
		final String thePackageName = Execution.toFullyQualifiedPackageName();
		assertEquals( thePackageName, getClass().getPackage().getName() );
	}

	@Test
	public void testToClassName() {
		final String theClassName = Execution.toClassName();
		assertEquals( theClassName, getClass().getName().substring( getClass().getName().lastIndexOf( '.' ) + 1 ) );
		final String theFooBar = "FooBar";
		final String theFullyQualifiedClassName = "foo.bar." + theFooBar;
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theFullyQualifiedClassName + " --> toClassName() --> " + Execution.toClassName( theFullyQualifiedClassName ) );
		}
		assertEquals( theFooBar, Execution.toClassName( theFullyQualifiedClassName ) );
	}

	@Test
	public void testToInnerClassName() {
		final String theClassName = Execution.toClassName();
		assertEquals( theClassName, getClass().getName().substring( getClass().getName().lastIndexOf( '.' ) + 1 ) );
		final String theInnerFooBar = "InnerFooBar";
		final String theFullyQualifiedClassName = "foo.bar.FooBar$" + theInnerFooBar;
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theFullyQualifiedClassName + " --> toClassName() --> " + Execution.toClassName( theFullyQualifiedClassName ) );
		}
		assertEquals( theInnerFooBar, Execution.toClassName( theFullyQualifiedClassName ) );
	}

	@Test
	public void testGetPid() {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "PID = " + Host.getPid() );
		}
	}

	@Test
	public void testToApplicationBaseFile() throws IOException {
		final File theFile = Execution.toLauncherDir();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Application base file = " + theFile.toURI().toURL().toString() );
		}
	}

	@Test
	public void testGetMainClass() throws IOException {
		final Class<?> theMainClass = Execution.getMainClass();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Main class = " + theMainClass.getName() );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	@MyAnnotation
	public class MyTest {}

	@YourAnnotation
	public class YourTest {}

	@Target(ElementType.TYPE)
	@Retention(RetentionPolicy.RUNTIME)
	public @interface MyAnnotation {}

	@MyAnnotation
	@Target(ElementType.TYPE)
	@Retention(RetentionPolicy.RUNTIME)
	public @interface YourAnnotation {}
}
