// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.runtime;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.refcodes.data.Delimiter;

/**
 * The Class PropertyBuilderTest.
 */
public class PropertyBuilderTest {
	// @formatter:off
	private static final String[] KEYS = new String[]{
		"my___System--Property__",
		"_My-System-Property",
		"mySystemProperty__",
		"mySystemProperty",
		"MY_SYSTEM_PROPERTY",
		"my_system_property",
		"My_System_Property",
		"My-System-Property",
		"my/System/Property",
	};
	// @formatter:on
	private static final String NORMALIZED_KEY = "my_system_property";
	private static final String ENVIRONMENT_VARIABLE = "MY_SYSTEM_PROPERTY";
	private static final String SYSTEM_PROPERTY = "my.system.property";
	private static final String PATH = "/my/system/property";

	@Test
	public void testNormalizeProperty() {
		for ( String eKey : KEYS ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( Configuration.asNormalized( eKey, Delimiter.SNAKE_CASE.getChar() ) );
			}
			assertEquals( NORMALIZED_KEY, Configuration.asNormalized( eKey, Delimiter.SNAKE_CASE.getChar() ) );
		}
	}

	@Test
	public void testEnvironmentVariable() {
		final Configuration theBuilder = new Configuration();
		for ( String eKey : KEYS ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theBuilder.toEnvironmentVariable( eKey ) );
			}
			assertEquals( ENVIRONMENT_VARIABLE, theBuilder.toEnvironmentVariable( eKey ) );
		}
	}

	@Test
	public void testEnvironmentVariableWithState() {
		final Configuration theBuilder = new Configuration();
		for ( String eKey : KEYS ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theBuilder.withKey( eKey ).toEnvironmentVariable() );
			}
			assertEquals( ENVIRONMENT_VARIABLE, theBuilder.withKey( eKey ).toEnvironmentVariable() );
		}
	}

	@Test
	public void testSystemProperty() {
		final Configuration theBuilder = new Configuration();
		for ( String eKey : KEYS ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theBuilder.toSystemProperty( eKey ) );
			}
			assertEquals( SYSTEM_PROPERTY, theBuilder.toSystemProperty( eKey ) );
		}
	}

	@Test
	public void testSystemPropertyWithState() {
		final Configuration theBuilder = new Configuration();
		for ( String eKey : KEYS ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theBuilder.withKey( eKey ).toSystemProperty() );
			}
			assertEquals( SYSTEM_PROPERTY, theBuilder.withKey( eKey ).toSystemProperty() );
		}
	}

	@Test
	public void testPath() {
		final Configuration theBuilder = new Configuration();
		for ( String eKey : KEYS ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theBuilder.toPath( eKey ) );
			}
			assertEquals( PATH, theBuilder.toPath( eKey ) );
		}
	}

	@Test
	public void testPathWithState() {
		final Configuration theBuilder = new Configuration();
		for ( String eKey : KEYS ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theBuilder.withKey( eKey ).toPath() );
			}
			assertEquals( PATH, theBuilder.withKey( eKey ).toPath() );
		}
	}
}
