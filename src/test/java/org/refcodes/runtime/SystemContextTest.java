// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.runtime;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.refcodes.data.Text;

public class SystemContextTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testSystemId1() {
		for ( SystemContext eCtx : SystemContext.values() ) {
			final int theSystemId1 = eCtx.toContextId();
			final int theSystemId2 = eCtx.toContextId();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eCtx + ": System-TID 1 = " + theSystemId1 );
			}
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eCtx + ": System-TID 2 = " + theSystemId2 );
			}
			assertEquals( theSystemId1, theSystemId2 );
		}
	}

	@Test
	public void testSystemId2() {
		for ( SystemContext eCtx : SystemContext.values() ) {
			final int theSystemId1 = eCtx.toContextId();
			final int theSystemId2 = eCtx.toContextId( "REFCODES.ORG" );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eCtx + ": System-TID 1 = " + theSystemId1 );
			}
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eCtx + ": System-TID 2 = " + theSystemId2 );
			}
			assertNotEquals( theSystemId1, theSystemId2 );
		}
	}

	@Test
	public void testSystemId3() {
		for ( SystemContext eCtx : SystemContext.values() ) {
			final int theSystemId1 = eCtx.toContextId( "FUNCODES.CLUB" );
			final int theSystemId2 = eCtx.toContextId( "REFCODES.ORG" );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eCtx + ": System-TID 1 = " + theSystemId1 );
			}
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eCtx + ": System-TID 2 = " + theSystemId2 );
			}
			assertNotEquals( theSystemId1, theSystemId2 );
		}
	}

	@Test
	public void testSystemId4() {
		for ( SystemContext eCtx : SystemContext.values() ) {
			final int theSystemId1 = eCtx.toContextId( "REFCODES.ORG" );
			final int theSystemId2 = eCtx.toContextId( "REFCODES.ORG" );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eCtx + ": System-TID 1 = " + theSystemId1 );
			}
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eCtx + ": System-TID 2 = " + theSystemId2 );
			}
			assertEquals( theSystemId1, theSystemId2 );
		}
	}

	@Test
	public void testSystemId5() {
		for ( SystemContext eCtx : SystemContext.values() ) {
			final int theSystemId1 = eCtx.toContextId( "FUNCODES.CLUB" );
			final int theSystemId2 = eCtx.toContextId( "FUNCODES.CLUB" );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eCtx + ": System-TID 1 = " + theSystemId1 );
			}
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eCtx + ": System-TID 2 = " + theSystemId2 );
			}
			assertEquals( theSystemId1, theSystemId2 );
		}
	}

	@Test
	public void testSystemIds1() {
		for ( SystemContext eCtx : SystemContext.values() ) {
			final int[] theSystemIds1 = eCtx.toContextIds( 3 );
			final int[] theSystemIds2 = eCtx.toContextIds( 3 );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eCtx + ": System-TID 1 = " + Arrays.toString( theSystemIds1 ) );
			}
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eCtx + ": System-TID 2 = " + Arrays.toString( theSystemIds2 ) );
			}
			assertArrayEquals( theSystemIds1, theSystemIds2 );
		}
	}

	@Test
	public void testSystemIds2() {
		for ( SystemContext eCtx : SystemContext.values() ) {
			final int[] theSystemIds1 = eCtx.toContextIds( 3 );
			final int[] theSystemIds2 = eCtx.toContextIds( 3, "REFCODES.ORG" );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eCtx + ": System-TID 1 = " + Arrays.toString( theSystemIds1 ) );
			}
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eCtx + ": System-TID 2 = " + Arrays.toString( theSystemIds2 ) );
			}
			assertNotEquals( theSystemIds1, theSystemIds2 );
		}
	}

	@Test
	public void testSystemIds3() {
		for ( SystemContext eCtx : SystemContext.values() ) {
			final int[] theSystemIds1 = eCtx.toContextIds( 3, "FUNCODES.CLUB" );
			final int[] theSystemIds2 = eCtx.toContextIds( 3, "REFCODES.ORG" );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eCtx + ": System-TID 1 = " + Arrays.toString( theSystemIds1 ) );
			}
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eCtx + ": System-TID 2 = " + Arrays.toString( theSystemIds2 ) );
			}
			assertNotEquals( theSystemIds1, theSystemIds2 );
		}
	}

	@Test
	public void testSystemIds4() {
		for ( SystemContext eCtx : SystemContext.values() ) {
			final int[] theSystemIds1 = eCtx.toContextIds( 3, "REFCODES.ORG" );
			final int[] theSystemIds2 = eCtx.toContextIds( 3, "REFCODES.ORG" );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eCtx + ": System-TID 1 = " + Arrays.toString( theSystemIds1 ) );
			}
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eCtx + ": System-TID 2 = " + Arrays.toString( theSystemIds2 ) );
			}
			assertArrayEquals( theSystemIds1, theSystemIds2 );
		}
	}

	@Test
	public void testSystemIds5() {
		for ( SystemContext eCtx : SystemContext.values() ) {
			final int[] theSystemIds1 = eCtx.toContextIds( 3, "FUNCODES.CLUB" );
			final int[] theSystemIds2 = eCtx.toContextIds( 3, "FUNCODES.CLUB" );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eCtx + ": System-TID 1 = " + Arrays.toString( theSystemIds1 ) );
			}
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eCtx + ": System-TID 2 = " + Arrays.toString( theSystemIds2 ) );
			}
			assertArrayEquals( theSystemIds1, theSystemIds2 );
		}
	}

	@Test
	public void testRun() {
		final Set<String> theIds = new HashSet<>();
		String eId;
		for ( SystemContext eGranularity : SystemContext.values() ) {
			eId = Integer.toString( ( eGranularity.toContextId() ) );
			theIds.add( eId );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eGranularity.name() + ": " + eId );
			}
		}
		assertEquals( SystemContext.values().length, theIds.size() );
		for ( SystemContext eGranularity : SystemContext.values() ) {
			eId = Integer.toString( eGranularity.toContextId( "_someSeed!" ) );
			theIds.add( eId );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eGranularity.name() + " (+seed): " + eId );
			}
		}
		assertEquals( SystemContext.values().length * 2, theIds.size() );
	}

	@Test
	public void testInvertible() {
		final char[] theText = Text.ARECIBO_MESSAGE.getText().toCharArray();
		char[] eObfuscated;
		char[] eUnObfuscated;
		for ( SystemContext eCtx : SystemContext.values() ) {
			eObfuscated = eCtx.toInvertible( theText );
			eUnObfuscated = eCtx.toInvertible( eObfuscated );
			assertArrayEquals( theText, eUnObfuscated );
			assertFalse( Arrays.equals( theText, eObfuscated ) );
			assertFalse( Arrays.equals( eObfuscated, eUnObfuscated ) );
		}
	}
}
