// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.runtime;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

public class TerminalTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////
	// @formatter:off
	private static final String MODE_CON_OUTPUT = 
		"""
			Status von Gerät CON:
			---------------------
			    Zeilen:          9001
			    Spalten:         120
			    Wiederholrate:   31
			    Verzögerungszeit:1
			    Codepage:        850\
			""";
	// @formatter:on

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testAnsiconWidth() {
		final String theAnsiconValue = "91x19999 (91x51)";
		final int theWidth = Terminal.toAnsiconWidth( theAnsiconValue );
		assertEquals( 91, theWidth );
	}

	@Test
	public void testAnsiconHeight() {
		final String theAnsiconValue = "91x19999 (91x51)";
		final int theHeight = Terminal.toAnsiconHeight( theAnsiconValue );
		assertEquals( 51, theHeight );
	}

	@Test
	public void testModeConWidth() {
		final int theWidth = Terminal.toModeConWidth( MODE_CON_OUTPUT );
		assertEquals( 120, theWidth );
	}

	@Test
	public void testModeConHeight() {
		final int theWidth = Terminal.toModeConHeight( MODE_CON_OUTPUT );
		assertEquals( 9001, theWidth );
	}

	@Disabled("For debugging purposes only")
	@Test
	public void testConsoleWidth() {
		System.out.println( "Width = " + Terminal.getWidth() );
	}
}
