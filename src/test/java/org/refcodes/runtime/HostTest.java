// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.runtime;

import java.util.Map;
import org.junit.jupiter.api.Test;

public class HostTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	// @Disabled("For debugging purposes only")
	@Test
	public void testSysInfo() {
		final Map<String, String> theSysInfos = Host.toSystemInfo();
		for ( String eKey : theSysInfos.keySet() ) {
			System.out.println( eKey + "= " + theSysInfos.get( eKey ) );
		}
	}

	// @Disabled("For debugging purposes only")
	@Test
	public void testPrettySysInfo() {
		System.out.println( Host.toPrettySystemInfo() );
	}
}
