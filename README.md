# README #

> The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.

## What is this repository for? ##

***This repository provides runtime as well as system related functionality such as determining the termina's width or height, the shell used to execute your java application and more.***

## How do I get set up? ##

To get up and running, include the following dependency (without the three dots "...") in your `pom.xml`:

```
<dependencies>
	...
	<dependency>
		<artifactId>refcodes-runtime</artifactId>
		<groupId>org.refcodes</groupId>
		<version>3.3.9</version>
	</dependency>
	...
</dependencies>
```

The artifact is hosted directly at [Maven Central](http://search.maven.org). Jump straight to the source codes at [Bitbucket](https://bitbucket.org/refcodes/refcodes-runtime). Read the artifact's javadoc at [javadoc.io](http://www.javadoc.io/doc/org.refcodes/refcodes-runtime). 

## Contribution guidelines ##

* [Report issues](https://bitbucket.org/refcodes/refcodes-runtime/issues)
* Finding bugs
* Helping fixing bugs
* Making code and documentation better
* Enhance the code

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.

> Due to the lack of [`jline3`](https://github.com/jline/jline3) Java 9 module support as of the time of this writing, the according code has been included by this artifact whilst adding Java module support to the "refcodes.org" artifacts. As of this, the sources below the package `org.jline` and all resources below the folder `./org/jline` are licensed under the [`BSD`](https://opensource.org/licenses/bsd-license.php) and the below license text. 

```
Copyright (c) 2002-2018, the original author or authors.
All rights reserved.

http://www.opensource.org/licenses/bsd-license.php

Redistribution and use in source and binary forms, with or
without modification, are permitted provided that the following
conditions are met:

Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer
in the documentation and/or other materials provided with
the distribution.

Neither the name of JLine nor the names of its contributors
may be used to endorse or promote products derived from this
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
OF THE POSSIBILITY OF SUCH DAMAGE.
```

> For the according license text file `org.jline.license.txt` is distributed alongside this artifact within this artifact's `/src/main/resources` resource folder. The included code is based on the artifact `org.jline:jline-terminal`, version `3.8.0`.